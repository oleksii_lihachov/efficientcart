��    i      d      �      �     �     �     �     �     �     �          	          &     6     =  
   E     P  
   \     g     t     }     �  N   �     �     �               '     9  1   L     ~     �     �  :   �  
   �  
   �  !   �     	     	     1	  	   7	  	   A	  	   K	  
   U	     `	     f	     w	     �	     �	     �	     �	     �	     �	  
   �	  
   �	     �	     �	     �	  
   
     
  -   
     B
     K
     [
     j
  
   p
  
   {
     �
     �
     �
     �
     �
     �
     �
     �
          
     '     8     F  
   T     _     g  	   p     z  E   ~  R   �               !     -     6     H  	   b     l  "   �      �     �  	   �  '   �  -   
     8     @     R     d  
   t       �  �     �     �     �     �     �     �          ,     J     f     �     �     �     �     �     �     �     
  /     H   G     �     �     �     �     �     �  ]        r  
   �  !   �  k   �     #     :  5   Q     �     �  
   �     �     �     �                    .     D     c      {     �     �     �  
   �     �     �     �     �  
        "  U   +     �     �     �  
   �     �     �  "        (     C     ^  $   k     �  
   �     �     �      �     �            
   )  
   4     ?     N     ^  i   g  W   �     )     2     D     Z  >   r  >   �     �               <     Y     h  N   w  O   �          #     =     Q     p  "   �    - Edit #my account Account Add to cart Additional Information Additional information Apply Apply coupon Billing Address Billing details Brands Buy Now Cart total Cart totals Categories Categories:  Checkout Compare Confirm New Password Confirm the last time to your order, You can change it <a href="%1$s">here</a> Confirm your order Continue Shopping Coupon apply Coupon code Create an account Create an account? Current Password (leave blank to leave unchanged) Display name Edit Email address Enter your details to the form below to process your order First Name First name Get the discount from coupon code Home I&rsquo;m searching for... Items Last Name Last name Load More Loading... Login Login / Register Login / Sign up Login to system Logout  Lost your password? Menu Menu: Mobile Menu My Account My Address My Addresses My Cart My Wishlist My account My cart New Password (leave blank to leave unchanged) Password Password Change Payment method Price Quick View Quick view Recently added items Related Products Related products Remember me Reset password Save changes Search Search for product... Share:  Ship to a different address? Shipping Address Shopping Cart Shopping cart Sign me up Sign up Subtotal Subtotal: TOP The following addresses will be used on the checkout page by default. This will be how your name will be displayed in the account section and in reviews Top Total Update Cart Username Username or email Username or email address View Cart What are you searching for?... What&sbquo;re you searching for... What&sbquo;re you searching for? Wishlist Wishlist  You have no items in your shopping cart You have not set up this type of address yet. compare continue shopping create an account login to system quick view recently added items Project-Id-Version: Thembay Theme
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100 >= 3 && n%100<=10 ? 3 : n%100 >= 11 && n%100<=99 ? 4 : 5;
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-03-31 11:23+0000
PO-Revision-Date: 2021-04-13 12:06+0000
Last-Translator: 
Language-Team: Arabic
Language: ar
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.2; wp-5.7 - تعديل #حساببي الحساب إضافة إلى السلة معلومات إضافية  معلومات إضافية  استخدام القسيمة
 استخدام القسيمة عنوان الفاتورة تفاصيل الفاتورة الماركة اشتر الآن إجمالي السلة إجمالي السلة الأقسام الأقسام: إنهاء الطلب مقارنة تأكيد كلمة المرور الجديدة لتحديث طلبك ، يمكنك تغيير <a href="%1$s">هنا</a> ملخص الطلب متابعة التسوق رمز القسيمة رمز القسيمة إنشاء حساب إنشاء حساب؟ كلمة المرور الحالية (اتركها فارغة لتركها دون تغيير) اسم المستخدم تعديل البريد الإلكتروني يرجى إدخال التفاصيل الخاصة بك في النموذج أدناه لإتمام طلبك الاسم الأول  الاسم الأول  احصل على الخصم من رمز القسيمة الرئيسية أنا أبحث عن... أصناف الاسم الأخير  الاسم الأخير  عرض المزيد تحميل... دخول دخول / تسجيل دخول / تسجيل الدخول إلى حسابك تسجيل الخروج نسيت كلمة المرور؟ القائمة القائمة القائمة حسابي عنواني عناويني سلتي منتجاتي المفضلة حسابي سلتي كلمة مرور جديدة (اتركها فارغة لتركها دون تغيير) كلمة المرور تغير كلمة المرور طرق الدفع السعر مشاهدة سريعة  مشاهدة سريعة  أصناف مضافة حديثاً منتجات ذات صلة منتجات ذات صلة تذكرني استعادة كلمة المرور حفظ البحث ابحث عن منتج... مشاركة: شحن لعنوان مختلف؟ عنوان الشحن سلة التسوق سلة التسوق سجلني تسجيل المجموع المجموع: أعلى سيتم استخدام العناوين التالية في صفحة الدفع بشكل افتراضي.  سيتم عرض اسم المستخدم في قسم الحساب وفي التقييم أعلى الإجمالي  تحديث السلة اسم المستخدم اسم المستخدم أو البريد الإلكتروني اسم المستخدم أو البريد الإلكتروني عرض السلة مالذي تبحث عنه؟ مالذي تبحث عنه؟ مالذي تبحث عنه؟ المفضلة المفضلة لا يوجد لديك منتجات في سلة التسوق الخاصة بك لم تقم بإعداد هذا النوع من العنوان حتى الآن. مقارنة متابعة التسوق إنشاء حساب الدخول إلى حسابك مشاهدة سريعة  أصناف مضافة حديثاً 