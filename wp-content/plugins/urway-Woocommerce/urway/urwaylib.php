<?php

/*****************************************************
* Avanthgarde Payments PHP class for encryption functions       *
* Developed by: Nagaraju Bandi                       *
* Date: 2016-01-08                                   *
* Version: 1.0                                       *
* Compatibility: PHP 5.2 and higher                  *  
* Copyright: Distribution and usage of this          *
* library is reserved.                               *
*****************************************************/


abstract class urwayLib
{
	//private constants
	const SANDBOX_PAYMENT_URL='http://49.206.197.63:8080/agcore/pg/v1/merchants';
	
	const LIVE_PAYMENT_URL='http://49.206.197.63:8080/agcore/pg/v1/merchants';
	
	
	const ENC 	= MCRYPT_RIJNDAEL_128;
	const MODE 	= MCRYPT_MODE_CBC;
	const IV 	= "0123456789abcdef";
	
	//variables
	public static $env;
	public static $cpUrl;


	//public methods  
	public static function getCpUrl($env)
	{
		//self::$env = $env;
		switch($env)
		{
			case 'sandbox':
				self::$cpUrl = self::SANDBOX_PAYMENT_URL;
				break;
			case 'live':
				self::$cpUrl = self::LIVE_PAYMENT_URL;
				break;
		}
		return self::$cpUrl;
	}
	
	
	public static function _Hashcreation($text)
	{
		
		
	
		$hash=hash('sha256', $text);
		
		return $hash;
	}
	
} // End of CitrusLib class... by Viatechs.in 08/07/2013 All Rights Reserved

