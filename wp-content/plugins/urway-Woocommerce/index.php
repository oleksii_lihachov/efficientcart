<?php
/*
Plugin Name: WooCommerce URWAY Payment Gateway
Description: Extends WooCommerce with URWAY Payment Gateway. AJAX Version supported.
Version: 1.0
Author: URWAY Technologies
*/

$bd=ABSPATH.'wp-content/plugins/'.dirname( plugin_basename( __FILE__ ) );
set_include_path($bd.'/urway'.PATH_SEPARATOR.get_include_path());
require_once($bd."/urway/urwaylib.php");

add_action('plugins_loaded', 'woocommerce_urway_init', 0);

function woocommerce_urway_init() {

  if ( !class_exists( 'WC_Payment_Gateway' ) ) return;

  /**
   * Localisation
   */
  load_plugin_textdomain('wc-urway', false, dirname( plugin_basename( __FILE__ ) ) . '/languages');
  
 
  if(isset($_GET['msg']) && $_GET['msg']!=''){
      
    add_action('the_content', 'showMessage');
  }

  function showMessage($content){
    return '<div class="content-area"><div class="box '.htmlentities($_GET['type']).'-'.$_GET['alert'].'">'.htmlentities(urldecode($_GET['msg'])).'</div></div>'.$content;
  }
  /**
   * Gateway class
   */
  class WC_urway extends WC_Payment_Gateway {
    protected $msg = array();
    public function __construct(){
      // Go wild in here
      $this -> id = 'urway';
      $this -> method_title = __('CardPayment', 'CardPayment');
	  $this->method_description = __( 'Pay securely by Credit or Debit card through Secured Server.', 'woocommerce' );
      $this -> icon = WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)) . '/images/urway.png';
      $this -> has_fields = false;
      $this -> init_form_fields();
      $this -> init_settings();
      $this -> title = $this -> settings['title'];
      $this -> description = $this -> settings['description'];
      $this -> merchant_id = $this -> settings['merchant_id'];
      $this -> merchant_key = $this -> settings['merchant_key'];
      $this -> terminalId = $this -> settings['terminalId'];
	  $this -> url = $this -> settings['url'];
      $this -> gateway_server = $this -> settings['gateway_server'];
      $this -> transaction_method = $this -> settings['transaction_method'];
      $this -> channel = $this -> settings['channel'];
      $this -> redirect_page_id = $this -> settings['redirect_page_id'];
      $this -> liveurl = 'http://www.abc.com';
      $this -> msg['message'] = "";
      $this -> msg['class'] = "";
      add_action('init', array(&$this, 'check_urway_response'));
      //update for woocommerce >2.0
      add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'check_urway_response' ) );

      add_action('valid-urway-request', array(&$this, 'SUCCESS'));
      if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ) {
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
      } else {
        add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
      }
      add_action('woocommerce_receipt_urway', array(&$this, 'receipt_page'));
      add_action('woocommerce_thankyou_urway',array(&$this, 'thankyou_page'));
      
    }
    
    function init_form_fields(){

      $this -> form_fields = array(
        'enabled' => array(
            'title' => __('Enable/Disable', 'urway'),
            'type' => 'checkbox',
            'label' => __('Enable urway Payment Module.', 'urway'),
            'default' => 'no'),
          'title' => array(
            'title' => __('Title:', 'urway'),
            'type'=> 'text',
            'description' => __('This controls the title which the user sees during checkout.', 'urway'),
            'default' => __('CardPayment', 'urway')),
          'description' => array(
            'title' => __('Description:', 'urway'),
            'type' => 'textarea',
            'description' => __('This controls the description which the user sees during checkout.', 'urway'),
            'default' => __('Pay securely by Credit or Debit card or net banking through urway Secure Servers.', 'urway')),		   
		  'password' => array(
            'title' => __('Password', 'urway'),
            'type' => 'text',
            'description' =>  __('Password.', 'urway')
            ),			
		  'merchant_key' => array(
            'title' => __('Merchant Key', 'urway'),
            'type' => 'text',
            'description' =>  __('Merchant Key.', 'urway')
            ),			
		  'aggregator_id' => array(
            'title' => __('Terminal ID', 'urway'),
            'type' => 'text',
            'description' =>  __('Terminal ID.', 'urway')
            ),
          'transaction_method' => array(
            'title' => __('Transaction Type', 'urway'),
            'type' => 'select',
            'options' => array("1"=>"Purchase","4"=>"Authorization","5"=>"Capture","10"=>"Transaction Inquiry	","2"=>"Credit","9"=>"Void Authorization","7"=>"Void Capture","3"=>"Void Purchase","6"=>"Void Refund","11"=>"Transafer"),
            'description' => __('Transaction Type','urway')
            ),
		  'url' => array(
            'title' => __('Request URL', 'urway'),
            'type' => 'text',
            'description' =>  __('Request URL.', 'urway')
            ),
          
          'channel' => array(
            'title' => __('Channel', 'urway'),
            'type' => 'select',
            'options' => array("0"=>"Select","WEB"=>"Web","MOBILE"=>"Mobile"),
            'description' => __('Channel.','urway')
            )
          );


    }
    
    /**
     * Admin Panel Options
     * - Options for bits like 'title' and availability on a country-by-country basis
     **/
    public function admin_options(){
      echo '<h3>'.__('URWAY Payment Gateway', 'urway').'</h3>';
      echo '<p>'.__('URWAY is most popular payment gateway for online shopping in KSA').'</p>';
      echo '<table class="form-table">';
      $this -> generate_settings_html();
      echo '</table>';

    }
    /**
     *  There are no payment fields for urway, but we want to show the description if set.
     **/
    function payment_fields(){
      if($this -> description) echo wpautop(wptexturize($this -> description));
    }
    /**
     * Receipt Page
     **/
    function receipt_page($order){
      echo '<p>'.__('Thank you for your order, please click the button below to pay with urway.', 'urway').'</p>';
      echo $this -> generate_urway_form($order);
    }
	/**
     * Receipt Page
     **/
    function thankyou_page($order){
      //echo '<p>'.__('Thank you for your order.', 'urway').'</p>';
      //echo $this -> generate_urway_form($order);
    }
	
    /**
     * Process the payment and return the result
     **/
    function process_payment($order_id){
      $order = new WC_Order($order_id);
	  return array('result' => 'success', 'redirect' => add_query_arg('order',
        $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay')))));
    }
    
    /**
     * Check for valid urway server callback
     **/    
    function check_urway_response(){
		global $woocommerce;
		
   
	  
	  if($_GET !== NULL)
		{
			$order 		= new WC_Order($_GET['TrackId']);
			$orderStatus=$order->get_status();
			
			$transauthorised = false;
			$merchant_key=$this->merchant_key;
			
			$requestHash ="".$_GET['TranId']."|".$merchant_key."|".$_GET['ResponseCode']."|".$_GET['amount']."";
	
			$hash=hash('sha256', $requestHash);
			
			$url= $this -> settings['url'];
			
			//Security API Call
			
			$host= gethostname();
			$ip = gethostbyname($host);
		
		 $terminalId= $this -> settings['aggregator_id'];
		 $password=$this -> settings['password'];
		 
			$currencycode = get_woocommerce_currency();
			//$currencycode = "SAR";

			
			$txn_details1= "".$_GET['TrackId']."|".$terminalId."|".$password."|".$merchant_key."|".$_GET['amount']."|".$currencycode."";
		$requestHash1 	= urwayLib::_Hashcreation($txn_details1);;	
			
			$apifields = array(
            'trackid' => $_GET['TrackId'],
            'terminalId' => $terminalId,
			'action' => '10',
			'merchantIp' =>$ip,
			'password'=> $password,
			'currency' => $currencycode,
			'transid'=>$_GET['TranId'],
			'amount' => $_GET['amount'],
			'udf5'=>"",
			'udf3'=>"",
			'udf4'=>"",
			'udf1'=>"",
			'udf2'=>"",
			'requestHash' => $requestHash1
            );
      
		
		$apifields_string = json_encode($apifields);	
//echo "<pre>";
//echo "request Json:- ".$apifields_string;die;		
		
		$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $apifields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Content-Length: ' . strlen($apifields_string))
						);
							curl_setopt($ch, CURLOPT_TIMEOUT, 5);
							curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

							//execute post
							$apiresult = curl_exec($ch);
						/*	echo "<pre>";
							print_r($apiresult);
							die;*/
							$urldecodeapi=(json_decode($apiresult,true));
							$inquiryResponsecode=$urldecodeapi['responseCode'];
							$inquirystatus=$urldecodeapi['result'];
							
			//End Security API Call
			
			
	
		if($orderStatus=='pending')
		{
			if($hash === $_GET['responseHash'] && ($_GET['Result']==="Successful" || $_GET['Result']==="SUCCESS" || $_GET['Result']==="Success"))
			{ 
					
			
					if($inquirystatus=='Successful' || $inquiryResponsecode=='000'){
			
				$woocommerce -> cart -> empty_cart();
				$order 		= new WC_Order($_GET['TrackId']);
				
				$transauthorised = true;
				$this -> msg['message'] = "Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be shipping your order to you soon.";
				$this -> msg['class'] = 'woocommerce';
				$this ->msg['type']="info";
						 
						 
									
				$order -> payment_complete();
				$order -> update_status('processing');
				$order -> add_order_note('URWAY Payment Gateway has processed the payment. Ref Number: '. $txrefno);
				$order -> add_order_note($this->msg['message']);
				
				//$redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0)?get_site_url() . "/":get_permalink($this -> redirect_page_id);
  
		//$redirect_url=$redirect_url."checkout/order-received/".$_GET['TrackId']."/?key=".$order -> order_key;
       
		//$redirect_url = $order ->get_checkout_order_received_url();
		$redirect_url=$this->get_return_url ($order);

       wp_redirect( $redirect_url );die;
				
					}
					else
					{
						$this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Thank you for shopping with us. However, the transaction has been declined.";
				$this -> msg['type']= "error";
					}
				
			}
			else
			{
			   
				if($inquirystatus=='UnSuccessful'){
				$order 		= new WC_Order($_GET['TrackId']);
								
				$transauthorised = false;
				
				
			
					      
					if($_GET['ResponseCode'] == "624"){
				    
				    $this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Thank you for shopping with us. However, the transaction has been Cancelled.";
				$this -> msg['type']= "error";
				
				$order -> update_status('cancelled');
						$order -> add_order_note('Cancelled');
						$order -> add_order_note($this->msg['message']);
				}else
				{
				    $this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Thank you for shopping with us. However, the transaction has been declined.";
				$this -> msg['type']= "error";
				
				$order -> update_status('failed');
						$order -> add_order_note('Failed');
						$order -> add_order_note($this->msg['message']);}
				 
			}else
					{
						$this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Thank you for shopping with us. However, the transaction has been declined.";
				$this -> msg['type']= "error";
					}
					  }
		}
		else
		{
				$this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Thank you for shopping with us. However, the transaction has been declined.";
				$this -> msg['type']= "error";
		}
					  
		$redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0)?get_site_url() . "/":get_permalink($this -> redirect_page_id);
  
       $redirect_url = add_query_arg( array('msg'=> urlencode($this -> msg['message']), 'type'=>$this -> msg['class'],'alert'=>$this -> msg['type']), $redirect_url );


       wp_redirect( $redirect_url );die;
		}
    }
    
    
    
    /*
     //Removed For WooCommerce 2.0
    function showMessage($content){
         return '<div class="box '.$this -> msg['class'].'-box">'.$this -> msg['message'].'</div>'.$content;
     }*/
    
    /**
     * Generate urway button link
     **/    
    public function generate_urway_form($order_id)
	{
		include("responsecode.php");
     

	 
      global $woocommerce;
      $order = new WC_Order($order_id);
      $redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0)?get_site_url() . "/":get_permalink($this -> redirect_page_id);
      
      //For wooCoomerce 2.0
      $redirect_url = add_query_arg( 'wc-api', get_class( $this ), $redirect_url );
      $order_id = $order_id.''.date("ymds").rand();
      
      //do we have a phone number?
      //get currency  

		$id=$order->id;
      $address = $order -> billing_address_1;
      if ($order -> billing_address_2 != "")
        $address = $address.' '.$order -> billing_address_2;
	
      
	  $currencycode = get_woocommerce_currency();
		//$currencycode = "SAR";	  
	
      $merchantTxnId = $order_id;
      $orderAmount = $order -> order_total;		
      $action = urwayLib::getCpUrl($this->gateway_server); 
      
	  
	  	$success_url =  $redirect_url;
		$failure_url =  $redirect_url;
		
		$host= gethostname();
		$ip = gethostbyname($host);
		
		 $terminalId= $this -> settings['aggregator_id'];
		 $password=$this -> settings['password'];
		 
		$txn_details= "".$id."|".$terminalId."|".$password."|".$this->merchant_key."|".$orderAmount."|".$currencycode."";
		$requestHash 	= urwayLib::_Hashcreation($txn_details);
		
		$url= $this -> settings['url'];

		$Transaction_type=$this -> transaction_method;
		
		$merchant_key=$this->merchant_key;
		
		if($terminalId=="" && $password=="" && $merchant_key=="" && $url=="")
		{
			
				$this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Contact administrator for issue related to Configuration !!!!";
				$this -> msg['type']= "error";
						
						$redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0)?get_site_url() . "/":get_permalink($this -> redirect_page_id);
  
       $redirect_url = add_query_arg( array('msg'=> urlencode($this -> msg['message']), 'type'=>$this -> msg['class'],'alert'=>$this -> msg['type']), $redirect_url );


       wp_redirect( $redirect_url );die;
		}
		if($terminalId=="" || $password=="" || $merchant_key=="" || $url=="")
		{
			$this -> msg['message'] = "";
			if($merchant_key=="")
			{
				$this -> msg['message'] = "Contact administrator for issue related to merchant key !!!!";
			}
			if($terminalId=="")
			{
				$this -> msg['message'] = "Contact administrator for issue related to terminal id !!!!";
			}
			if($password=="")
			{
				$this -> msg['message'] = "Contact administrator for issue related to password of terminal!!!!";
			}
			if($url=="")
			{
				$this -> msg['message'] = "Contact administrator for issue related to request URL!!!!";
			}
			$this -> msg['class'] = 'woocommerce';
				$this -> msg['type']= "error";
						
						$redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0)?get_site_url() . "/":get_permalink($this -> redirect_page_id);
  
       $redirect_url = add_query_arg( array('msg'=> urlencode($this -> msg['message']), 'type'=>$this -> msg['class'],'alert'=>$this -> msg['type']), $redirect_url );


       wp_redirect( $redirect_url );die;
		}
		
		$fields = array(
            'trackid' => $id,
            'terminalId' => $terminalId,
			'customerEmail' => $order -> billing_email ,
			'action' => $Transaction_type,
			'merchantIp' =>$ip,
			'password'=> $password,
			'currency' => $currencycode,
			'country'=>$order -> billing_country,
			'tranid' => $merchantTxnId,
			'amount' => $orderAmount,
			'udf5'=>"",
			'udf3'=>"",
			'udf4'=>"",
			'udf1'=>"WooCommerce",
			'udf2'=>"",
			'requestHash' => $requestHash
            );
      
		
		$fields_string = json_encode($fields);
		//echo "<pre>";
	//echo "request Json:- ".$fields_string;die;
		
		
		$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Content-Length: ' . strlen($fields_string))
						);
							curl_setopt($ch, CURLOPT_TIMEOUT, 5);
							curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

							//execute post
							$result = curl_exec($ch);
							
							//echo "hi";
							//print_r($result);
							//die;
							//close connection
							curl_close($ch);
									if(isset($result))
									{
									$urldecode=(json_decode($result,true));
									}
									else{
								
										$this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Thank you for shopping with us. Transaction Failed. Please check configuration values.";
				$this -> msg['type']= "error";
				
				$order -> update_status('failed');
						$order -> add_order_note('Failed');
						$order -> add_order_note($this->msg['message']);
						
						
						$redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0)?get_site_url() . "/":get_permalink($this -> redirect_page_id);
  
       $redirect_url = add_query_arg( array('msg'=> urlencode($this -> msg['message']), 'type'=>$this -> msg['class'],'alert'=>$this -> msg['type']), $redirect_url );


       wp_redirect( $redirect_url );die;
									}
									
									$responsecode=$urldecode['responseCode'];
								
									$url=$urldecode['targetUrl']."?paymentid=".$urldecode['payid'];
									
									if($urldecode['targetUrl']!='' || $urldecode['targetUrl']!=null)
									{
									if($urldecode['payid'] != NULL)
									{echo '
									<html>
									<form name="myform" method="POST" action="'.$url.'">
									<h1>Transaction is processing......</h1>
									</form>
									<script type="text/javascript">document.myform.submit();
									</script>
									</html>';die;}
									}
									else{
										$this -> msg['class'] = 'woocommerce';
				$this -> msg['message'] = "Thank you for shopping with us. Transaction Unsuccessful.".$arr[$responsecode];
				$this -> msg['type']= "error";
				
				$order -> update_status('failed');
						$order -> add_order_note('Failed');
						$order -> add_order_note($this->msg['message']);
						
						
						$redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0)?get_site_url() . "/":get_permalink($this -> redirect_page_id);
  
       $redirect_url = add_query_arg( array('msg'=> urlencode($this -> msg['message']), 'type'=>$this -> msg['class'],'alert'=>$this -> msg['type']), $redirect_url );


       wp_redirect( $redirect_url );
										
									}
									
							   }		
	
		
     
    function get_pages($title = false, $indent = true) {
      $wp_pages = get_pages('sort_column=menu_order');
      $page_list = array();
      if ($title) $page_list[] = $title;
      foreach ($wp_pages as $page) {
        $prefix = '';
        // show indented child pages?
        if ($indent) {
          $has_parent = $page->post_parent;
          while($has_parent) {
            $prefix .=  ' - ';
            $next_page = get_page($has_parent);
            $has_parent = $next_page->post_parent;
          }
        }
        // add to page list array array
        $page_list[$page->ID] = $prefix . $page->post_title;
      }
      return $page_list;
    }

  }

  /**
   * Add the Gateway to WooCommerce
   **/
  function woocommerce_add_urway_gateway($methods) {
    $methods[] = 'WC_urway';
    return $methods;
  }

  add_filter('woocommerce_payment_gateways', 'woocommerce_add_urway_gateway' );
}

?>
