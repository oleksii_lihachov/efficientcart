<?php

namespace WPStaging\Pro\Backup\Job\Jobs;

use RuntimeException;
use WPStaging\Framework\Adapter\Directory;
use WPStaging\Pro\Backup\Dto\Job\JobImportDataDto;
use WPStaging\Pro\Backup\Job\AbstractQueueJob;
use WPStaging\Pro\Backup\Entity\BackupMetadata;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\ExtractFilesTask;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\CleanupImportTask;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\RequirementsCheckTask;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\ImportDatabaseTask;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\ImportUploadsTask;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\ImportMuPluginsTask;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\ImportPluginsTask;
use WPStaging\Pro\Backup\Task\Tasks\JobImport\ImportThemesTask;

// Need to make sure the destination is not already there before we extract or will create problems with file contents
class JobImport extends AbstractQueueJob
{
    const TMP_DIRECTORY = 'tmp/import/';

    /** @var JobImportDataDto $jobDataDto */
    protected $jobDataDto;

    /** @var array The array of tasks to execute for this job. Populated at init(). */
    private $tasks = [];

    public static function getJobName()
    {
        return 'backup_import';
    }

    protected function getJobTasks()
    {
        return $this->tasks;
    }

    protected function execute()
    {
        $this->startBenchmark();

        $response = $this->getResponse($this->currentTask->execute());

        $this->finishBenchmark(get_class($this->currentTask));

        return $response;
    }

    protected function init()
    {
        if ($this->jobDataDto->getBackupMetadata()) {
            return;
        }

        try {
            $backupMetadata = (new BackupMetadata())->hydrateByFilePath($this->jobDataDto->getFile());
        } catch (\Exception $e) {
            throw $e;
        }

        if (!$backupMetadata->getHeaderStart()) {
            throw new RuntimeException('Failed to get backup metadata.');
        }

        $this->jobDataDto->setBackupMetadata($backupMetadata);
        $this->jobDataDto->setTmpDirectory($this->getTmpDirectory());

        $this->tasks[] = RequirementsCheckTask::class;
        $this->tasks[] = CleanupImportTask::class; // Delete extracted files on start
        $this->tasks[] = ExtractFilesTask::class;

        // todo: Refactor this when implementing the "replace" option, instead of "append"
        if ($backupMetadata->getIsExportingUploads() || $backupMetadata->getIsExportingOtherWpContentFiles() || $backupMetadata->getIsExportingDatabase()) {
            $this->tasks[] = ImportUploadsTask::class;
        }

        if ($backupMetadata->getIsExportingThemes()) {
            $this->tasks[] = ImportThemesTask::class;
        }

        if ($backupMetadata->getIsExportingPlugins()) {
            $this->tasks[] = ImportPluginsTask::class;
        }

        if ($backupMetadata->getIsExportingMuPlugins()) {
            $this->tasks[] = ImportMuPluginsTask::class;
        }

        if ($backupMetadata->getIsExportingDatabase()) {
            $this->tasks[] = ImportDatabaseTask::class;
        }

        $this->tasks[] = CleanupImportTask::class; // Delete extracted files on finish
    }

    private function getTmpDirectory()
    {
        $dir = $this->directory->getPluginUploadsDirectory() . static::TMP_DIRECTORY . $this->jobDataDto->getId();
        $this->filesystem->mkdir($dir);

        return trailingslashit($dir);
    }
}
