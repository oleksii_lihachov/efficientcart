<?php

namespace WPStaging\Pro\Backup\Job\Jobs;

use WPStaging\Pro\Backup\Dto\Job\JobExportDataDto;
use WPStaging\Pro\Backup\Job\AbstractQueueJob;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\DirectoryScannerTask;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\FileScannerTask;
use WPStaging\Pro\Backup\Dto\TaskResponseDto;
use WPStaging\Pro\Backup\Dto\Task\Export\Response\DatabaseExportResponseDto;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\DatabaseExportTask;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\CombineExportTask;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\IncludeDatabaseTask;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\FileExportTask;

class JobExport extends AbstractQueueJob
{
    /** @var JobExportDataDto $jobDataDto */
    protected $jobDataDto;

    private static $availableTasks = [
        DirectoryScannerTask::class,
        FileScannerTask::class,
        FileExportTask::class,
        DatabaseExportTask::class,
        IncludeDatabaseTask::class,
        CombineExportTask::class,
    ];

    private static $tasksManuallySet = false;

    public static function getJobName()
    {
        return 'backup_export';
    }

    protected function getJobTasks()
    {
        return static::$availableTasks;
    }

    protected function execute()
    {
        $this->startBenchmark();

        $response = $this->getResponse($this->currentTask->execute());
        $this->setDtoByResponse($response);

        $this->finishBenchmark(get_class($this->currentTask));

        return $response;
    }

    protected function init()
    {
        if (static::$tasksManuallySet || $this->jobDataDto->getIsExportingDatabase()) {
            return;
        }

        $removeTasks = [
            DatabaseExportTask::class,
            IncludeDatabaseTask::class,
        ];
        foreach ($removeTasks as $removeTask) {
            $key = array_search($removeTask, self::$availableTasks);
            if ($key) {
                // TODO PHP5.6; use a property instead of modifying self::$availableTasks
                unset(static::$availableTasks[$key]);
            }
        }
    }

    protected function findCurrentStatusTitleArgs()
    {
        $args = [];
        if (!$this->currentTask) {
            return $args;
        }

        switch (get_class($this->currentTask)) {
            case FileScannerTask::class:
                $args[] = $this->jobDataDto->getTotalDirectories();
                break;
            case FileExportTask::class:
                $args[] = $this->jobDataDto->getTotalFiles();
                break;
        }

        return $args;
    }

    private function setDtoByResponse(TaskResponseDto $responseDto)
    {
        switch ($responseDto->getTask()) {
            case DirectoryScannerTask::getTaskName():
                $this->jobDataDto->setTotalDirectories($responseDto->getTotal());
                break;
            case FileScannerTask::getTaskName():
                $this->jobDataDto->setTotalFiles($responseDto->getTotal());
                break;
            case DatabaseExportTask::getTaskName():
                /** @var DatabaseExportResponseDto $responseDto */
                $this->jobDataDto->setDatabaseFile($responseDto->getFilePath());
                break;
        }
    }

    // TODO Remove after Processing.php is removed. This is backward compatibility only
    public static function setJobTasks(array $tasks)
    {
        static::$tasksManuallySet = true;
        static::$availableTasks   = $tasks;
    }
}
