<?php

// TODO PHP7.x; declare(strict_types=1);
// TODO PHP7.x; return types && type-hints

namespace WPStaging\Pro\Backup\Job;

use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Framework\Interfaces\ShutdownableInterface;
use WPStaging\Framework\Traits\BenchmarkTrait;
use WPStaging\Pro\Backup\Dto\AbstractDto;
use WPStaging\Pro\Backup\Task\AbstractTask;
use WPStaging\Pro\Backup\Dto\TaskResponseDto;
use WPStaging\Framework\Queue\Queue;
use WPStaging\Framework\Queue\Storage\CacheStorage;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Core\WPStaging;
use WPStaging\Pro\Backup\Dto\JobDataDto;

abstract class AbstractQueueJob implements ShutdownableInterface
{
    use BenchmarkTrait;

    /** @var JobDataDto */
    protected $jobDataDto;

    /** @var Cache $jobDataCache Persists the JobDataDto in the filesystem. */
    private $jobDataCache;

    /** @var Queue Controls the queue of tasks. */
    private $jobQueue;

    /** @var AbstractTask */
    protected $currentTask;

    /** @var Filesystem */
    protected $filesystem;

    /** @var Directory */
    protected $directory;

    public function __construct(
        Queue $jobQueue,
        CacheStorage $jobQueueStorage,
        Cache $jobDataCache,
        JobDataDto $jobDataDto,
        Filesystem $filesystem,
        Directory $directory
    ) {
        $this->jobDataDto   = $jobDataDto;
        $this->jobDataCache = $jobDataCache;
        $this->jobQueue     = $jobQueue;
        $this->filesystem   = $filesystem;
        $this->directory    = $directory;

        $this->jobDataCache->setLifetime(HOUR_IN_SECONDS);
        $this->jobDataCache->setFilename('jobCache_' . $this::getJobName());
        #$this->jobDataCache->setPath(static::getJobCacheDirectory());

        #$this->jobQueueStorage->getCache()->setPath(static::getJobCacheDirectory());
        $this->jobQueue->setName($this->findCurrentJob());

        /** Persists the queue of tasks in the filesystem. */
        $this->jobQueue->setStorage($jobQueueStorage);
    }

    public function onWpShutdown()
    {
        if ($this->jobDataDto->isStatusCheck()) {
            return;
        }

        if ($this->jobDataDto->isFinished()) {
            // Finished processing this job
            #$this->clean();

            return;
        }

        $data = $this->jobDataDto->toArray();

        $this->jobDataCache->save($data, true);
    }


    /** @return string */
    abstract public static function getJobName();

    /** @return array */
    abstract protected function getJobTasks();

    /** @return TaskResponseDto */
    abstract protected function execute();

    /** @return void */
    abstract protected function init();

    /** @return TaskResponseDto */
    public function prepareAndExecute()
    {
        $this->prepare();

        $response = $this->execute();

        return $response;
    }

    /**
     * @return JobDataDto
     */
    public function getJobQueueDto()
    {
        return $this->jobDataDto;
    }

    /**
     * @var $jobQueueDto JobDataDto
     */
    public function setJobQueueDto($jobQueueDto)
    {
        $this->jobDataDto = $jobQueueDto;
    }

    private function prepare()
    {
        $data = $this->jobDataCache->get([]);

        if ($data) {
            $this->jobDataDto->hydrate($data);
        }

        // TODO RPoC Hack
        $this->jobDataDto->setStatusCheck(!empty($_GET['action']) && $_GET['action'] === 'wpstg--backups--status');

        if ($this->jobDataDto->isStatusCheck()) {
            return;
        }

        if ($this->jobDataDto->isInit()) {
            $this->init();
        }

        if ($this->jobDataDto->isInit()) {
            $this->jobQueue->reset();
            $this->addTasks($this->getJobTasks());
        }

        $this->jobDataDto->setInit(false);

        /** @var AbstractTask currentTask */
        $this->currentTask = WPStaging::getInstance()->get($this->jobQueue->pop());

        if (!$this->currentTask instanceof AbstractTask) {
            throw new \RuntimeException('Next task of queue job is null or invalid. Maybe the queue is not working correctly?');
        }

        if (!$this->jobDataDto instanceof AbstractDto) {
            throw new \RuntimeException('Job Queue DTO is null or invalid.');
        }

        $this->currentTask->setJobContext($this);
        $this->currentTask->setJobQueueDto($this->jobDataDto);
        $this->currentTask->setJobId($this->jobDataDto->getId());
        $this->currentTask->setJobName($this::getJobName());
        $this->currentTask->setDebug(defined('WPSTG_DEBUG') && WPSTG_DEBUG);

        $this->saveCurrentStatusTitle();
    }

    /**
     * @param TaskResponseDto $response
     *
     * @return TaskResponseDto
     */
    protected function getResponse(TaskResponseDto $response)
    {
        $response->setJob(substr($this->findCurrentJob(), 3));

        // TODO RPoC below
        // Task is not done yet, add it to beginning of the queue again
        if (!$response->isStatus()) {
            // TODO PHP7.x; $this->currentTask::class;
            $className = get_class($this->currentTask);
            $this->jobQueue->prepend($className);
        }

        if ($response->isStatus() && $this->jobQueue->count() === 0) {
            $this->jobDataDto->setFinished(true);

            return $response;
        }

        $response->setStatus(false);

        return $response;
    }

    private function findCurrentJob()
    {
        $class = explode('\\', static::class);

        return end($class);
    }

    protected function addTasks(array $tasks = [])
    {
        foreach ($tasks as $task) {
            $this->jobQueue->push($task);
        }
    }

    public static function getJobCacheDirectory()
    {
        $cacheDir = trailingslashit((new Directory())->getCacheDirectory()) . static::getJobName();

        return trailingslashit($cacheDir);
    }

    /**
     * Provides arguments to be used within saveCurrentStatusTitle()
     * In most use cases it will be overwritten but in case it is forgotten, it will not throw error
     * @return array
     */
    protected function findCurrentStatusTitleArgs()
    {
        return [];
    }

    protected function saveCurrentStatusTitle()
    {
        // Might happen when xdebug enabled so saving us some extra reading unnecessary log output
        if (!$this->currentTask) {
            return;
        }

        $args  = $this->findCurrentStatusTitleArgs();
        $title = $this->currentTask->getTaskTitle();
        $this->jobDataDto->setCurrentStatusTitle($title);
        $this->jobDataCache->save($this->jobDataDto->toArray());
    }
}
