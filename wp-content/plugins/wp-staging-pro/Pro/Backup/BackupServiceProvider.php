<?php

namespace WPStaging\Pro\Backup;

use WPStaging\Framework\DI\FeatureServiceProvider;
use WPStaging\Framework\Filesystem\DirectoryScanner;
use WPStaging\Framework\Filesystem\FileScanner;
use WPStaging\Pro\Backup\Ajax\Export\PrepareExport;
use WPStaging\Pro\Backup\Ajax\Import\PrepareImport;
use WPStaging\Pro\Backup\Dto\Job\JobExportDataDto;
use WPStaging\Pro\Backup\Dto\Job\JobImportDataDto;
use WPStaging\Pro\Backup\Dto\JobDataDto;
use WPStaging\Pro\Backup\Ajax\Cancel;
use WPStaging\Pro\Backup\Ajax\Export;
use WPStaging\Pro\Backup\Ajax\Delete;
use WPStaging\Pro\Backup\Ajax\Edit;
use WPStaging\Pro\Backup\Ajax\Listing;
use WPStaging\Pro\Backup\Ajax\Import;
use WPStaging\Pro\Backup\Ajax\FileInfo;
use WPStaging\Pro\Backup\Ajax\FileList;
use WPStaging\Pro\Backup\Ajax\Status;
use WPStaging\Pro\Backup\Ajax\Upload;
use WPStaging\Pro\Backup\Job\Jobs\JobExport;
use WPStaging\Pro\Backup\Job\Jobs\JobImport;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\DirectoryScannerTask;
use WPStaging\Pro\Backup\Task\Tasks\JobExport\FileScannerTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;

class BackupServiceProvider extends FeatureServiceProvider
{
    /**
     * Toggle the experimental backup feature on/off.
     * Used only for developers of WPSTAGING while the backups feature is being developed.
     * Do not turn this on unless you know what you're doing, as it might irreversibly delete
     * files, databases, etc.
     */
    public static function getFeatureTrigger()
    {
        return 'WPSTG_FEATURE_ENABLE_BACKUP';
    }

    protected function registerClasses()
    {
        // @todo: Remove this once this is merged: https://github.com/lucatume/di52/pull/32
        $this->container->bind(JobDataDto::class, function () {
            return new JobDataDto();
        });

        // Jobs
        #$this->container->singleton(JobImport::class);
        #$this->container->singleton(JobExport::class);

        $this->container->when(JobExport::class)
                        ->needs(JobDataDto::class)
                        ->give(JobExportDataDto::class);

        $this->container->when(JobImport::class)
                        ->needs(JobDataDto::class)
                        ->give(JobImportDataDto::class);

        $this->sharedLoggerForFileScanner();
        $this->sharedLoggerForDirectoryScanner();
    }

    protected function addHooks()
    {
        add_action('wp_ajax_wpstg--backups--prepare-export', $this->container->callback(PrepareExport::class, 'ajaxPrepare'));
        add_action('wp_ajax_wpstg--backups--export', $this->container->callback(Export::class, 'render'));

        add_action('wp_ajax_wpstg--backups--prepare-import', $this->container->callback(PrepareImport::class, 'ajaxPrepare'));
        add_action('wp_ajax_wpstg--backups--import', $this->container->callback(Import::class, 'render'));

        add_action('wp_ajax_wpstg--backups--listing', $this->container->callback(Listing::class, 'render'));
        add_action('wp_ajax_wpstg--backups--delete', $this->container->callback(Delete::class, 'render'));
        add_action('wp_ajax_wpstg--backups--cancel', $this->container->callback(Cancel::class, 'render'));
        add_action('wp_ajax_wpstg--backups--edit', $this->container->callback(Edit::class, 'render'));
        add_action('wp_ajax_wpstg--backups--status', $this->container->callback(Status::class, 'render'));
        add_action('wp_ajax_wpstg--backups--import--file-list', $this->container->callback(FileList::class, 'render'));
        add_action('wp_ajax_wpstg--backups--import--file-info', $this->container->callback(FileInfo::class, 'render'));
        add_action('wp_ajax_wpstg--backups--import--file-upload', $this->container->callback(Upload::class, 'render'));

        // Nopriv
        add_action('wp_ajax_nopriv_wpstg--backups--import', $this->container->callback(Import::class, 'render'));
    }

    /**
     * Makes sure FileScannerTask and FileScanner receives
     * the same instance of LoggerInterface.
     */
    private function sharedLoggerForFileScanner()
    {
        $fileScannerLogger = clone $this->container->make(LoggerInterface::class);

        $this->container->when(FileScannerTask::class)
                        ->needs(LoggerInterface::class)
                        ->give($fileScannerLogger);

        $this->container->when(FileScanner::class)
                        ->needs(LoggerInterface::class)
                        ->give($fileScannerLogger);
    }

    /**
     * Makes sure DirectoryScannerTask and DirectoryScanner receives
     * the same instance of LoggerInterface.
     */
    private function sharedLoggerForDirectoryScanner()
    {
        $directoryScannerLogger = clone $this->container->make(LoggerInterface::class);

        $this->container->when(DirectoryScannerTask::class)
                        ->needs(LoggerInterface::class)
                        ->give($directoryScannerLogger);

        $this->container->when(DirectoryScanner::class)
                        ->needs(LoggerInterface::class)
                        ->give($directoryScannerLogger);
    }
}
