<?php

namespace WPStaging\Pro\Backup\Task;

use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Framework\Queue\Queue;
use WPStaging\Framework\Queue\Storage\CacheStorage;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Job\Jobs\JobImport;
use WPStaging\Pro\Backup\Task\ImportFileHandlers\ImportFileProcessor;
use WPStaging\Vendor\Psr\Log\LoggerInterface;

/**
 * Class FileImportTask
 *
 * This is an abstract class for the filesystem-based import actions of importing a site,
 * such as plugins, themes, mu-plugins and uploads files.
 *
 * It's main philosophy is to control the individual queue of what needs to be processed
 * from each of the concrete imports. It delegates actual processing of the queue to a separate class.
 *
 * @package WPStaging\Pro\Backup\Abstracts\Task
 */
abstract class FileImportTask extends ImportTask
{
    protected $filesystem;
    protected $directory;

    private $importQueue;
    private $importQueueStorage;
    private $importFileProcessor;

    public function __construct(LoggerInterface $logger, Cache $cache, StepsDto $stepsDto, Filesystem $filesystem, Directory $directory, Queue $importQueue, CacheStorage $importQueueStorage, ImportFileProcessor $importFileProcessor)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->filesystem          = $filesystem;
        $this->directory           = $directory;
        $this->importQueue         = $importQueue;
        $this->importQueueStorage  = $importQueueStorage;
        $this->importFileProcessor = $importFileProcessor;
    }

    public function prepareFileImport()
    {
        $this->importQueueStorage->setKey(static::getTaskName() . '_ImportQueue');
        #$this->importQueueStorage->getCache()->setPath(JobImport::getJobCacheDirectory());
        $this->importQueue->setStorage($this->importQueueStorage);

        if (!$this->queueExists()) {
            // Populate queue
            $this->buildQueue();

            $this->logger->info(sprintf(
                __('%s: Enqueued %d actions', 'wp-staging'),
                static::getTaskTitle(),
                $this->importQueue->count()
            ));
        }
    }

    /**
     * @return \WPStaging\Pro\Backup\Dto\TaskResponseDto
     */
    public function execute()
    {
        $this->prepareFileImport();

        if ($this->stepsDto->getTotal() === 0) {
            $this->stepsDto->setTotal($this->importQueue->count());
        }

        if ($this->importQueue->count() > 0) {
            $this->processNextItemInQueue();
        }

        return $this->generateResponse();
    }

    /**
     * Concrete classes of the FileImportTask must build
     * the queue once, enqueuing everything that needs
     * to be moved or deleted, using $this->enqueueMove
     * or $this->enqueueDelete.
     *
     * @return void
     */
    abstract protected function buildQueue();

    protected function getSafeExtractPath($extractPath)
    {
        return trailingslashit($this->filesystem->safePath($this->jobQueueDto->getTmpDirectory() . $extractPath));
    }

    /**
     * @return bool True if queue already exists. False if not.
     */
    private function queueExists()
    {
        return file_exists($this->importQueueStorage->getCache()->getFilePath());
    }

    /**
     * Executes the next item in the queue.
     */
    protected function processNextItemInQueue()
    {
        $nextInQueue = $this->importQueue->pop();

        // Make sure we read expected data from the queue
        if (!is_array($nextInQueue)) {
            $this->logger->warning(sprintf(
                __('%s: An internal error occurred that prevented this file from being imported. No changes have been made. (Error Code: INVALID_QUEUE_ITEM)', 'wp-staging'),
                static::getTaskTitle()
            ));
            $this->logger->debug(wp_json_encode($nextInQueue));

            return;
        }

        // Make sure data is in the expected format
        array_map(function ($requiredKey) use ($nextInQueue) {
            if (!array_key_exists($requiredKey, $nextInQueue)) {
                $this->logger->warning(sprintf(
                    __('%s: An internal error occurred that prevented this file from being imported. No changes have been made. (Error Code: INVALID_QUEUE_ITEM)', 'wp-staging'),
                    static::getTaskTitle()
                ));
                $this->logger->debug(wp_json_encode($nextInQueue));

                return;
            }
        }, ['action', 'source', 'destination']);

        $source = $nextInQueue['source'];

        // Make sure destination is within WordPress
        // @todo Test exporting in Windows and importing in Linux and vice-versa
        $destination = $nextInQueue['destination'];
        $destination = wp_normalize_path($destination);
        $destination = ABSPATH . str_replace(wp_normalize_path(ABSPATH), '', $destination);

        // Executes the action
        $this->importFileProcessor->handle($nextInQueue['action'], $source, $destination, $this, $this->logger);
    }

    /**
     * @param string $source          Source path to move.
     * @param string $destination     Where to move source to.
     * @param string $positionInQueue Controls the position of this action in the queue.
     */
    public function enqueueMove($source, $destination, $positionInQueue = 'last')
    {
        $this->enqueue([
            'action'      => 'move',
            'source'      => $source,
            'destination' => $destination,
        ], $positionInQueue);
    }

    /**
     * @param string $source          Source path to move.
     * @param string $destination     Where to move source to.
     * @param string $positionInQueue Controls the position of this action in the queue.
     */
    public function enqueueOverwrite($source, $destination, $positionInQueue = 'last')
    {
        $this->enqueue([
            'action'      => 'copy',
            'source'      => $source,
            'destination' => $destination,
        ], $positionInQueue);
    }

    /**
     * @param string $path            The path to delete. Can be a folder, which will be deleted recursively.
     * @param string $positionInQueue Controls the position of this action in the queue.
     */
    public function enqueueDelete($path, $positionInQueue = 'last')
    {
        $this->enqueue([
            'action'      => 'delete',
            'source'      => '',
            'destination' => $path,
        ], $positionInQueue);
    }

    /**
     * @param array  $action          An array of actions to perform.
     * @param string $positionInQueue Controls the position of this action in the queue.
     */
    private function enqueue($action, $positionInQueue)
    {
        if ($positionInQueue === 'last') {
            /*
             * Position "last" is the default.
             * Technically speaking, it's FIFO. First in, first out, like in a supermarket queue.
             */
            $this->importQueue->push($action);
        } elseif ($positionInQueue === 'first') {
            /*
             * Position "first", LIFO, Last in, first out.
             * Good for retrying an action, as if your credit card failed in the supermarket
             * and you're trying again with another machine without going back to the end of the queue.
             */
            $this->importQueue->prepend($action);
        }
    }
}
