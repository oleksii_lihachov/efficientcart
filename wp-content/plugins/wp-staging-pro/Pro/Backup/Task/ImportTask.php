<?php

namespace WPStaging\Pro\Backup\Task;

use WPStaging\Pro\Backup\Dto\Job\JobImportDataDto;

abstract class ImportTask extends AbstractTask
{
    /** @var JobImportDataDto */
    protected $jobQueueDto;
}
