<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobImport;

use WPStaging\Pro\Backup\Task\FileImportTask;

class ImportThemesTask extends FileImportTask
{
    public static function getTaskName()
    {
        return 'backup_site_import_themes';
    }

    public static function getTaskTitle()
    {
        return 'Importing Themes From Backup';
    }

    protected function buildQueue()
    {
        $themesToImport = $this->getThemesToImport();
        $existingThemes  = $this->getExistingThemes();

        $themeRoot = trailingslashit(get_theme_root());

        foreach ($themesToImport as $themeName => $themePath) {
            /*
             * Scenario: Importing a theme that already exists
             * 1. Backup old theme
             * 2. Import new theme
             * 3. Delete backup
             */
            if (array_key_exists($themeName, $existingThemes)) {
                $this->enqueueMove($existingThemes[$themeName], "$themeRoot$themeName.wpstgImport.original");
                $this->enqueueMove($themesToImport[$themeName], "$themeRoot$themeName");
                $this->enqueueDelete("$themeRoot$themeName.wpstgImport.original");
                continue;
            }

            /*
             * Scenario 2: Importing a theme that does not yet exist
             */
            $this->enqueueMove($themesToImport[$themeName], "$themeRoot$themeName");
        }
    }

    /**
     * @return array An array of paths of themes to import.
     */
    private function getThemesToImport()
    {
        $tmpDir = $this->getSafeExtractPath($this->jobQueueDto->getBackupMetadata()->getDirThemes());
        $tmpDir = (string)apply_filters('wpstg.import.themes.tmpDir', $tmpDir);

        return $this->findThemesInDir($tmpDir);
    }

    /**
     * @return array An array of paths of existing themes.
     */
    private function getExistingThemes()
    {
        $destDir = get_theme_root();
        $destDir = (string)apply_filters('wpstg.import.themes.destDir', $destDir);

        return $this->findThemesInDir($destDir);
    }

    /**
     * @param string $path Folder to look for themes, eg: '/var/www/wp-content/themes'
     *
     * @example [
     *              'twentynineteen' => '/var/www/wp-content/themes/twentynineteen',
     *              'twentytwenty' => '/var/www/wp-content/themes/twentytwenty',
     *          ]
     *
     * @return array An array of paths of themes found in the root of given directory,
     *               where the index is the name of the theme, and the value it's path.
     */
    private function findThemesInDir($path)
    {
        $it = new \DirectoryIterator($path);

        $themes = [];

        /** @var \DirectoryIterator $item */
        foreach ($it as $item) {
            if ($item->isDir()) {
                if (file_exists(trailingslashit($item->getPathname()) . 'style.css')) {
                    $themes[$item->getBasename()] = $item->getPathname();
                }
            }
        }

        return $themes;
    }
}
