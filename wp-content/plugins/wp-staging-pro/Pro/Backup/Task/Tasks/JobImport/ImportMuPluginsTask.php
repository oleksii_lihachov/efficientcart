<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobImport;

use WPStaging\Pro\Backup\Task\FileImportTask;

class ImportMuPluginsTask extends FileImportTask
{
    public static function getTaskName()
    {
        return 'backup_site_import_muPlugins';
    }

    public static function getTaskTitle()
    {
        return 'Importing Mu-Plugins From Backup';
    }

    protected function buildQueue()
    {
        $muPluginsToImport = $this->getMuPluginsToImport();
        $existingMuPlugins  = $this->getExistingMuPlugins();

        $muPluginRoot = trailingslashit(WPMU_PLUGIN_DIR);

        foreach ($muPluginsToImport as $muPluginSlug => $muPluginPath) {
            /*
             * Scenario: Importing a mu-plugin that already exists
             * 1. Backup old mu-plugin
             * 2. Import new mu-plugin
             * 3. Delete backup
             */
            if (array_key_exists($muPluginSlug, $existingMuPlugins)) {
                $this->enqueueMove($existingMuPlugins[$muPluginSlug], "$muPluginRoot$muPluginSlug.wpstgImport.original");
                $this->enqueueMove($muPluginsToImport[$muPluginSlug], "$muPluginRoot$muPluginSlug");
                $this->enqueueDelete("$muPluginRoot$muPluginSlug.wpstgImport.original");
                continue;
            }

            /*
             * Scenario 2: Importing a plugin that does not yet exist
             */
            $this->enqueueMove($muPluginsToImport[$muPluginSlug], "$muPluginRoot$muPluginSlug");
        }
    }

    /**
     * @return array An array of paths of mu-plugins to import.
     */
    private function getMuPluginsToImport()
    {
        $tmpDir = $this->getSafeExtractPath($this->jobQueueDto->getBackupMetadata()->getDirMuPlugins());
        $tmpDir = (string)apply_filters('wpstg.import.muPlugins.tmpDir', $tmpDir);

        return $this->findMuPluginsInDir($tmpDir);
    }

    /**
     * @return array An array of paths of existing mu-plugins.
     */
    private function getExistingMuPlugins()
    {
        $destDir = WPMU_PLUGIN_DIR;
        $destDir = (string)apply_filters('wpstg.import.muPlugins.destDir', $destDir);

        return $this->findMuPluginsInDir($destDir);
    }

    /**
     * @param string $path Folder to look for mu-plugins, eg: '/var/www/wp-content/mu-plugins'
     *
     * @example [
     *              'foo' => '/var/www/wp-content/mu-plugins/foo',
     *              'foo.php' => '/var/www/wp-content/mu-plugins/foo.php',
     *          ]
     *
     * @return array An array of paths of mu-plugins found in the root of given directory,
     *               where the index is the name of the mu-plugin, and the value it's path.
     */
    private function findMuPluginsInDir($path)
    {
        $it = new \DirectoryIterator($path);

        $plugins = [];

        /** @var \DirectoryIterator $fileInfo */
        foreach ($it as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }

            if ($fileInfo->isLink()) {
                continue;
            }

            // wp-content/plugins/foo
            if ($fileInfo->isDir()) {
                $plugins[$fileInfo->getBasename()] = $fileInfo->getPathname();

                continue;
            }

            // wp-content/plugins/foo.php
            if ($fileInfo->isFile() && $fileInfo->getExtension() === 'php' && $fileInfo->getBasename() !== 'index.php') {
                $plugins[$fileInfo->getBasename()] = $fileInfo->getPathname();

                continue;
            }
        }

        return $plugins;
    }
}
