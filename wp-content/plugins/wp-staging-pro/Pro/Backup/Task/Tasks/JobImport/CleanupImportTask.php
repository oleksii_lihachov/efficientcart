<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobImport;

use WPStaging\Framework\Adapter\Directory;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Job\Jobs\JobImport;
use WPStaging\Pro\Backup\Task\ImportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Framework\Filesystem\Filesystem;

class CleanupImportTask extends ImportTask
{
    private $filesystem;
    private $directory;

    public function __construct(LoggerInterface $logger, Cache $cache, StepsDto $stepsDto, Filesystem $filesystem, Directory $directory)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->filesystem = $filesystem;
        $this->directory  = $directory;
    }

    public static function getTaskName()
    {
        return 'backup_site_import_cleanUp';
    }

    public static function getTaskTitle()
    {
        return 'Cleaning Up Extracted Import Files';
    }

    public function execute()
    {
        $this->prepareCleanupImportTask();

        // Make sure path to clean is inside WordPress
        $pathToClean = trailingslashit(wp_normalize_path(ABSPATH)) . str_replace(wp_normalize_path(ABSPATH), '', $this->jobQueueDto->getToClean());

        $pathToClean = untrailingslashit($pathToClean);

        // Clear the .sql file used during the import
        $databaseFile = ABSPATH . $this->jobQueueDto->getBackupMetadata()->getDatabaseFile();
        if (file_exists($databaseFile)) {
            unlink($databaseFile);
        }

        if (!file_exists($pathToClean)) {
            // We don't care if $pathToClean is a file or a dir
            $this->logger->info(sprintf(
                __('%s: No need to cleanup %s, as path does not exist.', 'wp-staging'),
                static::getTaskTitle(),
                $pathToClean
            ));
        } else {
            try {
                $deleted = $this->filesystem
                    ->setRecursive(true)
                    ->setShouldStop(function () {
                        return $this->isThreshold();
                    })
                    ->deleteNew($pathToClean);
            } catch (\Exception $e) {
                $this->logger->warning(sprintf(
                    __('%s: Could not cleanup path %s. May be a permission issue?', 'wp-staging'),
                    static::getTaskTitle(),
                    $pathToClean
                ));
            }

            if ($deleted) {
                // Successfully deleted
                $this->logger->info(sprintf(
                    __('%s: Path %s successfully cleaned up.', 'wp-staging'),
                    static::getTaskTitle(),
                    $pathToClean
                ));
            } else {
                /*
                 * Not successfully deleted.
                 * This can happen if the folder to delete is too large
                 * to be deleted in a single request. We continue
                 * deleting it in the next request...
                 */
                $response = $this->generateResponse();
                $response->setStatus(false);
                $this->stepsDto->setCurrent(1);

                $this->logger->info(sprintf(
                    __('%s: Re-enqueing path %s for deletion, as it couldn\'t be deleted in a single request without
                        hitting execution limits. If you see this message in a loop, PHP might not be able to delete
                        this directory, so you might want to try to delete it manually.', 'wp-staging'),
                    static::getTaskTitle(),
                    $pathToClean
                ));

                // Early bail: Response modified for repeating
                return $response;
            }
        }

        return $this->generateResponse();
    }

    public function prepareCleanupImportTask()
    {
        if ($this->stepsDto->getTotal() === 1) {
            return;
        }

        $this->jobQueueDto->setToClean(trailingslashit($this->directory->getPluginUploadsDirectory()) . JobImport::TMP_DIRECTORY);

        $this->stepsDto->setTotal(1);
    }
}
