<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobImport;

use WPStaging\Pro\Backup\Task\FileImportTask;

class ImportPluginsTask extends FileImportTask
{
    public static function getTaskName()
    {
        return 'backup_site_import_plugins';
    }

    public static function getTaskTitle()
    {
        return 'Importing Plugins From Backup';
    }

    protected function buildQueue()
    {
        $pluginsToImport = $this->getPluginsToImport();
        $existingPlugins  = $this->getExistingPlugins();

        $pluginRoot = trailingslashit(WP_PLUGIN_DIR);

        foreach ($pluginsToImport as $pluginSlug => $pluginPath) {
            /*
             * Scenario: Importing a plugin that already exists
             * 1. Backup old plugin
             * 2. Import new plugin
             * 3. Delete backup
             */
            if (array_key_exists($pluginSlug, $existingPlugins)) {
                $this->enqueueMove($existingPlugins[$pluginSlug], "$pluginRoot$pluginSlug.wpstgImport.original");
                $this->enqueueMove($pluginsToImport[$pluginSlug], "$pluginRoot$pluginSlug");
                $this->enqueueDelete("$pluginRoot$pluginSlug.wpstgImport.original");
                continue;
            }

            /*
             * Scenario 2: Importing a plugin that does not yet exist
             */
            $this->enqueueMove($pluginsToImport[$pluginSlug], "$pluginRoot$pluginSlug");
        }
    }

    /**
     * @return array An array of paths of plugins to import.
     */
    private function getPluginsToImport()
    {
        $tmpDir = $this->getSafeExtractPath($this->jobQueueDto->getBackupMetadata()->getDirPlugins());
        $tmpDir = (string)apply_filters('wpstg.import.plugins.tmpDir', $tmpDir);

        return $this->findPluginsInDir($tmpDir);
    }

    /**
     * @return array An array of paths of existing plugins.
     */
    private function getExistingPlugins()
    {
        $destDir = WP_PLUGIN_DIR;
        $destDir = (string)apply_filters('wpstg.import.plugins.destDir', $destDir);

        return $this->findPluginsInDir($destDir);
    }

    /**
     * @param string $path Folder to look for plugins, eg: '/var/www/wp-content/plugins'
     *
     * @example [
     *              'foo' => '/var/www/wp-content/plugins/foo',
     *              'foo.php' => '/var/www/wp-content/plugins/foo.php',
     *          ]
     *
     * @return array An array of paths of plugins found in the root of given directory,
     *               where the index is the name of the plugin, and the value it's path.
     */
    private function findPluginsInDir($path)
    {
        $it = new \DirectoryIterator($path);

        $plugins = [];

        /** @var \DirectoryIterator $fileInfo */
        foreach ($it as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }

            if ($fileInfo->isLink()) {
                continue;
            }

            // wp-content/plugins/foo
            if ($fileInfo->isDir()) {
                $plugins[$fileInfo->getBasename()] = $fileInfo->getPathname();

                continue;
            }

            // wp-content/plugins/foo.php
            if ($fileInfo->isFile() && $fileInfo->getExtension() === 'php' && $fileInfo->getBasename() !== 'index.php') {
                $plugins[$fileInfo->getBasename()] = $fileInfo->getPathname();

                continue;
            }
        }

        return $plugins;
    }
}
