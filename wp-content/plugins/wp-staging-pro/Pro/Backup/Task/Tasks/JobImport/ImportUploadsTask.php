<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobImport;

use WPStaging\Pro\Backup\Task\FileImportTask;

class ImportUploadsTask extends FileImportTask
{
    public static function getTaskName()
    {
        return 'backup_site_import_uploads';
    }

    public static function getTaskTitle()
    {
        return 'Importing Uploads Folder';
    }

    /**
     * The most critical step because it has to run in one request
     */
    protected function buildQueue()
    {
        $uploadsToImport = $this->getUploadsToImport();

        $uploadsRoot = trailingslashit($this->directory->getUploadsDirectory());

        foreach ($uploadsToImport as $id => $fileInfo) {
            /*
             * Scenario: Importing an upload that already exists
             * 1. Backup old upload
             * 2. Import new upload
             * 3. Delete backup
             */
            $this->enqueueOverwrite($uploadsToImport[$id]['path'], $uploadsRoot . $fileInfo['relativePath']);
        }
    }

    /**
     * @return array An array of paths of uploads to import.
     */
    private function getUploadsToImport()
    {
        $tmpDir = $this->getSafeExtractPath($this->jobQueueDto->getBackupMetadata()->getDirUploads());
        $tmpDir = (string)apply_filters('wpstg.import.uploads.tmpDir', $tmpDir);

        return $this->findUploadsInDir($tmpDir);
    }

    /**
     * @param string $path Folder to look for uploads, eg: '/var/www/wp-content/uploads'
     *
     * @return array An array of paths of uploads found in the root of given directory,
     *               where the index is the relative path of the upload, and the value it's absolute path.
     * @example [
     *              '2020/01/image.jpg' => '/var/www/wp-content/uploads/2020/01/image.jpg',
     *              'debug.log' => '/var/www/wp-content/uploads/debug.log',
     *          ]
     *
     */
    private function findUploadsInDir($path)
    {
        $path = untrailingslashit($path);
        $it   = new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS);
        $it   = new \RecursiveIteratorIterator($it);

        $uploads = [];

        /** @var \SplFileInfo $item */
        foreach ($it as $item) {
            // Early bail: We don't want dots, links or anything that is not a file.
            if (!$item->isFile() || $item->isLink()) {
                continue;
            }

            // Allocate pathname to a variable because we use it multiple times below.
            $pathName = $item->getPathname();

            $relativePath = str_replace($path, '', $pathName);

            $uploads[] = [
                'path'         => $pathName,
                'relativePath' => $relativePath,
            ];
        }

        return $uploads;
    }
}
