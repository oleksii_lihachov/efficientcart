<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobImport;

use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Service\Database\DatabaseExporter;
use WPStaging\Pro\Backup\Service\Database\DatabaseImporter;
use WPStaging\Pro\Backup\Task\ImportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Framework\Traits\MaintenanceTrait;
use WPStaging\Framework\Database\SearchReplace;

class ImportDatabaseTask extends ImportTask
{
    //use MaintenanceTrait;

    private $filesystem;

    /** @var DatabaseImporter */
    private $databaseImport;

    public function __construct(DatabaseImporter $databaseImport, Filesystem $filesystem, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        //$this->skipMaintenanceMode();
        $this->databaseImport = $databaseImport;
        $this->databaseImport->setLogger($this->logger);

        $this->filesystem = $filesystem;
    }

    public static function getTaskName()
    {
        return 'backup_site_import_database';
    }

    public static function getTaskTitle()
    {
        return 'Importing Database';
    }

    public function __destruct()
    {
        //$this->enableMaintenance(false);
    }

    public function init()
    {
        //$this->enableMaintenance(true);
    }

    public function execute()
    {
        $this->prepare();
        $this->databaseImport->import();

        $steps = $this->stepsDto;
        $steps->setCurrent($this->databaseImport->getCurrentLine());
        $this->logger->info(sprintf('Executed %d/%d queries', $steps->getCurrent(), $steps->getTotal()));

        return $this->generateResponse();
    }

    public function prepare()
    {
        $databaseFile = ABSPATH . $this->jobQueueDto->getBackupMetadata()->getDatabaseFile();

        if (!file_exists($databaseFile)) {
            // todo: This should kill the import process, but it only hangs without showing the message to the user
            $this->logger->critical(__('Could not find database file to import.', 'wp-staging'));
            throw new \RuntimeException();
        }

        $this->databaseImport->setShouldStop([$this, 'isThreshold']);
        $this->databaseImport->setFile($databaseFile);
        $this->databaseImport->seekLine($this->stepsDto->getCurrent());

        if (!$this->stepsDto->getTotal()) {
            $this->stepsDto->setTotal($this->databaseImport->getTotalLines());
        }

        $searchDefault = [
            DatabaseExporter::DB_REPLACE_SITE_URL,
            DatabaseExporter::DB_REPLACE_ABSPATH,
        ];

        $replaceDefault = [
            get_site_url(),
            ABSPATH,
        ];

        $search  = array_merge($searchDefault, $this->jobQueueDto->getSearch());
        $replace = array_merge($replaceDefault, $this->jobQueueDto->getReplace());

        $searchReplace = (new SearchReplace())
            ->setSearch($search)
            ->setReplace($replace);

        $this->databaseImport->setSearchReplace($searchReplace);
    }
}
