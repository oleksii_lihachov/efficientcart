<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobImport;

use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Entity\BackupMetadata;
use WPStaging\Pro\Backup\Task\ImportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use RuntimeException;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Pro\Backup\Service\Extractor;
use WPStaging\Pro\Backup\Dto\Service\ExtractorDto;

class ExtractFilesTask extends ImportTask
{
    /** @var Extractor */
    protected $extractorService;

    public function __construct(Extractor $extractor, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->extractorService = $extractor;
    }

    public static function getTaskName()
    {
        return 'backup_site_extract_files';
    }

    public static function getTaskTitle()
    {
        return 'Extracting Files';
    }

    public function execute()
    {
        try {
            $extractDto = $this->provideExtractDto();
        } catch (RuntimeException $e) {
            $this->logger->critical($e->getMessage());

            return $this->generateResponse();
        }

        $this->stepsDto->setTotal($extractDto->getBackupMetadata()->getTotalFiles());
        $this->extractorService->setDto($extractDto, $this->jobQueueDto->getTmpDirectory());
        $this->extractorService->extract([$this, 'isThreshold']);

        $this->stepsDto->setCurrent($extractDto->getProcessedFiles());

        $this->logger->info(sprintf('Extracted %d/%d files', $this->stepsDto->getCurrent(), $this->stepsDto->getTotal()));

        return $this->generateResponse();
    }

    protected function provideExtractDto()
    {
        try {
            $backupMetadata = (new BackupMetadata())->hydrateByFilePath($this->jobQueueDto->getFile());
        } catch (\Exception $e) {
            throw $e;
        }
        $extractDto = new ExtractorDto();
        $extractDto->setId($this->jobQueueDto->getId());
        $extractDto->setBackupMetadata($backupMetadata);
        $extractDto->setFullPath($this->jobQueueDto->getFile());

        /** @todo: Is setHeaderStartsAt being set at all? */
        $extractDto->setSeekToHeader($this->jobQueueDto->getHeaderStartsAt());

        /** @todo: Is setFileStartsAt being set at all? */
        $extractDto->setSeekToFile($this->jobQueueDto->getFileStartsAt());

        return $extractDto;
    }
}
