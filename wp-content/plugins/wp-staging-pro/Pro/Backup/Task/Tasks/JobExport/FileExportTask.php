<?php

// TODO PHP7.x; declare(strict_type=1);
// TODO PHP7.x; type hints & return types
// TODO PHP7.1; constant visibility

namespace WPStaging\Pro\Backup\Task\Tasks\JobExport;

use Exception;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Task\ExportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Pro\Backup\Service\Compressor;

class FileExportTask extends ExportTask
{
    /** @var Compressor */
    private $compressor;

    public function __construct(Compressor $compressor, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->compressor = $compressor;
    }

    public static function getTaskName()
    {
        return 'backup_export_file_export';
    }

    public static function getTaskTitle()
    {
        return 'Exporting %d Files';
    }

    public function execute()
    {
        $this->prepareFileExportTask();

        while ($this->shouldContinue()) {
            $this->export();
        }

        $steps = $this->stepsDto;
        $steps->setCurrent($this->stepsDto->getCurrent());
        $this->logger->info(sprintf('Exported %d files', $steps->getCurrent()));

        return $this->generateResponse();
    }

    public function export()
    {
        $compressorDto = $this->compressor->getDto();
        $compressorDto->setOffset($this->jobQueueDto->getOffset());
        $compressorDto->setFilePath($this->jobQueueDto->getCurrentFile());

        $status = false;
        try {
            $status = $this->compressor->export();
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        if ($status !== false) {
            $this->stepsDto->incrementCurrentStep();
        }

        $this->jobQueueDto->setOffset($compressorDto->getOffset());
        $this->jobQueueDto->setCurrentFile($compressorDto->getRelativeFilePath());
    }

    private function prepareFileExportTask()
    {
        if ($this->stepsDto->getTotal() > 0) {
            return;
        }
        $this->stepsDto->setTotal($this->jobQueueDto->getTotalFiles());
    }

    /**
     * @inheritDoc
     */
    public function getStatusTitle(array $args = [])
    {
        if ($this->jobQueueDto && $this->stepsDto) {
            $total = $this->stepsDto->getTotal();
        } else {
            $total = isset($args[0]) ? (int)$args[0] : 0;
        }

        return sprintf(__(static::getTaskTitle(), 'wp-staging'), $total);
    }

    public function getCaches()
    {
        $caches = parent::getCaches();
        $caches[] = $this->compressor->getQueue()->getStorage()->getCache();
        $caches[] = $this->compressor->getCacheIndex();
        $caches[] = $this->compressor->getCacheCompressed();

        return $caches;
    }

    /**
     * @return bool
     */
    private function shouldContinue()
    {
        return !$this->isThreshold() && !$this->stepsDto->isFinished();
    }
}
