<?php

// TODO PHP7.x; declare(strict_type=1);
// TODO PHP7.x; type hints & return types
// TODO PHP7.1; constant visibility

namespace WPStaging\Pro\Backup\Task\Tasks\JobExport;

use Exception;
use InvalidArgumentException;
use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Interfaces\ShutdownableInterface;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Task\ExportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Framework\Queue\FinishedQueueException;
use WPStaging\Framework\Utils\Cache\BufferedCache;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Framework\Filesystem\DirectoryScannerControl;

/**
 * Class DirectoryScannerTask
 *
 * @see     DirectoryScannerControl
 *
 * @package WPStaging\Component\Task\Filesystem
 */
class DirectoryScannerTask extends ExportTask implements ShutdownableInterface
{
    /** @var DirectoryScannerControl */
    private $scannerControl;

    /** @var BufferedCache */
    private $directoryCache;

    /** @var array */
    private $directories;

    /** @var Directory */
    protected $directory;

    public function __construct(DirectoryScannerControl $scannerControl, Directory $directory, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->directory   = $directory;
        $this->directories = [];

        $this->scannerControl = $scannerControl;
        $this->scannerControl->setQueueByName();

        $this->directoryCache = clone $scannerControl->getCache();
        $this->directoryCache->setLifetime(DAY_IN_SECONDS);
        $this->directoryCache->setFilename(DirectoryScannerControl::DATA_CACHE_FILE);
    }

    public function onWpShutdown()
    {
        if ($this->directories) {
            $this->directoryCache->append($this->directories);
        }
    }

    public static function getTaskName()
    {
        return 'backup_export_directory_scan';
    }

    public static function getTaskTitle()
    {
        return 'Scanning Directories';
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $this->prepareDirectoryScanner();
        while ($this->shouldContinue()) {
            $this->scanCurrentDirectory();
        }
        $this->updateSteps();
        $this->logger->info(sprintf('Scanned %d directories', $this->stepsDto->getTotal()));

        return $this->generateResponse();
    }

    protected function scanCurrentDirectory()
    {
        $directories = null;
        try {
            $directories = $this->scannerControl->scanCurrentPath($this->jobQueueDto->getExcludedDirectories());
        } catch (FinishedQueueException $e) {
            $this->logger->info('Finished scanning directories');
            $this->stepsDto->finish();

            return;
        } catch (InvalidArgumentException $e) {
            // This happens when a symlink is there and we don't follow them.
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }

        // No directories found here, skip it
        if (empty($directories)) {
            return;
        }

        foreach ($directories as $directory) {
            if ($this->isThreshold()) {
                return;
            }
            $relativePath = str_replace(ABSPATH, null, $directory);
            $this->scannerControl->addToNewQueue($relativePath);
            $this->directories[] = $relativePath;
        }
        $this->directories = array_unique($this->directories);
    }

    protected function prepareDirectoryScanner()
    {
        if ($this->stepsDto->getTotal() > 0) {
            return;
        }

        $this->jobQueueDto->setIncludedDirectories([WP_CONTENT_DIR]);

        $this->directories = array_map(static function ($dir) {
            return str_replace(ABSPATH, '', $dir);
        }, $this->jobQueueDto->getIncludedDirectories());

        /** @noinspection NullPointerExceptionInspection */
        if ($this->scannerControl->getQueue()->count() < 1) {
            $this->scannerControl->setNewQueueItems($this->directories);
        }

        $totalSteps = count($this->jobQueueDto->getIncludedDirectories());
        if ($totalSteps < 0) {
            $totalSteps = 0;
        }
        $this->stepsDto->setTotal($totalSteps);

        // Exclude WP Staging related directories and Cache dir
        // There is no need to backup WP Staging as you shouldn't import WPSTG backup without WPSTG having installed
        // Don't backup / restore cache as it can be problematic
        $excludedDirs = $this->jobQueueDto->getExcludedDirectories();

        $excludedDirs[] = WPSTG_PLUGIN_DIR;
        $excludedDirs[] = $this->scannerControl->getDirectory()->getPluginUploadsDirectory();
        $excludedDirs[] = WP_CONTENT_DIR . '/cache';


        if (!$this->jobQueueDto->getIsExportingPlugins()) {
            $excludedDirs[] = WP_PLUGIN_DIR;
        }

        if (!$this->jobQueueDto->getIsExportingThemes()) {
            $excludedDirs[] = get_theme_root();
        }

        if (!$this->jobQueueDto->getIsExportingMuPlugins()) {
            $excludedDirs[] = WPMU_PLUGIN_DIR;
        }

        if (!$this->jobQueueDto->getIsExportingUploads()) {
            $excludedDirs[] = $this->directory->getPluginUploadsDirectory();
        }

        /**
         * Allow user to filter the excluded directories in a site export.
         *
         * @param array $excludedDirectories
         *
         * @return array An array of directories to exclude.
         */
        $excludedDirs = (array)apply_filters('wpstg.export.site.ignore.directories', $excludedDirs);

        $this->jobQueueDto->setExcludedDirectories($excludedDirs);
    }

    protected function getCaches()
    {
        $caches   = parent::getCaches();
        $caches[] = $this->directoryCache;
        $caches[] = $this->scannerControl->getCache();
        /** @noinspection NullPointerExceptionInspection */
        $caches[] = $this->scannerControl->getQueue()->getStorage()->getCache();

        return $caches;
    }

    /**
     * @return bool
     */
    private function shouldContinue()
    {
        return !$this->isThreshold() && !$this->stepsDto->isFinished();
    }

    private function updateSteps()
    {
        // Update steps
        $steps = $this->stepsDto;
        $steps->setTotal($steps->getTotal() + count($this->directories));

        /** @noinspection NullPointerExceptionInspection */
        if ($this->scannerControl->getQueue()->count() > 0) {
            return;
        }

        $total = $steps->getTotal() - count($this->jobQueueDto->getIncludedDirectories());
        $total = $total >= 0 ? $total : 0;

        $steps->setTotal($total);
        $steps->setCurrent($total);
    }
}
