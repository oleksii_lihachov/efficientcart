<?php

namespace WPStaging\Pro\Backup\Task\Tasks\JobExport;

use DateTime;
use Exception;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Dto\Task\Export\Response\DatabaseExportResponseDto;
use WPStaging\Pro\Backup\Task\ExportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Pro\Backup\Dto\TaskResponseDto;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Framework\Adapter\Database;
use WPStaging\Framework\Database\TableService;
use WPStaging\Pro\Backup\Service\Database\DatabaseExporter;
use WPStaging\Pro\Backup\Service\Compressor;
use WPStaging\Pro\Backup\Dto\Service\CompressorDto;

class DatabaseExportTask extends ExportTask
{
    const FILE_FORMAT = 'sql';

    /**  @var DatabaseExporter */
    private $databaseExporter;

    /** @var TableService */
    private $tableService;

    /** @var Compressor */
    private $compressor;

    /** @var CompressorDto */
    private $compressorDto;

    public function __construct(Compressor $compressor, DatabaseExporter $databaseExporter, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->compressor       = $compressor;
        $this->compressorDto    = $this->compressor->getDto();
        $this->databaseExporter = $databaseExporter;
        $this->tableService     = new TableService(new Database());
    }

    public static function getTaskName()
    {
        return 'backup_export_database';
    }

    public static function getTaskTitle()
    {
        return 'Export database';
    }

    /**
     * @return object|TaskResponseDto
     * @throws Exception
     */
    public function execute()
    {
        $this->setTimeLimit(DatabaseExporter::MAX_EXECUTION_TIME_SECONDS);

        $result = $this->generateSqlFile();

        $this->stepsDto->setCurrent($this->databaseExporter->getTableIndex());
        $this->jobQueueDto->setTableRowsExported($this->databaseExporter->getTableRowsExported());
        $this->jobQueueDto->setTableRowsOffset($this->databaseExporter->getTableRowsOffset());

        $this->writeLog();

        $response = $this->generateResponse();

        if (!$result) {
            // Not finished - continue with current table
            $this->stepsDto->setCurrent($this->databaseExporter->getTableIndex());

            return $response;
        }

        $this->stepsDto->setTotal(0);
        $response->setFilePath($result);

        return $response;
    }

    /**
     * @param null|string $filePath
     *
     * @return DatabaseExportResponseDto
     */
    public function generateResponse($filePath = null)
    {
        /** @var DatabaseExportResponseDto $response */
        $response = parent::generateResponse();
        $response->setFilePath($filePath);

        return $response;
    }

    protected function getResponseDto()
    {
        return new DatabaseExportResponseDto();
    }

    protected function writeLog()
    {
        if ($this->jobQueueDto && $this->jobQueueDto->getTableRowsExported()) {
            $this->logger->info(sprintf(__('Exporting database... %s records saved', 'wp-staging'), number_format_i18n($this->jobQueueDto->getTableRowsExported())));
        } else {
            $this->logger->info('Exporting database...');
        }
    }

    /**
     * @return string|null
     */
    protected function generateSqlFile()
    {
        $this->databaseExporter->setTables($this->getIncludeTables());
        $this->databaseExporter->setFileName($this->getDatabaseFileName());
        $this->databaseExporter->setTableIndex($this->stepsDto->getCurrent());
        $this->databaseExporter->setTableRowsOffset($this->jobQueueDto->getTableRowsOffset());
        $this->databaseExporter->setTableRowsExported($this->jobQueueDto->getTableRowsExported());

        $this->stepsDto->setTotal(count($this->getIncludeTables()) + 1);

        try {
            return $this->databaseExporter->export([$this, 'isThreshold']);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());

            return false;
        }
    }

    /**
     * @return array
     */
    protected function getIncludeTables()
    {
        $tables = $this->tableService->findTableNamesStartWith($this->databaseExporter->getDatabase()->getPrefix());
        $views  = $this->tableService->findViewsNamesStartWith($this->databaseExporter->getDatabase()->getPrefix());
        // Add views to bottom of the array to make sure they can be created. Views are based on tables. So tables need to be created before views
        $tablesAndViews = array_merge($tables, $views);

        return $tablesAndViews;
    }

    /**
     * @return string
     */
    private function getDatabaseFileName()
    {
        if (!$this->jobQueueDto->getDatabaseFile()) {
            $this->jobQueueDto->setDatabaseFile(sprintf(
                '%s_%s_%s.%s',
                rtrim($this->databaseExporter->getDatabase()->getPrefix(), '_-'),
                (new DateTime())->format('Y-m-d_H-i-s'),
                md5(mt_rand()),
                self::FILE_FORMAT
            ));
        }

        $this->compressorDto->setFilePath($this->compressor->findDestinationDirectory() . $this->jobQueueDto->getDatabaseFile());

        return $this->compressor->findDestinationDirectory() . $this->jobQueueDto->getDatabaseFile();
    }
}
