<?php

// TODO PHP7.x; declare(strict_type=1);
// TODO PHP7.x; type hints & return types
// TODO PHP7.1; constant visibility

namespace WPStaging\Pro\Backup\Task\Tasks\JobExport;

use Exception;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Entity\ListableBackup;
use WPStaging\Pro\Backup\Dto\Task\Export\Response\CombineExportResponseDto;
use WPStaging\Pro\Backup\Task\ExportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Pro\Backup\Service\Compressor;

class CombineExportTask extends ExportTask
{
    /** @var Compressor */
    private $compressor;

    // TODO reduce args
    public function __construct(Compressor $compressor, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->compressor = $compressor;
    }

    public static function getTaskName()
    {
        return 'backup_export_combine';
    }

    public static function getTaskTitle()
    {
        return 'Finalizing Backup Export';
    }

    public function execute()
    {
        $compressorDto = $this->compressor->getDto();
        $compressorDto->setOffset($this->stepsDto->getCurrent());

        $backupMetadata = $compressorDto->getBackupMetadata();
        $backupMetadata->setTotalDirectories($this->jobQueueDto->getTotalDirectories());
        $backupMetadata->setTotalFiles($this->jobQueueDto->getTotalFiles());
        $backupMetadata->setDatabaseFile($this->transformAbsolutePathToRelative($this->jobQueueDto->getDatabaseFile()));
        $backupMetadata->setName($this->jobQueueDto->getName() ?: __('Backup', 'wp-staging'));
        $backupMetadata->setIsAutomatedBackup($this->jobQueueDto->getIsAutomatedBackup());

        $backupMetadata->setIsExportingPlugins($this->jobQueueDto->getIsExportingPlugins());
        $backupMetadata->setIsExportingMuPlugins($this->jobQueueDto->getIsExportingMuPlugins());
        $backupMetadata->setIsExportingThemes($this->jobQueueDto->getIsExportingThemes());
        $backupMetadata->setIsExportingUploads($this->jobQueueDto->getIsExportingUploads());
        $backupMetadata->setIsExportingOtherWpContentFiles($this->jobQueueDto->getIsExportingOtherWpContentFiles());
        $backupMetadata->setIsExportingDatabase($this->jobQueueDto->getIsExportingDatabase());

        $exportFilePath = null;
        try {
            $exportFilePath = $this->compressor->combine();
        } catch (Exception $e) {
            $this->logger->critical('Failed to generate backup file: ' . $e->getMessage());
        }

        if ($exportFilePath) {
            $this->stepsDto->finish();

            return $this->generateResponse($this->makeListableBackup($exportFilePath));
        }

        $steps = $this->stepsDto;
        $steps->setCurrent($compressorDto->getOffset());
        $steps->setTotal($compressorDto->getFileSize());

        $this->logger->info(sprintf('Written %d bytes to compressed export', $compressorDto->getWrittenBytes()));

        return $this->generateResponse();
    }

    private function transformAbsolutePathToRelative($path)
    {
        return str_replace(ABSPATH, '', $path);
    }

    /**
     * @param null|ListableBackup $backup
     *
     * @return CombineExportResponseDto
     */
    public function generateResponse(ListableBackup $backup = null)
    {
        /** @var CombineExportResponseDto $response */
        $response = parent::generateResponse();
        $response->setBackupMd5($backup ? $backup->md5BaseName : null);
        $response->setBackupSize($backup ? size_format($backup->size) : null);

        return $response;
    }

    public function getCaches()
    {
        $caches   = parent::getCaches();
        $caches[] = $this->compressor->getCacheIndex();
        $caches[] = $this->compressor->getCacheCompressed();

        return $caches;
    }

    protected function getResponseDto()
    {
        return new CombineExportResponseDto();
    }

    /**
     * This is used to display the "Download Modal" after the backup completes.
     *
     * @see string src/Backend/public/js/wpstg-admin.js, search for "wpstg--backups--export"
     *
     * @param string $exportFilePath
     *
     * @return ListableBackup
     */
    protected function makeListableBackup($exportFilePath)
    {
        clearstatcache();
        $backup              = new ListableBackup();
        $backup->md5BaseName = md5(basename($exportFilePath));
        $backup->size        = filesize($exportFilePath);

        return $backup;
    }
}
