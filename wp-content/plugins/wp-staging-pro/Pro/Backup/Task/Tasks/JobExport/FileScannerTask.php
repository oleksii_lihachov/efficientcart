<?php

// TODO PHP7.x; declare(strict_type=1);
// TODO PHP7.x; type hints & return types
// TODO PHP7.1; constant visibility

namespace WPStaging\Pro\Backup\Task\Tasks\JobExport;

use Exception;
use WPStaging\Framework\Interfaces\ShutdownableInterface;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Task\ExportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use RuntimeException;
use WPStaging\Framework\Queue\FinishedQueueException;
use WPStaging\Framework\Queue\Storage\BufferedCacheStorage;
use WPStaging\Framework\Utils\Cache\AbstractCache;
use WPStaging\Framework\Utils\Cache\BufferedCache;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Framework\Filesystem\DirectoryScannerControl;
use WPStaging\Framework\Filesystem\FileScannerControl;
use WPStaging\Framework\Filesystem\Filesystem;

class FileScannerTask extends ExportTask implements ShutdownableInterface
{
    /** @var FileScannerControl */
    private $scannerControl;

    /** @var BufferedCache */
    private $fileCache;

    /** @var array */
    private $files;

    public function __construct(FileScannerControl $scannerControl, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->files = [];

        $this->scannerControl = $scannerControl;
        $this->scannerControl->setQueueByName();

        $this->fileCache = clone $scannerControl->getCache();
        $this->fileCache->setLifetime(DAY_IN_SECONDS);
        $this->fileCache->setFilename(FileScannerControl::DATA_CACHE_FILE);
    }

    public function onWpShutdown()
    {
        if ($this->files) {
            $this->fileCache->append($this->files);
        }
    }

    public static function getTaskName()
    {
        return 'backup_export_file_scan';
    }

    public static function getTaskTitle()
    {
        return 'Scanning Files in %d Directories';
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $this->prepareFileScanner();

        while ($this->shouldContinue()) {
            $this->scanCurrentDirectory();
        }

        $this->updateSteps();
        $this->logger->info(sprintf('Scanned %d files', $this->stepsDto->getTotal()));
        return $this->generateResponse();
    }

    /**
     * @inheritDoc
     */
    public function getStatusTitle(array $args = [])
    {
        $total = isset($args[0]) ? $args[0] : 0;
        if ($this->jobQueueDto && $this->stepsDto) {
            $total = $this->stepsDto->getTotal();
        }
        return sprintf(__(static::getTaskTitle(), 'wp-staging'), $total);
    }

    protected function scanCurrentDirectory()
    {
        $files = null;
        try {
            $files = $this->scannerControl->scanCurrentPath($this->jobQueueDto->getIsExportingOtherWpContentFiles(), $this->jobQueueDto->getExcludedDirectories());
        } catch (FinishedQueueException $e) {
            $this->logger->info('Finished scanning files');
            $this->stepsDto->finish();
            return;
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }

        // No files found here, skip it
        if (empty($files)) {
            return;
        }

        foreach ($files as $file) {
            if ($this->isThreshold()) {
                return;
            }
            $relativePath = str_replace(ABSPATH, null, $file);
            $this->files[] = $relativePath;
        }
        $this->files = array_unique($this->files);
    }

    protected function prepareFileScanner()
    {
        if ($this->stepsDto->getTotal() > 0) {
            return;
        }

        $this->jobQueueDto->setIncludedDirectories(array_map(static function ($dir) {
            return str_replace(ABSPATH, null, $dir);
        }, $this->jobQueueDto->getIncludedDirectories()));

        /** @noinspection NullPointerExceptionInspection */
        if ($this->scannerControl->getQueue()->count() < 1) {
            $this->initiateQueue();
        }

        $this->stepsDto->setTotal(1);
    }

    protected function getCaches()
    {
        $caches = parent::getCaches();
        $caches[] = $this->fileCache;
        /** @noinspection NullPointerExceptionInspection */
        $caches[] = $this->scannerControl->getQueue()->getStorage()->getCache();
        return $caches;
    }

    private function initiateQueue()
    {
        if ($this->jobQueueDto->getIncludedDirectories()) {
            $this->scannerControl->setNewQueueItems($this->jobQueueDto->getIncludedDirectories());
        }

        $directoryData = $this->cache->getPath() . DirectoryScannerControl::DATA_CACHE_FILE . '.' . AbstractCache::EXTENSION;
        if (!file_exists($directoryData)) {
            throw new RuntimeException(sprintf(
                'File %s does not exists. Need to Scan Directories first',
                $directoryData
            ));
        }

        $queueFile = $this->cache->getPath() . BufferedCacheStorage::FILE_PREFIX . FileScannerControl::QUEUE_CACHE_FILE;
        $queueFile .= '.' . AbstractCache::EXTENSION;

        if (!(new Filesystem())->copy($directoryData, $queueFile)) {
            throw new RuntimeException('Failed to copy %s as file scanner queue %s', $directoryData, $queueFile);
        }
    }

    /**
     * @return bool
     */
    private function shouldContinue()
    {
        return !$this->isThreshold() && !$this->stepsDto->isFinished();
    }

    private function updateSteps()
    {
        // Update steps
        $steps = $this->stepsDto;
        $steps->setTotal($steps->getTotal() + count($this->files));

        /** @noinspection NullPointerExceptionInspection */
        if ($this->scannerControl->getQueue()->count() > 0) {
            return;
        }

        $steps = $this->stepsDto;
        $total = $steps->getTotal() - 1;
        $total = $total >= 0 ? $total : 0;

        $steps->setTotal($total);
        $steps->setCurrent($total);
    }
}
