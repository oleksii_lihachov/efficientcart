<?php

// TODO PHP7.x; declare(strict_type=1);
// TODO PHP7.x; type hints & return types
// TODO PHP7.1; constant visibility

namespace WPStaging\Pro\Backup\Task\Tasks\JobExport;

use Exception;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Task\ExportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Pro\Backup\Service\Compressor;
use WPStaging\Framework\Filesystem\Filesystem;

class IncludeDatabaseTask extends ExportTask
{
    /** @var Compressor */
    private $compressor;

    public function __construct(Compressor $compressor, LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        parent::__construct($logger, $cache, $stepsDto);
        $this->compressor = $compressor;
    }

    public static function getTaskName()
    {
        return 'backup_export_include_database';
    }

    public static function getTaskTitle()
    {
        return 'Including Database to Site Export';
    }

    public function execute()
    {
        $this->prepareDatabaseTask();

        if (!$this->shouldExecute()) {
            return $this->generateResponse();
        }

        $compressorDto = $this->compressor->getDto();
        $compressorDto->setOffset($this->stepsDto->getCurrent());

        try {
            $this->compressor->appendFile($this->jobQueueDto->getDatabaseFile());
        } catch (Exception $e) {
            $this->logger->warning(sprintf(
                'Failed to include database export to backup: %s',
                $compressorDto->getFilePath()
            ));
        }

        $steps = $this->stepsDto;
        $steps->setCurrent($compressorDto->getOffset());

        if ($compressorDto->isFinished()) {
            $this->jobQueueDto->setTotalFiles($this->jobQueueDto->getTotalFiles() + 1);

            $this->stepsDto->finish();
            (new Filesystem())->delete($this->jobQueueDto->getDatabaseFile());
        }

        $this->logger->info(sprintf('Included %d bytes of Database Export', $steps->getCurrent()));
        return $this->generateResponse();
    }

    public function prepareDatabaseTask()
    {
        $filePath = $this->jobQueueDto->getDatabaseFile();
        if (!$filePath || !file_exists($filePath)) {
            $this->logger->warning(sprintf('Database Export file not found: %s', $filePath));
            $this->stepsDto->finish();
        }

        if ($this->stepsDto->getTotal() > 0) {
            return;
        }

        $this->compressor->getDto()->setFilePath($this->jobQueueDto->getDatabaseFile());
        $this->stepsDto->setTotal(filesize($this->compressor->getDto()->getFilePath()));
    }

    public function getCaches()
    {
        $caches = parent::getCaches();
        $caches[] = $this->compressor->getCacheIndex();
        $caches[] = $this->compressor->getCacheCompressed();
        return $caches;
    }

    private function shouldExecute()
    {
        return $this->jobQueueDto->getDatabaseFile()
            && !$this->stepsDto->isFinished()
        ;
    }
}
