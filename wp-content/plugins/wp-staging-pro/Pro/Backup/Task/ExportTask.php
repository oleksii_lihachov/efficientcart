<?php

namespace WPStaging\Pro\Backup\Task;

use WPStaging\Pro\Backup\Dto\Job\JobExportDataDto;

abstract class ExportTask extends AbstractTask
{
    /** @var JobExportDataDto */
    protected $jobQueueDto;
}
