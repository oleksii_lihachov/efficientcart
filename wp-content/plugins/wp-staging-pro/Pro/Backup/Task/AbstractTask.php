<?php

namespace WPStaging\Pro\Backup\Task;

use WPStaging\Framework\Traits\ResourceTrait;
use WPStaging\Pro\Backup\Dto\AbstractDto;
use WPStaging\Pro\Backup\Dto\JobDataDto;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Dto\TaskResponseDto;
use WPStaging\Pro\Backup\Job\AbstractQueueJob;
use WPStaging\Vendor\Psr\Log\LoggerInterface;
use WPStaging\Framework\Traits\TimerTrait;
use WPStaging\Framework\Utils\Cache\AbstractCache;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Core\Utils\Logger;

abstract class AbstractTask
{
    use TimerTrait;
    use ResourceTrait;

    /** @var LoggerInterface */
    protected $logger;

    /** @var Cache */
    protected $cache;

    /** @var bool */
    protected $prepared;

    // TODO RPoC
    /** @var string|null */
    protected $jobName;

    /** @var int|null */
    protected $jobId;

    /** @var bool */
    protected $debug;

    /** @var AbstractDto */
    protected $stepsDto;

    /** @var JobDataDto */
    protected $jobQueueDto;

    /** @var AbstractQueueJob */
    protected $job;

    public function __construct(LoggerInterface $logger, Cache $cache, StepsDto $stepsDto)
    {
        /** @var Logger logger */
        $this->logger   = $logger;
        $this->cache    = $cache;
        $this->stepsDto = $stepsDto;

        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    /**
     * @return \WPStaging\Pro\Backup\Dto\TaskResponseDto
     */
    abstract public function execute();

    /**
     * @example 'backup_site_import_themes'
     * @return string
     */
    abstract public static function getTaskName();

    /**
     * @example 'Importing Themes From Backup'
     * @return string
     */
    abstract public static function getTaskTitle();

    public function setJobContext(AbstractQueueJob $job)
    {
        #$this->cache->setPath($job::getJobCacheDirectory());
        $this->cache->setLifetime(HOUR_IN_SECONDS);
        $this->cache->setFilename('task_steps_' . static::getTaskName());

        $this->stepsDto->hydrate($this->cache->get([
            'current' => 0,
            'total'   => 0,
        ]));

        $this->job = $job;
    }

    public function setRelativeCacheDirectory($path)
    {
        /** @var AbstractCache $cache */
        foreach ($this->getCaches() as $cache) {
            $fullPath = trailingslashit($cache->getPath() . $path);
            $cache->setPath($fullPath);
        }
    }

    public function setJobQueueDto(JobDataDto $jobQueueDto)
    {
        $this->jobQueueDto = $jobQueueDto;
    }

    /**
     * @return \WPStaging\Pro\Backup\Dto\TaskResponseDto
     */
    public function generateResponse()
    {
        $this->stepsDto->incrementCurrentStep();

        // TODO Hydrate
        $response = $this->getResponseDto();
        $response->setStatus($this->stepsDto->isFinished());
        $response->setPercentage($this->stepsDto->getPercentage());
        $response->setTotal($this->stepsDto->getTotal());
        $response->setStep($this->stepsDto->getCurrent());
        $response->setTask($this->getTaskName());
        $response->setStatusTitle(static::getTaskTitle());
        $response->addMessage($this->logger->getLastLogMsg()); // TODO grab current task's logs, not last log message!

        $this->logger->setFileName(sprintf(
            '%s__%s__%s',
            $this->getJobId(),
            $this->getJobName(),
            date('Y_m_d__H')
        ));

        if ($this->stepsDto->isFinished()) {
            $this->cache->delete();
        } else {
            $this->cache->save($this->stepsDto->toArray(), true);
        }

        return $response;
    }

    /**
     * @return string|null
     */
    public function getJobName()
    {
        return $this->jobName;
    }

    /**
     * @param string|null $jobName
     */
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
        // TODO RPoC?
        #$this->setRelativeCacheDirectory($jobName);
    }

    /**
     * @return string|int|null
     */
    public function getJobId()
    {
        return $this->jobId;
    }

    /**
     * @param string|int|null $jobId
     */
    public function setJobId($jobId)
    {
        $this->jobId = $jobId;
    }

    /**
     * @param bool $debug
     */
    public function setDebug($debug)
    {
        $this->debug = (bool)$debug;
    }

    protected function getResponseDto()
    {
        return new TaskResponseDto();
    }

    /**
     * @return Cache[]|AbstractCache[]|array
     */
    protected function getCaches()
    {
        return [
            $this->cache,
        ];
    }
}
