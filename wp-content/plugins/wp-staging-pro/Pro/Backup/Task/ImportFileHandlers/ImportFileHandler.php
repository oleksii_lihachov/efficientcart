<?php

namespace WPStaging\Pro\Backup\Task\ImportFileHandlers;

use WPStaging\Pro\Backup\Task\FileImportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;

abstract class ImportFileHandler
{
    /** @var FileImportTask */
    protected $fileImportTask;

    /** @var LoggerInterface */
    protected $logger;

    public function setContext(FileImportTask $fileImportTask, LoggerInterface $logger)
    {
        $this->fileImportTask = $fileImportTask;
        $this->logger         = $logger;
    }

    abstract public function handle($source, $destination);
}
