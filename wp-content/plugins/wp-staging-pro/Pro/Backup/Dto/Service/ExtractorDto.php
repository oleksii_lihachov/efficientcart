<?php

namespace WPStaging\Pro\Backup\Dto\Service;

use RuntimeException;
use WPStaging\Pro\Backup\Entity\BackupMetadata;

class ExtractorDto
{
    /** @var BackupMetadata */
    private $backupMetadata;

    /** @var int */
    private $id;

    /** @var string */
    private $fullPath;

    /** @var int */
    private $seekToHeader;

    /** @var int */
    private $seekToFile;

    /** @var int */
    private $processedFiles;

    /** @var bool */
    private $singleOrMulti;

    /**
     * @return BackupMetadata
     */
    public function getBackupMetadata()
    {
        return $this->backupMetadata;
    }

    /**
     * @param BackupMetadata $backupMetadata
     */
    public function setBackupMetadata($backupMetadata)
    {
        $this->backupMetadata = $backupMetadata;
    }

    /**
     * @return int
     */
    public function getId()
    {
        if (!$this->id) {
            $this->id = time();
        }

        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return $this->fullPath;
    }

    /**
     * @param string $fullPath
     */
    public function setFullPath($fullPath)
    {
        $this->fullPath = $fullPath;
    }

    /**
     * @return int
     */
    public function getSeekToHeader()
    {
        if ($this->seekToHeader) {
            return $this->seekToHeader;
        }

        if (!$this->backupMetadata || !$this->backupMetadata->getHeaderStart()) {
            throw new RuntimeException('Failed to get header start');
        }

        $this->seekToHeader = $this->backupMetadata->getHeaderStart();

        return $this->seekToHeader;
    }

    /**
     * @param int|null $seekToHeader
     */
    public function setSeekToHeader($seekToHeader)
    {
        $this->seekToHeader = $seekToHeader;
    }

    public function addSeekToHeader($lineLength)
    {
        if ($this->seekToHeader === null) {
            $this->seekToHeader = 0;
        }
        $this->seekToHeader += $lineLength;
    }

    /**
     * @return int|null
     */
    public function getSeekToFile()
    {
        return $this->seekToFile;
    }

    /**
     * @param int|null $seekToFile
     */
    public function setSeekToFile($seekToFile)
    {
        $this->seekToFile = $seekToFile;
    }

    public function addSeekToFile($writtenBytes)
    {
        if ($this->seekToFile === null) {
            $this->seekToFile = 0;
        }
        $this->seekToFile += $writtenBytes;
    }

    /**
     * @return int
     */
    public function getProcessedFiles()
    {
        return $this->processedFiles;
    }

    /**
     * @param int $processedFiles
     */
    public function setProcessedFiles($processedFiles)
    {
        $this->processedFiles = $processedFiles;
    }

    public function incrementProcessedFiles()
    {
        if (!$this->processedFiles) {
            $this->processedFiles = 0;
        }
        $this->processedFiles++;
    }

    public function isFinished()
    {
        return $this->seekToHeader >= $this->backupMetadata->getHeaderEnd();
    }

    /**
     * @return bool
     */
    public function getSingleOrMulti()
    {
        return $this->singleOrMulti;
    }

    /**
     * @param bool $singleOrMulti
     */
    public function setSingleOrMulti($singleOrMulti)
    {
        $this->singleOrMulti = $singleOrMulti;
    }
}
