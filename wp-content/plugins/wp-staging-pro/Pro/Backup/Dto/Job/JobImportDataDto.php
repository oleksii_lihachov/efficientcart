<?php

namespace WPStaging\Pro\Backup\Dto\Job;

use WPStaging\Pro\Backup\Dto\JobDataDto;
use WPStaging\Pro\Backup\Entity\BackupMetadata;
use WPStaging\Pro\Database\Legacy\Entity\Backup;

class JobImportDataDto extends JobDataDto
{
    /** @var string */
    private $file;

    /** @var array */
    private $search = [];

    /** @var array */
    private $replace = [];

    /** @var BackupMetadata */
    private $backupMetadata;

    private $toClean;

    /** @var string */
    protected $tmpDirectory;

    private $fileStartsAt;

    private $headerStartsAt;

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
        if ($this->id) {
            return;
        }

        $id = rtrim($file, '.wpstg');
        $id = substr($id, strrpos($id, '_') + 1, strlen($id));
        $this->setId($id);
    }

    public function getSearch()
    {
        return (array)$this->search;
    }

    public function setSearch(array $search = [])
    {
        $this->search = $search;
    }

    public function getReplace()
    {
        return (array)$this->replace;
    }

    public function setReplace(array $replace = [])
    {
        $this->replace = $replace;
    }

    /**
     * @return BackupMetadata|null
     */
    public function getBackupMetadata()
    {
        return $this->backupMetadata;
    }

    /**
     * @param $backupMetadata
     */
    public function setBackupMetadata($backupMetadata)
    {
        if ($backupMetadata instanceof BackupMetadata) {
            $this->backupMetadata = $backupMetadata;

            return;
        }
        if (is_array($backupMetadata)) {
            try {
                $this->backupMetadata = (new BackupMetadata())->hydrate($backupMetadata);

                return;
            } catch (\Exception $e) {
                $this->backupMetadata = null;

                return;
            }
        }

        $this->backupMetadata = null;
    }

    public function getToClean()
    {
        return $this->toClean;
    }

    public function setToClean($path)
    {
        $this->toClean = $path;
    }

    /**
     * @return string
     */
    public function getTmpDirectory()
    {
        return $this->tmpDirectory;
    }

    /**
     * @param string $tmpPath
     *
     * @todo filesystem->safePath
     */
    public function setTmpDirectory($tmpPath)
    {
        $this->tmpDirectory = trailingslashit($tmpPath);
    }

    public function getHeaderStartsAt()
    {
        return $this->headerStartsAt;
    }

    public function setHeaderStartsAt($headerStartsAt)
    {
        $this->headerStartsAt = $headerStartsAt;
    }

    public function getFileStartsAt()
    {
        return $this->fileStartsAt;
    }

    public function setFileStartsAt($fileStartsAt)
    {
        $this->fileStartsAt = $fileStartsAt;
    }
}
