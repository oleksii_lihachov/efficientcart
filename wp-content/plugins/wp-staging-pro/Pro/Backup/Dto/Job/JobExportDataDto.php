<?php

namespace WPStaging\Pro\Backup\Dto\Job;

use WPStaging\Pro\Backup\Dto\JobDataDto;
use WPStaging\Pro\Backup\Dto\Traits\IsExportingTrait;

class JobExportDataDto extends JobDataDto
{
    use IsExportingTrait;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $notes;

    /** @var array */
    private $excludedDirectories = [];

    /** @var array */
    private $includedDirectories = [];

    /** @var bool */
    private $isAutomatedBackup = false;

    /** @var int */
    private $totalDirectories;

    /** @var int */
    private $totalFiles;

    /** @var string */
    private $databaseFile;

    /** @var string */
    private $currentFile;

    /** @var int */
    private $offset;

    /** @var string */
    private $fileName;

    /** @var int */
    private $tableRowsExported;

    /** @var int */
    private $tableRowsOffset;

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string|null $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return array|null
     */
    public function getExcludedDirectories()
    {
        return (array)$this->excludedDirectories;
    }

    public function setExcludedDirectories(array $excludedDirectories = [])
    {
        $this->excludedDirectories = $excludedDirectories;
    }

    /**
     * @return array
     */
    public function getIncludedDirectories()
    {
        return (array)$this->includedDirectories;
    }

    /**
     * @param array $includedDirectories
     */
    public function setIncludedDirectories($includedDirectories)
    {
        $this->includedDirectories = (array)$includedDirectories;
    }

    /**
     * @return bool
     */
    public function getIsAutomatedBackup()
    {
        return (bool)$this->isAutomatedBackup;
    }
    /**
     * @param bool $isAutomatedBackup
     */
    public function setIsAutomatedBackup($isAutomatedBackup)
    {
        $this->isAutomatedBackup = $isAutomatedBackup;
    }

    /**
     * @return int
     */
    public function getTotalDirectories()
    {
        return $this->totalDirectories;
    }

    /**
     * @param int $totalDirectories
     */
    public function setTotalDirectories($totalDirectories)
    {
        $this->totalDirectories = $totalDirectories;
    }

    /**
     * @return int
     */
    public function getTotalFiles()
    {
        return $this->totalFiles;
    }

    /**
     * @param int $totalFiles
     */
    public function setTotalFiles($totalFiles)
    {
        $this->totalFiles = $totalFiles;
    }

    /**
     * @return string
     */
    public function getDatabaseFile()
    {
        return $this->databaseFile;
    }

    /**
     * @param string $databaseFile
     */
    public function setDatabaseFile($databaseFile)
    {
        $this->databaseFile = $databaseFile;
    }

    /**
     * @return string
     */
    public function getCurrentFile()
    {
        return $this->currentFile;
    }

    /**
     * @param string $currentFile
     */
    public function setCurrentFile($currentFile)
    {
        $this->currentFile = $currentFile;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getTableRowsOffset()
    {
        return $this->tableRowsOffset;
    }

    /**
     * @param int $tableRowsOffset
     */
    public function setTableRowsOffset($tableRowsOffset)
    {
        $this->tableRowsOffset = $tableRowsOffset;
    }

    /**
     * @return int
     */
    public function getTableRowsExported()
    {
        return $this->tableRowsExported;
    }

    /**
     * @param int $tableRowsCompleted
     */
    public function setTableRowsExported($tableRowsExported)
    {
        $this->tableRowsExported = $tableRowsExported;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
}
