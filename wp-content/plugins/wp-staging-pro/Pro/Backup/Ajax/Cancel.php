<?php

// TODO PHP7.x; declare(strict_type=1);
// TODO PHP7.x; type hints & return types

namespace WPStaging\Pro\Backup\Ajax;

use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Component\AbstractTemplateComponent;
use WPStaging\Pro\Backup\Job\Jobs\JobExport;
use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Core\WPStaging;

// TODO RPoC
class Cancel extends AbstractTemplateComponent
{
    public function render()
    {
        if (! $this->canRenderAjax()) {
            return;
        }

        $directory = WPStaging::getInstance()->get(Directory::class);
        // todo: Check this:
        //(new Filesystem())->delete($directory->getCacheDirectory() . JobExport::getJobName());
        wp_send_json(true);
    }
}
