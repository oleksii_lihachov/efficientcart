<?php

namespace WPStaging\Pro\Backup\Ajax\Import;

use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Framework\Security\Auth;
use WPStaging\Pro\Backup\Dto\Job\JobImportDataDto;
use WPStaging\Pro\Backup\Job\Jobs\JobImport;

class PrepareImport
{
    private $auth;
    private $filesystem;
    private $directory;
    private $jobQueueDto;
    private $jobImport;

    public function __construct(Auth $auth, Filesystem $filesystem, Directory $directory, JobImportDataDto $jobQueueDto, JobImport $jobImport)
    {
        $this->auth        = $auth;
        $this->filesystem  = $filesystem;
        $this->directory   = $directory;
        $this->jobQueueDto = $jobQueueDto;
        $this->jobImport   = $jobImport;
    }

    public function ajaxPrepare($data)
    {
        if (!$this->auth->isAuthenticatedRequest()) {
            wp_send_json_error(null, 401);
        }

        $response = $this->prepare($data);

        if ($response instanceof \WP_Error) {
            wp_send_json_error($response->get_error_message(), $response->get_error_code());
        } else {
            wp_send_json_success();
        }
    }

    public function prepare($data = null)
    {
        if (empty($data) && array_key_exists('wpstgImportData', $_POST)) {
            $data = $_POST['wpstgImportData'];
        }

        try {
            $sanitizedData = $this->setupInitialData($data);
        } catch (\Exception $e) {
            return new \WP_Error(400, $e->getMessage());
        }

        return $sanitizedData;
    }

    private function setupInitialData($sanitizedData)
    {
        $sanitizedData = $this->validateAndSanitizeData($sanitizedData);
        $this->clearCacheFolder();

        $this->jobQueueDto->hydrate($sanitizedData);
        $this->jobQueueDto->setInit(true);
        $this->jobQueueDto->setFinished(false);

        if ($this->jobQueueDto->getId() === null) {
            $this->jobQueueDto->setId(time());
        }

        $this->jobImport->setJobQueueDto($this->jobQueueDto);

        $this->jobImport->onWpShutdown();

        return $sanitizedData;
    }

    private function clearCacheFolder()
    {
        $this->filesystem->delete($this->directory->getCacheDirectory());
        $this->filesystem->mkdir($this->directory->getCacheDirectory(), true);
    }

    /**
     * @return array
     */
    private function validateAndSanitizeData($data)
    {
        if (!array_key_exists('search', $data)) {
            $data['search'] = [];
        }

        if (!array_key_exists('replace', $data)) {
            $data['replace'] = [];
        }

        $expectedKeys = [
            'file',
            'search',
            'replace',
        ];

        // Make sure data has no keys other than the expected ones.
        $data = array_intersect_key($data, array_flip($expectedKeys));

        // Make sure data has all expected keys.
        foreach ($expectedKeys as $expectedKey) {
            if (!array_key_exists($expectedKey, $data)) {
                throw new \UnexpectedValueException("Invalid request. Missing '$data'.");
            }
        }

        return $data;
    }
}
