<?php

namespace WPStaging\Pro\Backup\Ajax\Export;

use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Framework\Security\Auth;
use WPStaging\Pro\Backup\Dto\Job\JobExportDataDto;
use WPStaging\Pro\Backup\Job\Jobs\JobExport;

class PrepareExport
{
    private $auth;
    private $filesystem;
    private $directory;
    private $jobQueueDto;
    private $jobExport;

    public function __construct(Auth $auth, Filesystem $filesystem, Directory $directory, JobExportDataDto $jobQueueDto, JobExport $jobExport)
    {
        $this->auth        = $auth;
        $this->filesystem  = $filesystem;
        $this->directory   = $directory;
        $this->jobQueueDto = $jobQueueDto;
        $this->jobExport   = $jobExport;
    }

    public function ajaxPrepare($data)
    {
        if (!$this->auth->isAuthenticatedRequest()) {
            wp_send_json_error(null, 401);
        }

        $response = $this->prepare($data);

        if ($response instanceof \WP_Error) {
            wp_send_json_error($response->get_error_message(), $response->get_error_code());
        } else {
            wp_send_json_success();
        }
    }

    public function prepare($data = null)
    {
        if (empty($data) && array_key_exists('wpstgExportData', $_POST)) {
            $data = $_POST['wpstgExportData'];
        }

        try {
            $sanitizedData = $this->setupInitialData($data);
        } catch (\Exception $e) {
            return new \WP_Error(400, $e->getMessage());
        }

        return $sanitizedData;
    }

    private function setupInitialData($sanitizedData)
    {
        $sanitizedData = $this->validateAndSanitizeData($sanitizedData);
        $this->clearCacheFolder();

        $this->jobQueueDto->hydrate($sanitizedData);
        $this->jobQueueDto->setInit(true);
        $this->jobQueueDto->setFinished(false);

        if ($this->jobQueueDto->getId() === null) {
            $this->jobQueueDto->setId(time());
        }

        $this->jobExport->setJobQueueDto($this->jobQueueDto);

        $this->jobExport->onWpShutdown();

        return $sanitizedData;
    }

    private function clearCacheFolder()
    {
        $this->filesystem->delete($this->directory->getCacheDirectory());
        $this->filesystem->mkdir($this->directory->getCacheDirectory(), true);
    }

    /**
     * @return array
     */
    private function validateAndSanitizeData($data)
    {
        $expectedKeys = [
            'name',
            'isExportingPlugins',
            'isExportingMuPlugins',
            'isExportingThemes',
            'isExportingUploads',
            'isExportingOtherWpContentFiles',
            'isExportingDatabase',
        ];

        // Make sure data has no keys other than the expected ones.
        $data = array_intersect_key($data, array_flip($expectedKeys));

        // Make sure data has all expected keys.
        foreach ($expectedKeys as $expectedKey) {
            if (!array_key_exists($expectedKey, $data)) {
                throw new \UnexpectedValueException("Invalid request. Missing '$data'.");
            }
        }

        // Sanitize data
        $data['name']                           = substr(sanitize_text_field(html_entity_decode($data['name'])), 0, 100);
        $data['isExportingPlugins']             = $this->jsBoolean($data['isExportingPlugins']);
        $data['isExportingMuPlugins']           = $this->jsBoolean($data['isExportingMuPlugins']);
        $data['isExportingThemes']              = $this->jsBoolean($data['isExportingThemes']);
        $data['isExportingUploads']             = $this->jsBoolean($data['isExportingUploads']);
        $data['isExportingOtherWpContentFiles'] = $this->jsBoolean($data['isExportingOtherWpContentFiles']);
        $data['isExportingDatabase']            = $this->jsBoolean($data['isExportingDatabase']);

        return $data;
    }

    /**
     * @param mixed $value A value that we want to detect if it's true or false.
     *
     * @return bool A PHP boolean interpretation of this value.
     */
    private function jsBoolean($value)
    {
        return $value === 'true' || $value === true;
    }
}
