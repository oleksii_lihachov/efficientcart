<?php

namespace WPStaging\Pro\Backup\Service;

use Exception;
use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Filesystem\File;
use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Pro\Backup\Dto\Service\ExtractorDto;
use WPStaging\Pro\Backup\Entity\FileBeingExtracted;
use WPStaging\Pro\Backup\Service\Exceptions\ExtractorException;

class Extractor
{
    /** @var Directory */
    private $directory;

    /** @var ExtractorDto */
    private $dto;

    /** @var FileBeingExtracted|null */
    private $extractingFile;

    /** @var File */
    private $file;

    /** @var string */
    private $dirImport;

    /** @var string */
    private $databaseFile;

    public function __construct(Directory $directory)
    {
        $this->directory = $directory;
    }

    public function setDto(ExtractorDto $dto, $tmpDirectory)
    {
        $this->dto = $dto;
        $this->setup($tmpDirectory);
    }

    /**
     * @param callable|null $shouldStop
     *
     * @return ExtractorDto
     */
    public function extract(callable $shouldStop = null)
    {
        while (!$shouldStop() && !$this->dto->isFinished()) {
            $this->extractCurrentFile($shouldStop);
        }

        return $this->dto;
    }

    private function extractCurrentFile(callable $shouldStop = null)
    {
        // Move file to the current header / index position
        $this->file->fseek($this->dto->getSeekToHeader());
        // Get file info from the position
        $this->extractingFile = new FileBeingExtracted($this->file, $this->dto);
        // Move cursor to file position
        $this->file->fseek($this->extractingFile->findSeekTo());

        // Create / Write to File
        try {
            $this->fileBatchWrite($shouldStop);
        } catch (Exception $e) {
            // Set this file as "written", so that we can skip to the next file.
            $this->extractingFile->setWrittenBytes($this->extractingFile->getTotalBytes());

            /** @todo Show errors to user */
            if (defined('WPSTG_DEBUG') && WPSTG_DEBUG) {
                error_log($e->getMessage());
            }
        }

        if (!$this->extractingFile->isFinished()) {
            $this->dto->addSeekToFile($this->extractingFile->getWrittenBytes());

            return;
        }

        $this->dto->incrementProcessedFiles();
        $this->dto->addSeekToHeader(strlen($this->extractingFile->getLine()));
    }

    /**
     * @param callable|null $shouldStop
     *
     * @throws ExtractorException
     */
    private function fileBatchWrite(callable $shouldStop = null)
    {
        $isStop          = false;
        $importFilePath = $this->dirImport . $this->extractingFile->getRelativePath();

        if (file_exists($importFilePath)) {
            throw ExtractorException::fileAlreadyExists($importFilePath);
        }

        if (!ctype_print($importFilePath)) {
            if (defined('WPSTG_DEBUG') && WPSTG_DEBUG) {
                error_log('Export: Trying to create a folder with a path that does not seem to be valid.');
            }
        }

        (new Filesystem())->mkdir(dirname($importFilePath));
        $targetFile = new File($importFilePath, File::MODE_APPEND);

        while (!$isStop) {
            $length          = $this->extractingFile->findReadTo();
            $partialContents = '';
            if ($length > 0) {
                /*
                 * @todo: Add option to delete from .wpstg file as we extract, for filesystems with low disk space.
                 */
                $partialContents = $this->file->fread($this->extractingFile->findReadTo());
            }
            $writtenBytes = $targetFile->fwrite($partialContents);
            $this->extractingFile->addWrittenBytes($writtenBytes);
            $isStop = $shouldStop() || $this->extractingFile->isFinished();
        }
    }

    private function setup($tmpDirectory)
    {
        if ($this->dto->getFullPath()) {
            $this->file = new File($this->dto->getFullPath());
        }

        if ($this->dto->getId()) {
            $this->dirImport = (new Filesystem())->mkdir($tmpDirectory);
        }

        $this->dto->getBackupMetadata()->setDirUploads($this->directory->getUploadsDirectory());
    }
}
