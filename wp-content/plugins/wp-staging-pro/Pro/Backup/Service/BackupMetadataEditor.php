<?php

namespace WPStaging\Pro\Backup\Service;

use WPStaging\Framework\Utils\Cache\BufferedCache;
use WPStaging\Pro\Backup\Entity\BackupMetadata;

class BackupMetadataEditor
{
    private $bufferedCache;

    public function __construct(BufferedCache $bufferedCache)
    {
        $this->bufferedCache = $bufferedCache;
    }

    public function setBackupMetadata($backupPath, BackupMetadata $newHeaders)
    {
        $this->bufferedCache->setPath($backupPath);

        $info = $this->bufferedCache->readLines(1, null, BufferedCache::POSITION_BOTTOM);

        // Expected: ['', '{BACKUP_METADATA_IN_JSON_FORMAT}']
        if (!is_array($info) || count($info) !== 2) {
            throw new \RuntimeException('Could not read the existing metadata from backup.');
        }

        // Expected: ['{BACKUP_METADATA_IN_JSON_FORMAT}']
        $info = array_filter($info, function ($infoLine) {
            return !empty(trim($infoLine));
        });

        if (count($info) !== 1) {
            throw new \RuntimeException('Could not read the existing metadata from backup.');
        }

        // Expected: '{BACKUP_METADATA_IN_JSON_FORMAT}'
        $existingHeaders = end($info);

        if (!is_object(json_decode($existingHeaders))) {
            throw new \RuntimeException('Could not read valid existing metadata from backup.');
        }

        // @todo Should we use mb_strlen($_writtenBytes, '8bit') instead of strlen?
        $this->bufferedCache->deleteBottomBytes(strlen($existingHeaders));
        $this->bufferedCache->append(json_encode($newHeaders));
    }
}
