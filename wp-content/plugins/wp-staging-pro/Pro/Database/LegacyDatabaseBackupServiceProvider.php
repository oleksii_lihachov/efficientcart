<?php

namespace WPStaging\Pro\Database;

use WPStaging\Framework\DI\ServiceProvider;
use WPStaging\Pro\Database\Legacy\Ajax\Listing;
use WPStaging\Pro\Database\Legacy\Job\JobQueueDto;
use WPStaging\Pro\Database\Legacy\Ajax\Create;
use WPStaging\Pro\Database\Legacy\Ajax\Export;
use WPStaging\Pro\Database\Legacy\Ajax\ConfirmDelete;
use WPStaging\Pro\Database\Legacy\Ajax\Delete;
use WPStaging\Pro\Database\Legacy\Ajax\ConfirmRestore;
use WPStaging\Pro\Database\Legacy\Ajax\Restore;
use WPStaging\Pro\Database\Legacy\Job\JobRestoreBackup;
use WPStaging\Pro\Database\Legacy\Job\JobRestoreBackupDto;
use WPStaging\Pro\Database\Legacy\Ajax\Edit;

class LegacyDatabaseBackupServiceProvider extends ServiceProvider
{
    public function registerClasses()
    {
        $this->container->when(JobRestoreBackup::class)
                        ->needs(JobQueueDto::class)
                        ->give(JobRestoreBackupDto::class);
    }

    public function addHooks()
    {
        // Legacy database
        add_action('wp_ajax_wpstg--backups--database-legacy-create', $this->container->callback(Create::class, 'render'));
        add_action('wp_ajax_wpstg--backups--database-legacy-listing', $this->container->callback(Listing::class, 'render'));
        add_action('wp_ajax_wpstg--backups--database-legacy-delete', $this->container->callback(Delete::class, 'render'));
        add_action('wp_ajax_wpstg--backups--database-legacy-edit', $this->container->callback(Edit::class, 'render'));
        add_action('wp_ajax_wpstg--backups--database-legacy-export', $this->container->callback(Export::class, 'render'));
        add_action('wp_ajax_wpstg--backups--database-legacy-delete-confirm', $this->container->callback(ConfirmDelete::class, 'render'));
        add_action('wp_ajax_wpstg--backups--database-legacy-restore-confirm', $this->container->callback(ConfirmRestore::class, 'render'));
        add_action('wp_ajax_wpstg--backups--database-legacy-restore', $this->container->callback(Restore::class, 'render'));

        // Nopriv
        add_action('wp_ajax_nopriv_wpstg--backups--database-legacy-listing', $this->container->callback(Listing::class, 'render'));
        add_action('wp_ajax_nopriv_wpstg--backups--database-legacy-restore', $this->container->callback(Restore::class, 'render'));
    }
}
