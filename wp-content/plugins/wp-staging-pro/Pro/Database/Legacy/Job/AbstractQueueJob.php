<?php

// TODO PHP7.x; declare(strict_types=1);
// TODO PHP7.x; return types && type-hints

namespace WPStaging\Pro\Database\Legacy\Job;

use WPStaging\Framework\Adapter\Directory;
use WPStaging\Framework\Filesystem\Filesystem;
use WPStaging\Framework\Interfaces\ShutdownableInterface;
use WPStaging\Pro\Database\Legacy\Component\Task\AbstractTask;
use WPStaging\Pro\Database\Legacy\Component\TaskResponseDto;
use WPStaging\Framework\Queue\Queue;
use WPStaging\Framework\Queue\Storage\CacheStorage;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Core\WPStaging;

abstract class AbstractQueueJob implements ShutdownableInterface
{
    /** @var Cache */
    private $jobCache;

    /** @var JobQueueDto */
    protected $jobQueueDto;

    /** @var Queue */
    private $queue;

    /**
     * @var CacheStorage $queueCache Controls the cache file for the queue.
     */
    private $queueCache;

    /** @var AbstractTask */
    protected $currentTask;

    public function __construct(
        Cache $jobCache,
        Queue $queue,
        CacheStorage $queueCache,
        JobQueueDto $dto
    ) {
        $this->jobCache    = $jobCache;
        $this->queue       = $queue;
        $this->queueCache  = $queueCache;
        $this->jobQueueDto = $dto;

        $this->setUp();
    }

    public function onWpShutdown()
    {
        if ($this->jobQueueDto->isStatusCheck()) {
            return;
        }

        if ($this->jobQueueDto->isFinished()) {
            $this->clean();

            return;
        }

        $this->jobCache->save($this->jobQueueDto->toArray());
    }

    /**
     * @return object
     */
    abstract public function execute();

    /**
     * @return string
     */
    abstract public function getJobName();

    abstract protected function initiateTasks();

    protected function setUp()
    {
        if (!empty($_POST['reset']) && ($_POST['reset'] === true || $_POST['reset'] === 'true')) {
            $this->clean();
        }

        $this->setQueue();

        $this->jobCache->setLifetime(HOUR_IN_SECONDS);
        $this->jobCache->setFilename('job_' . $this->getJobName());
        $this->jobCache->setPath(trailingslashit($this->jobCache->getPath() . $this->getJobName()));

        $this->jobQueueDto->setInit(true);
        $this->jobQueueDto->setFinished(false);

        $data = $this->jobCache->get([]);
        if ($data) {
            $this->jobQueueDto->hydrate($data);
        }

        // TODO RPoC Hack
        $this->jobQueueDto->setStatusCheck(!empty($_GET['action']) && $_GET['action'] === 'wpstg--backups--status');

        if ($this->jobQueueDto->isStatusCheck()) {
            return;
        }

        if ($this->jobQueueDto->getId() === null) {
            $this->jobQueueDto->setId(time());
        }

        if ($this->jobQueueDto->isInit() && method_exists($this, 'init')) {
            $this->init();
        }

        if ($this->jobQueueDto->isInit()) {
            $this->queue->reset();
            $this->initiateTasks();
        }

        $this->jobQueueDto->setInit(false);
        /** @var AbstractTask currentTask */
        $this->currentTask = WPStaging::getInstance()->get($this->queue->pop());
        if (!$this->currentTask) {
            return;
        }
        $this->currentTask->setJobId($this->jobQueueDto->getId());
        $this->currentTask->setJobName($this->getJobName());
        $this->currentTask->setDebug(defined('WPSTG_DEBUG') && WPSTG_DEBUG);
    }

    /**
     * Clean anything that is left over from executing the job
     * @return void
     */
    protected function clean()
    {
        /** @var Directory $directory */
        $directory = WPStaging::getInstance()->get(Directory::class);
        (new Filesystem())->delete($directory->getCacheDirectory());
        (new Filesystem())->mkdir($directory->getCacheDirectory(), true);

        $this->queue->reset();
    }

    protected function setQueue()
    {
        $this->queue->setName($this->findCurrentJob());

        $this->queueCache->getCache()->setPath(
            trailingslashit($this->queueCache->getCache()->getPath() . $this->getJobName())
        );

        $this->queue->setStorage($this->queueCache);
    }

    /**
     * @param TaskResponseDto $response
     *
     * @return TaskResponseDto
     */
    protected function getResponse(TaskResponseDto $response)
    {
        $response->setJob(substr($this->findCurrentJob(), 3));

        // TODO RPoC below
        // Task is not done yet, add it to beginning of the queue again
        if (!$response->isStatus()) {
            // TODO PHP7.x; $this->currentTask::class;
            $className = get_class($this->currentTask);
            $this->queue->prepend($className);
        }

        if ($response->isStatus() && $this->queue->count() === 0) {
            $this->jobQueueDto->setFinished(true);

            return $response;
        }

        $response->setStatus(false);

        return $response;
    }

    protected function findCurrentJob()
    {
        $class = explode('\\', static::class);

        return end($class);
    }

    /**
     * @param string $notation
     * @param mixed  $value
     */
    protected function injectTaskRequest($notation, $value)
    {
        if (!isset($_POST['wpstg']) || !is_array($_POST['wpstg'])) {
            $_POST['wpstg'] = [];
        }

        if (!isset($_POST['wpstg']['tasks']) || !is_array($_POST['wpstg']['tasks'])) {
            $_POST['wpstg']['tasks'] = [];
        }

        $data = &$_POST['wpstg']['tasks'];
        $keys = explode('.', $notation);
        foreach ($keys as $key) {
            if (!isset($data[$key])) {
                $data[$key] = [];
            }
            $data = &$data[$key];
        }

        if (!is_array($value)) {
            $data = $value;

            return;
        }

        if (!is_array($data)) {
            $data = [];
        }

        $data = array_merge($data, $value);
    }

    /**
     * @return JobQueueDto
     */
    public function getDto()
    {
        return $this->jobQueueDto;
    }

    protected function prepare()
    {
        if (method_exists($this, 'provideRequestDto')) {
            $this->provideRequestDto();
        }

        if (method_exists($this, 'injectRequests')) {
            $this->injectRequests();
        }

        $this->saveCurrentStatusTitle();
    }

    protected function addTasks(array $tasks = [])
    {
        foreach ($tasks as $task) {
            $this->queue->push($task);
        }
    }

    /**
     * Provides arguments to be used within saveCurrentStatusTitle()
     * In most use cases it will be overwritten but in case it is forgotten, it will not throw error
     * @return array
     */
    protected function findCurrentStatusTitleArgs()
    {
        return [];
    }

    protected function saveCurrentStatusTitle()
    {
        // Might happen when xdebug enabled so saving us some extra reading unnecessary log output
        if (!$this->currentTask) {
            return;
        }

        $args  = $this->findCurrentStatusTitleArgs();
        $title = $this->currentTask->getStatusTitle($args);
        $this->jobQueueDto->setCurrentStatusTitle($title);
        $this->jobCache->save($this->jobQueueDto->toArray());
    }
}
