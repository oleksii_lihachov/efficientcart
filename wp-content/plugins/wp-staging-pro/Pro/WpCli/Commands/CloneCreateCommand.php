<?php

/**
 * Allow creating staging sites by cloning the main site.
 *
 * @package WPStaging\Pro\WpCli\Commands
 */

namespace WPStaging\Pro\WpCli\Commands;

/**
 * Class CloneCreateCommand
 *
 * @package WPStaging\Pro\WpCli\Commands
 */
class CloneCreateCommand implements CommandInterface
{
    /**
     * Creates a staging site.
     *
     * @subcommand create
     *
     */
    public function __invoke(array $args = [], array $assocArgs = [])
    {
        return true;
    }
}
