<?php

/**
 * Provides information about the plugin integration with wp-cli.
 *
 * @package WPStaging\Pro\WpCli\Commands
 */

namespace WPStaging\Pro\WpCli\Commands;

use WPStaging\Core\WPStaging;

/**
 * Class Dispatcher
 *
 * @package WPStaging\Pro\WpCli\Commands
 */
class Dispatcher
{
    /**
     * {@inheritdoc}
     */
    public static function registrationArgs()
    {
        return [
            'shortdesc' => 'Manages WPStaging PRO cloning and pushing operations.'
        ];
    }

    /**
     * Returns the map from the sub-command slugs to the class implementing them.
     *
     *
     * @return array<string,string> A map from the sub-command slugs to the classes implementing them.
     */
    protected static function getSubCommandMap()
    {
        $subCommandMap = [
            'create' => CloneCreateCommand::class
        ];

        /**
         * Allows filtering the map from command slugs to the classes implementing them.
         *
         * @param array<string,string> $subCommandMap A map from the sub-command slugs to
         *                                            the classes implementing them.
         */
        $subCommandMap = apply_filters('wpstg_wpcli_subcommand_map', $subCommandMap);

        return $subCommandMap;
    }

    /**
     * Creates a staging site.
     *
     * @subcommand create
     */
    public function create(array $args = [], array $assocArgs = [])
    {
        /** @var CommandInterface $subCommand */
        $subCommand = static::getSubcommand('create');
        return $subCommand($args, $assocArgs);
    }

    /**
     * Returns the sub-command mapped to the sub-command slug..
     *
     * @param string $subCommand The sub-command slug; e.g. `create`.
     *
     * @return CommandInterface A command instance reference.
     */
    public static function getSubcommand($subCommand)
    {
        $subCommandMap = self::getSubCommandMap();

        $commandClass = isset($subCommandMap[$subCommand]) ? $subCommandMap[$subCommand] : static::class;

        if ($commandClass === false) {
            // If we're here, and the sub-command is not mapped, then this is a developer error: report it.
            throw new \LogicException("No command class is mapped to the {$subCommand} sub-command!");
        }

        if (!class_implements($commandClass, CommandInterface::class)) {
            throw new \LogicException(
                "The class {$commandClass} MUST implement the " . CommandInterface::class . ' interface.'
            );
        }

        $container = WPStaging::getInstance()->getContainer();

        return $container->make($commandClass);
    }
}
