<?php
$textDomain = $viewParams['textDomain'] ?? 'tamara';

/** @var WC_Order $wcOrder */
$wcOrder           = $viewParams['wcOrder'] ?? null;
$tamaraCheckoutUrl = $viewParams['tamaraCheckoutUrl'] ?? null;
?>
<div class="tamara-confirm-button">
    <p> <?php echo __('Tamara Checkout Session created.', $textDomain) ?> <br/>
        <strong>
            <?php echo sprintf(__('To continue, please confirm your payment with Tamara.',
                $textDomain)) ?>
        </strong>
    </p>

    <br/>
    <a class="tamara-confirm-button__proceed-link button"
       href="<?php echo $tamaraCheckoutUrl ?>"><?php echo __('Proceed To Tamara Payment',
            $textDomain) ?></a>
    <br/>
    <a class="tamara-confirm-button__cancel-link button"
       href="<?php echo $wcOrder->get_cancel_order_url() ?>"><?php echo __('Cancel Order',
            $textDomain) ?></a>
</div>