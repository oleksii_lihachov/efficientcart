<?php

$textDomain = $viewParams['textDomain'] ?? 'tamara';
$defaultDescription = $viewParams['defaultDescription'] ?? '';

?>
<div class="tamara-gateway-description">
    <p><?php echo __('No immediate payment needed. Buy now and and pay within 30 days.', $textDomain) ?></p>
    <table class="tamara-gateway-description__table">
        <tr class="tamara-gateway-description__table__icon">
            <td class="tamara-gateway-description__table__icon--first"></td>
            <td class="tamara-gateway-description__table__icon--second"></td>
            <td class="tamara-gateway-description__table__icon--third"></td>
        </tr>
        <tr class="tamara-gateway-description__table__head">
            <td><span><?php echo __('Enter your data', $textDomain) ?></span></td>
            <td><span><?php echo __('Receive your order... and pay later', $textDomain) ?></span></td>
            <td><span><?php echo __('Pay however you want', $textDomain) ?></span></td>
        </tr>
        <tr class="tamara-gateway-description__table__text">
            <td>
                <span><?php echo __('Mobile number, ID details (first order only) and payment details to confirm your order', $textDomain) ?></span>
            </td>
            <td>
                <span><?php echo __('without any payment today', $textDomain) ?></span>
            </td>
            <td>
                <span><?php echo __('Pay your order value only within 30 days via Tamara App', $textDomain) ?></span>
            </td>
        </tr>
    </table>
    <p class="tamara-gateway-description__default"><?php echo $defaultDescription ?></p>
</div>
