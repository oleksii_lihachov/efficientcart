<?php
$textDomain   = $viewParams['textDomain'] ?? 'tamara';
$errorMessage = $viewParams['errorMessage'] ?? null;
?>
<div class="tamara-error-content">
    <p>
        <?php echo __('Tamara service is having problem. Please try again later!',
            $textDomain) ?>
    </p>
    <p><?php echo $errorMessage ?></p>
</div>