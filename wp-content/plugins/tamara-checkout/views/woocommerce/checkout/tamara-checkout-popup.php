<?php
$textDomain = $viewParams['textDomain'] ?? 'tamara';
$baseUrl = $viewParams['baseUrl'] ?? '/wp-content/plugins/tamara-checkout';
$imagePath = $baseUrl.'/assets/dist/img/';
$instalmentsMinAmount = $viewParams['instalmentsMinAmount'] ?? 0;
$cartCurrency = __(get_woocommerce_currency(), $textDomain);
$logo = __('tamara-logo-transparent-color.svg', $textDomain);

?>

<div id="tamara-checkout-popup" class="tamara-checkout-popup">
    <div class="tamara-modal-heading">
        <img src="<?php echo $imagePath.$logo ?>" alt="Tamara Modal">
        <p class="tamara-modal-heading-sub-title"><?php echo __('Buy now, pay later', $textDomain) ?></p>
    </div>
    <div class="tamara-modal-body">

        <div class="tamara-sub-title-head tamara-one-block"><?php echo __('Why Tamara', $textDomain) ?></div>

        <div class="tamara-one-block">
            <div class="tamara-left-content">
                <img src="<?php echo $imagePath.'zero-interest.svg' ?>" alt="Tamara Zero Interest"></div>
            <div class="tamara-right-content">
                <p class="tamara-sub-title-head-smaller"><?php echo __('Zero interest', $textDomain) ?></p>
            </div>
        </div>

        <div class="tamara-one-block">
            <div class="tamara-left-content"><img src="<?php echo $imagePath.'no-hidden-fee.svg' ?>" alt="Tamara No Hidden Fees"></div>
            <div class="tamara-right-content">
                <p class="tamara-sub-title-head-smaller"><?php echo __('No hidden fees', $textDomain) ?></p>
            </div>
        </div>

        <div class="tamara-one-block">
            <div class="tamara-left-content"><img src="<?php echo $imagePath.'use-any-debit-card.svg' ?>" alt="Tamara Debit Card">
            </div>
            <div class="tamara-right-content">
                <p class="tamara-sub-title-head-smaller"><?php echo __('Use any debit card or even ApplePay',
                        $textDomain) ?></p>
            </div>
        </div>

        <div class="tamara-one-block">
            <div class="tamara-left-content"><img src="<?php echo $imagePath.'quick-and-easy.svg' ?>" alt="Tamara Quick And Easy">
            </div>
            <div class="tamara-right-content">
                <p class="tamara-sub-title-head-smaller"><?php echo __('Quick and easy', $textDomain) ?></p>
            </div>
        </div>

        <div class="tamara-sub-title-head tamara-one-block"><?php echo __('Payment Options', $textDomain) ?></div>

        <div class="tamara-payment-option-box">
            <p><span><?php echo __('Pay Later', $textDomain) ?></span></p>
            <ul>
                <li><?php echo __('Pay within 30 days after shipping.', $textDomain) ?></li>
                <li><?php echo __('Receive the goods before you pay for it.', $textDomain) ?></li>
            </ul>
        </div>
        <p class="tamara-payment-option-separate-text"><span><?php echo __('OR', $textDomain) ?></span></p>
        <div class="tamara-payment-option-box">
            <p><span><?php echo __('Installments', $textDomain) ?></span></p>
            <ul>
                <li><?php echo __('Pay 1/3 of the price now.', $textDomain) ?></li>
                <li><?php echo __('Pay the rest in hassle free installments.', $textDomain) ?></li>
            </ul>
            <div class="tamara-small-note-left">
                <?php echo __('*Installments are available for purchases over ', $textDomain).$cartCurrency.' '.$instalmentsMinAmount ?>
            </div>
        </div>

        <div class="tamara-find-out-more">
            <a target="_blank" href="<?php echo __('https://tamara.co/en/index.html', $textDomain) ?>">
                <?php echo __('Find out more', $textDomain) ?>
            </a>
        </div>
        <div class="tamara-small-note-center">
            <?php echo __('*Applicable for KSA and UAE customers only', $textDomain) ?>
        </div>
    </div>
</div>
