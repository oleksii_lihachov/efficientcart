<?php

use Tamara\Wp\Plugin\TamaraCheckout;

$productPrice = $viewParams['productPrice'] ?? 0;
$siteLocale = $viewParams['siteLocale'] ?? 'en';
$instalmentsThreshold = TamaraCheckout::getInstance()->getWCTamaraGatewayService()->populateInstalmentsMinLimit();
$payByLaterMaxLimit = TamaraCheckout::getInstance()->getWCTamaraGatewayService()->populateMaxLimit();
$isPayByLaterEnabled = TamaraCheckout::getInstance()->isPayByLaterEnabled();
$isPayByInstalmentsEnabled = TamaraCheckout::getInstance()->isPayByInstalmentsEnabled();
$disableInstalment = false;
$disablePaylater = false;

if (!$isPayByLaterEnabled || $productPrice > $payByLaterMaxLimit) {
    $disablePaylater = true;
}
if (!$isPayByInstalmentsEnabled || $productPrice < $instalmentsThreshold) {
    $disableInstalment = true;
}

echo '<div class="tamara-product-widget" data-disable-paylater="'.($disablePaylater ? 'true' : 'false').'" data-disable-installment="'.($disableInstalment ? 'true' : 'false').'" data-price="'.esc_attr($productPrice).'" 
           data-lang="'.esc_attr($siteLocale).'" data-installment-minimum-amount="'.esc_attr($instalmentsThreshold).'"></div>';