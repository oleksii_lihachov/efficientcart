<?php
$textDomain     = $viewParams['textDomain'] ?? 'tamara';
$minLimitAmount = $viewParams['minLimitAmount'] ?? null;
$maxLimitAmount = $viewParams['maxLimitAmount'] ?? null;

/** @var WC_Order $wcOrder */
$wcOrder           = $viewParams['wcOrder'] ?? null;
$tamaraCheckoutUrl = $viewParams['tamaraCheckoutUrl'] ?? null;
?>

<ul class="limit-amount">
    <strong>
        <?php echo __('Pay Later Information', $textDomain) ?>
    </strong>
    <li class="limit-amount-item"><?php echo __('Min Limit: ',
                $textDomain) . $wcOrder->get_currency() . number_format($minLimitAmount, 2, ".", ",") ?></li>
    <li class="limit-amount-item"><?php echo __('Max Limit: ',
                $textDomain) . $wcOrder->get_currency() . number_format($maxLimitAmount, 2, ".", ",") ?></li>
</ul>

<p class="tamara-error-content"><?php echo __('*The grand total of order is over/under limit of Tamara',
        $textDomain) ?></p>

<div class="tamara-confirm-button">
    <a class="tamara-confirm-button__proceed-link button"
       href="<?php echo wc_get_cart_url() ?>"><?php echo __('Back to Cart',
            $textDomain) ?></a>
    <br/>
    <a class="tamara-confirm-button__cancel-link button"
       href="<?php echo $wcOrder->get_cancel_order_url() ?>"><?php echo __('Cancel Order',
            $textDomain) ?></a>
</div>