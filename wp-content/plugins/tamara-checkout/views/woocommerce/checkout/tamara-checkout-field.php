<?php

use Tamara\Wp\Plugin\TamaraCheckout;
use Tamara\Wp\Plugin\Helpers\MoneyHelper;

$baseUrl = $viewParams['baseUrl'] ?? '/wp-content/plugins/tamara-checkout';
$imagePath = $baseUrl.'/assets/dist/img/';
$textDomain = $viewParams['textDomain'] ?? 'tamara';
$cartTotal = $viewParams['cartTotal'] ?? 0;

$totalToCalculate = TamaraCheckout::getInstance()->getTotalToCalculate($cartTotal);

$cartCurrency = __(get_woocommerce_currency(), $textDomain);
$logo = __('tamara-logo-transparent-color.svg', $textDomain);
$siteLocale = $viewParams['siteLocale'] ?? 'en';

$minAmount = $viewParams['minAmount'] ?? 0;
$maxAmount = $viewParams['maxAmount'] ?? 0;
$instalmentsMinAmount = $viewParams['instalmentsMinAmount'] ?? 0;
$instalmentsMaxAmount = $viewParams['instalmentsMaxAmount'] ?? 0;

$instalmentsThreshold = TamaraCheckout::getInstance()->getWCTamaraGatewayService()->getInstalmentsMinLimit();

// Check min/max of each payment type and verify if the payment type is enabled in settings
$isPayLaterEnabled = false;
if (TamaraCheckout::getInstance()->isPayByLaterEnabled() && $totalToCalculate < $maxAmount && $totalToCalculate > $minAmount) {
    $isPayLaterEnabledClass = 'tamara-show';
    $checkPayLaterOption = 'checked';
    $isPayLaterEnabled = true;
} else {
    $isPayLaterEnabledClass = 'tamara-hide';
    $checkPayLaterOption = '';
}

$isPayByInstalmentsEnabled = false;
if (TamaraCheckout::getInstance()->isPayByInstalmentsEnabled() && $totalToCalculate <= $instalmentsMaxAmount && $totalToCalculate >= $instalmentsMinAmount) {
    $isInstalmentsEnabledClass = 'tamara-show';
    $checkInstalmentsOption = 'checked';
    $isPayByInstalmentsEnabled = true;
} else {
    $isInstalmentsEnabledClass = 'tamara-hide';
    $checkInstalmentsOption = '';
}

$instalmentPlan = TamaraCheckout::calculateInstalmentPlan((float) $totalToCalculate);

$payForEachMonth = MoneyHelper::formatNumberGeneral($instalmentPlan[TamaraCheckout::INSTALMENT]);
$dueToDay = MoneyHelper::formatNumberGeneral($instalmentPlan[TamaraCheckout::DOWN_PAYMENT]);
?>

<div class="tamara_checkout_field form-row form-row-wide validate-required" id="tamara_checkout_field">
    <?php
    if ($isPayLaterEnabled && $isPayByInstalmentsEnabled) {
        ?>
        <label><b><?php echo __('Select your payment type', $textDomain) ?></b></label>
        <?php
    }
    ?>
    <span class="woocommerce-input-wrapper">
        <div class="tamara_checkout_field_pay_by_later <?php echo $isPayLaterEnabledClass ?>">
            <input type="radio" class="input-radio " value="pay_by_later" name="tamara_checkout_field" id="tamara_checkout_field_pay_by_later" <?php echo $checkPayLaterOption ?>><label for="tamara_checkout_field_pay_by_later" class="radio ">
                <?php echo __('Pay later in 30 days.', $textDomain) ?>
                <?php echo ' ' ?>
                <a class="tamara-learn-more tamara-product-widget" data-inject-template="false" data-price="<?php echo $totalToCalculate ?>" data-lang="<?php echo esc_attr($siteLocale) ?>" data-disable-injection="true"><?php echo __('Learn more.', $textDomain) ?></a>
            </label>
            <div id="tamara_checkout_field_pay_by_later_text">
                <ul class="tamara_checkout_field__description">
                    <li class="tamara_checkout_field_pay_by_later__description__item"><?php echo __('Fee-free and hassle-free', $textDomain) ?></li>
                    <li class="tamara_checkout_field_pay_by_later__description__item"><?php echo __('Pay it your way (debit, credit card, or ApplePay)', $textDomain) ?></li>
                </ul>
            </div>
        </div>
        <div class="tamara_checkout_field_pay_by_instalments <?php echo $isInstalmentsEnabledClass ?>">
            <input type="radio" class="input-radio " value="pay_by_instalments" name="tamara_checkout_field" id="tamara_checkout_field_pay_by_instalments" <?php echo $checkInstalmentsOption ?>><label for="tamara_checkout_field_pay_by_instalments" class="radio ">
                <?php echo __('Split into 3 payments, no fees.', $textDomain) ?>
                <?php echo ' ' ?>
                <a class="tamara-learn-more tamara-product-widget" data-inject-template="false" data-price="<?php echo $totalToCalculate ?>" data-payment-type="installment" data-disable-injection="true" data-lang="<?php echo esc_attr($siteLocale) ?>" data-installment-minimum-amount="<?php echo esc_attr($instalmentsThreshold) ?>"><?php echo __('Learn more.', $textDomain) ?></a>
            </label>
            <div id="tamara_checkout_field_instalments_text">
                <ul class="tamara_checkout_field__description">
                    <li class="tamara_checkout_field_pay_by_instalments__description__item"><?php echo __('Fee-free and hassle-free', $textDomain) ?></li>
                    <li class="tamara_checkout_field_pay_by_instalments__description__item"><?php echo __('3 Easy payments', $textDomain) ?></li>
                    <li class="tamara_checkout_field_pay_by_instalments__description__item"><?php echo __('Shop without draining your wallet', $textDomain) ?></li>
                </ul>
                <table>
                    <thead class="tamara_checkout_field_pay_by_instalments__head">
                    <tr class="tamara_checkout_field_pay_by_instalments__head__period">
                        <th><?php echo __('Due today', $textDomain) ?></th>
                        <th><?php echo __('1 month later', $textDomain) ?></th>
                        <th><?php echo __('2 months later', $textDomain) ?></th>
                    </tr>
                    </thead>
                    <tbody class="tamara_checkout_field_pay_by_instalments__body">
                    <tr class="tamara_checkout_field_pay_by_instalments__body__icon">
                        <td class="tamara_checkout_field_pay_by_instalments__body__icon--first"></td>
                        <td class="tamara_checkout_field_pay_by_instalments__body__icon--second"></td>
                        <td class="tamara_checkout_field_pay_by_instalments__body__icon--third"></td>
                    </tr>
                    <tr class="tamara_checkout_field_pay_by_instalments__body__amount">
                        <td>
                            <span><?php echo sprintf(__('SAR %.2f', $textDomain), $dueToDay) ?></span>
                        </td>
                        <td>
                            <span><?php echo sprintf(__('SAR %.2f', $textDomain), $payForEachMonth) ?></span>
                        </td>
                        <td>
                            <span><?php echo sprintf(__('SAR %.2f', $textDomain), $payForEachMonth) ?></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </span>
</div>
