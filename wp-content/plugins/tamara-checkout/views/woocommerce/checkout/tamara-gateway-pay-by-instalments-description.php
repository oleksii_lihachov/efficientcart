<?php

use Tamara\Wp\Plugin\Helpers\MoneyHelper;
use Tamara\Wp\Plugin\TamaraCheckout;

$textDomain = $viewParams['textDomain'] ?? 'tamara';
$defaultDescription = $viewParams['defaultDescription'] ?? '';
$cartTotal = $viewParams['cartTotal'] ?? 0;
$cartCurrency = __(get_woocommerce_currency(), $textDomain);
$totalToCalculate = TamaraCheckout::getInstance()->getTotalToCalculate($cartTotal);

$instalmentPlan = TamaraCheckout::calculateInstalmentPlan((float)$totalToCalculate);
$payForEachMonth = MoneyHelper::formatNumberGeneral($instalmentPlan[TamaraCheckout::INSTALMENT]);
$dueToDay = MoneyHelper::formatNumberGeneral($instalmentPlan[TamaraCheckout::DOWN_PAYMENT]);

?>

<div class="payment_box payment_method_tamara-gateway-pay-by-instalments">
    <div class="tamara-gateway-description">
        <p><?php echo __('Pay the third today and the rest on two payments made over the next two months.', $textDomain) ?></p>
        <table class="tamara-gateway-description__table">
            <tr class="tamara-gateway-description__table__icon">
                <td class="tamara-gateway-description__table__icon--first"></td>
                <td class="tamara-gateway-description__table__icon--second"></td>
                <td class="tamara-gateway-description__table__icon--third"></td>
            </tr>
            <tr class="tamara-gateway-description__table__head">
                <td><span><?php echo __('Enter your data', $textDomain) ?></span></td>
                <td><span><?php echo __('Pay the first payment', $textDomain) ?></span></td>
                <td><span><?php echo __('Pay however you want', $textDomain) ?></span></td>
            </tr>
            <tr class="tamara-gateway-description__table__text">
                <td>
                    <span><?php echo __('Mobile number, ID details (first order only) and payment details', $textDomain) ?></span>
                </td>
                <td>
                    <span><?php echo __('Pay easily via any bank card or Apple Pay to confirm your order', $textDomain) ?></span>
                </td>
                <td>
                    <span><?php echo __('Pay the remaining dues within 2 months via Tamara App', $textDomain) ?></span>
                </td>
            </tr>
        </table>
        <ul class="tamara-gateway-description__instalments">
            <li class="tamara-gateway-description__instalments--item-first">
                <p class="tamara-gateway-description__instalments-item-text">
                    <?php echo sprintf(__('%.2f %s', $textDomain), $dueToDay, $cartCurrency).' '.__('Due today', $textDomain) ?>
                </p>
            </li>
            <li class="tamara-gateway-description__instalments--item-second">
                <p class="tamara-gateway-description__instalments-item-text">
                    <?php echo sprintf(__('%.2f %s', $textDomain), $payForEachMonth, $cartCurrency).' '.__('1 month later', $textDomain) ?>
                </p>
            </li>
            <li class="tamara-gateway-description__instalments--item-third">
                <p class="tamara-gateway-description__instalments-item-text">
                    <?php echo sprintf(__('%.2f %s', $textDomain), $payForEachMonth, $cartCurrency).' '.__('2 months later', $textDomain) ?>
                </p>
            </li>
        </ul>
        <p class="tamara-gateway-description__default"><?php echo $defaultDescription ?></p>
    </div>
</div>