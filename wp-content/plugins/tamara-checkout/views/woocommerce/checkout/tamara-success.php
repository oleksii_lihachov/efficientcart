<?php
$textDomain = $viewParams['textDomain'] ?? 'tamara';

get_header();
?>
    <div class="tamara-success">
        <div class="tamara-success__heading">
            <div class="tamara-success__heading__logo">
            </div>
            <p class="tamara-success__heading__text"><?php echo __('Order Received by Tamara', $textDomain) ?></p>
        </div>
        <div class="tamara-success__content">
            <h4><?php echo __("Please don't close this window.", $textDomain) ?></h4>
            <h4><?php echo __('Your order paying with Tamara is still under process.', $textDomain) ?></h4>
        </div>
    </div>
<?php
get_footer();
