<?php
$siteLocale = $viewParams['siteLocale'] ?? 'en';
?>
<div class="tamara-checkout-icon">
    <div class="tamara-widget" data-lang="<?php echo esc_attr($siteLocale) ?>"></div>
</div>
