<?php
/**
 * Plugin Name: Tamara Checkout
 * Plugin URI:  https://tamara.co/
 * Description: Plugin for checking out using Tamara payment method
 * Author:      dev@tamara.co, nptrac@yahoo.com
 * Author URI:  https://tamara.co/
 * Version:     1.5.0
 * Text Domain: tamara
 */

use Tamara\Wp\Plugin\TamaraCheckout;

defined('TAMARA_CHECKOUT_VERSION') || define('TAMARA_CHECKOUT_VERSION', '1.5.0');

// Use autoload if it isn't loaded before
// phpcs:ignore PSR2.ControlStructures.ControlStructureSpacing.SpacingAfterOpenBrace
if ( ! class_exists(TamaraCheckout::class)) {
    require __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
}

$config = require(__DIR__ . DIRECTORY_SEPARATOR . 'config.php');
$config = array_merge($config, [
    'pluginFilename' => __FILE__,
]);

// We need to set up the main instance for the plugin.
// Use 'init' event but with low (<10) processing order to be able to execute before -> able to add other init.
add_action('init', function () use ($config) {
    TamaraCheckout::initInstanceWithConfig($config);

    register_activation_hook(__FILE__, [TamaraCheckout::getInstance(), 'activatePlugin']);
    register_deactivation_hook(__FILE__, [TamaraCheckout::getInstance(), 'deactivatePlugin']);

    TamaraCheckout::getInstance()->initPlugin();
}, 7);
