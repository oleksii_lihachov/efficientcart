<?php

use Tamara\Wp\Plugin\Services\TamaraNotificationService;
use Tamara\Wp\Plugin\Services\ViewService;
use Tamara\Wp\Plugin\Services\WCTamaraGateway;
use Tamara\Wp\Plugin\Services\WCTamaraGatewayPayByInstalments;

$textDomain = 'tamara';

$config = [
    'version' => TAMARA_CHECKOUT_VERSION,
    'basePath' => __DIR__,
    'baseUrl' => plugins_url(null, __FILE__),
    'textDomain' => $textDomain,
    'services' => [
        ViewService::class => [
        ],
        WCTamaraGateway::class => [
            'textDomain' => $textDomain,
        ],
        WCTamaraGatewayPayByInstalments::class => [
            'textDomain' => $textDomain,
        ],
        TamaraNotificationService::class => [
            'textDomain' => $textDomain,
        ],
    ],
];

return $config;
