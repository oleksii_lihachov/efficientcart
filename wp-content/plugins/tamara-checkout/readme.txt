=== Tamara Checkout ===
Contributors: dev@tamara.co, nptrac@yahoo.com
Tags: tamara, tamara-checkout, tamara-payment, e-commerce, store, sales, sell, woo, shop, cart, checkout, payments, woocommerce
Requires at least: 5.0
Tested up to: 5.5
Requires PHP: 7.1
Stable tag: 1.5.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Pay later with tamara

== Description ==

Tamara is the best way to pay for your online shopping. advantage icon. Buy now and pay later. No immediate payment needed.

Tamara is a solution for brick and mortar and e-commerce players in the GCC. We are a regional startup with a bold vision and a global experienced team.

We provide a Buy Now Pay Later solution in Saudi Arabia and the UAE. Our solution solves local challenges and provides merchants and their customers with significant value.

The plugin integrates with the merchant's checkout. It tackles cash on delivery challenges and increases order frequency, average order size and conversion. It also provides consumers with an exceptional checkout experience.

### FOR MERCHANTS
#### Your customers want to buy now and pay later!
Customers enjoy a flexible payment solution to buy the product they love. No hidden fees. No risk.

#### Why tamara?
Unleash your store’s potential by enabling ​Pay Later ​with tamara!

* Increase sales and average order value.
* Tackle cash on delivery.
* Reduce fraud and non-payment risks.

#### Benefits to Merchants

* More sales: Higher average order value, conversion, repeat purchases and less card transaction declined.
* Access to quality customers: tamara directs new customers to merchants through its network.
* No collections: tamara collects from the customers, not merchants. 
* No fraud and credit risk: tamara assumes 100% of payment risk on behalf of the merchant.
* No COD: In case merchants offer it, we'll take this headache away from them and provide a better alternative.
* Logistics: Freedom to select the top couriers (e.g. DHL) as there is no restriction on specific couriers who collect cash. 

### FOR SHOPPERS
#### Pay later with tamara
Buy online and pay later after your goods are delivered. No hidden fees.

#### What is tamara?
tamara is the best way to pay for your online shopping.

* Buy now and pay later
* No hidden fees
* Smooth process

#### Benefits to Customers

* Convenient and smooth checkout experience: No friction of having customers to pay upfront. 
* In-store experience for e-commerce: Customers can try the product at home without having to pay upfront.
* Payment flexibility and better money management: Pay later in 30 days or in installments.
* Trust: customers can purchase the products they love with peace of mind. No credit/debit card details needed upfront, no chasing logistics companies for delivery. 

== Installation ==

= Minimum Requirements =

* Wordpress 5.2 or greater is recommended
* Woocommerce 4.0 or greater is recommended

### Install and Enable/Disable the plugin
* The Tamara Checkout plugin is now available for WordPress. After installing it, simply click on **Activate** the plugin.
* Please note that the plugin can only be activated when Woocommerce is active.
* After the plugin is activated, there is an option to Enable/Disable the plugin in **Woocommerce => Settings => Payments => Tamara Gateway Settings**

* You can find out more how to install and use the plugin with our user guides documentation.

== Frequently Asked Questions ==

= Where can I find Tamara Checkout documentation and user guides? =

For more information about the documentation, user guides and the FAQs, please visit: [our website](https://tamara.co/en/faq.html)

== Changelog ==

= 1.5.0 - 2021-04-19 =

Enhancements:

*  Split payment types into 2 different methods on checkout screen.
*  Add before add to cart form position to display popup.
*  Add custom title fields for payment types on both languages EN & AR.
*  Add default titles for payment types on both languages EN & AR.
*  Update descriptions on checkout screen for payment types.
*  Update translation files for new strings.

= 1.4.31 - 2021-04-07 =

Bugfixes:

*  Fix response for order created failed through Rest API.
*  Update payment type param format for request body.

= 1.4.30 - 2021-04-07 =

Bugfixes:

* Fix limit amounts for each country payment types on checkout.


= 1.4.29 - 2021-03-18 =

Enhancements:

* Add Tamara Cancel Id to order meta data.
* Disable Iframe checkout on mobile.
* Add Tamara checkout data to order meta data created through RESTAPI.
 * Create WC Order Endpoint `wp-json/wc/v3/orders/`:
  - "meta_data": [
            {
                "key": "tamara_checkout_session_id",
                "value": "18f6efe9-6cac-48a2-bc79-28b3317a8b96"
            },
            {
                "key": "tamara_checkout_url",
                "value": "https://checkout-sandbox.tamara.co/checkout/18f6efe9-6cac-48a2-bc79-28b3317a8b96?locale=en_US&orderId=9dde35e1-259f-423b-9149-c259e393ea35"
            }
        ]
 * Get Order details Endpoint `wp-json/wc/v3/orders/{order-id}`:
  - "meta_data": [
            {
                "key": "tamara_checkout_session_id",
                "value": "8944639c-e762-4939-a230-7ceca309c561"
            },
            {
                "key": "tamara_checkout_url",
                "value": "https://checkout-sandbox.tamara.co/checkout/8944639c-e762-4939-a230-7ceca309c561?locale=en_US&orderId=8f893a5f-ca3a-4691-a9cf-036758234d08"
            }
        ]

= 1.4.26 - 2021-03-02 =

Bugfixes:

* Add limit amount for Pay By Later popup widget to display on frontend.
* Add force pull from remote param for get payment types through Tamara API.

Enhancements:

* Update translation files for new error codes.

= 1.4.25 - 2021-02-25 =

Enhancements:

* Edit instalment plans amount to display on frontend.
* Add default country code to verify Tamara API.

= 1.4.22 - 2021-01-30 =

Bugfixes:

* Get correct product price to display on instalment popup.
* Fix Tamara widget call repeat on checkout.
* Add redirect url on order pay page.

= 1.4.6 - 2020-12-30 =

Enhancements:

* Improve Tamara popups to have better view.
* Use custom scripts to call Tamara widgets.
* Remove Capture ID in condition when creating a Refund request.

= 1.4.4 - 2020-12-09 =

Enhancements:

* Improve Tamara popups to have better view.
* Edit get Woocommerce checkout/cart methods.
* Update translation for Arabic language.

= 1.4.1 - 2020-12-03 =

Enhancements:

* Improve Tamara Checkout fields to have better view.
* Edit options for Pay By Later and Pay By Instalments on checkout.

= 1.4.0 - 2020-11-30 =

Bugfixes:

* Fix errors and not loading services when unused.
* Move some methods to Tamara Checkout class.

Enhancements:

* Add Tamara Pay By Instalments service.
* Add Update status and add order note wrapper.
* Add options for Pay By Later and Pay By Instalments.

= 1.3.4 - 2020-11-12 =

Bugfixes:

* Fix errors on duplicate Jquery modal script.
* Enhance get payment types method.
* Fix payment icon layout displays on checkout.

Enhancements:

* Add allowed shipping country codes option.
* Hide Tamara Payment method on checkout if the total order is under/over limit.
* Polish Tamara Success page returned.

= 1.3.3 - 2020-11-12 =

Bugfixes:

* Fix errors on check Woocommerce existence.

Enhancements:

* Add allow message log option.

= 1.2.8 - 2020-11-03 =

Bugfixes:

* Fix errors display on Iframe checkout.
* Add some error codes for error displaying on checkout.
* Fix errors when webhook id value displays on Admin settings page.

Enhancements:

* Not run the webhook events on authorised orders.

= 1.2.3 - 2020-09-25 =

Bugfixes:

* Fix errors display on Iframe checkout.
* Edit the parse request to fix conflicts with other plugins.

= 1.2.2 - 2020-09-20 =

Enhancements:

* Add Iframe for Tamara Checkout.

= 1.2.1 - 2020-09-19 =

Enhancements:

* Add specific endpoints for merchant urls.

= 1.2.0 - 2020-09-18 =

Enhancements:

* Add Custom Tamara status for Capturing Payment.

= 1.1.9 - 2020-09-13 =

Bugfixes:

* Fix automatic disabling issue.
* Repair to have better readme file.

= 1.1.8 - 2020-09-06 =

Bugfixes:

* Fixes errors displayed on Checkout page. 

Enhancements:

* Add webhook to handle Order declined and Order expired from Tamara.
* Update the Arabic translation.

= 1.0 - 2020-08-28 =

Release version