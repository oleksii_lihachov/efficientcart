var tamaraEnvToggle = document.getElementById('woocommerce_tamara-gateway_environment');
var valueSelected;

// Display/Hide fields on selected
function getValueSelected() {
    valueSelected = tamaraEnvToggle.value;

    if ('live_mode' === valueSelected) {
        document.querySelector('#woocommerce_tamara-gateway_live_api_url').closest('tr').style.display = 'table-row';
        document.querySelector('#woocommerce_tamara-gateway_live_api_token').closest('tr').style.display = 'table-row';
        document.querySelector('#woocommerce_tamara-gateway_live_notification_token').closest('tr').style.display = 'table-row';
        document.querySelector('#woocommerce_tamara-gateway_sandbox_api_url').closest('tr').style.display = 'none';
        document.querySelector('#woocommerce_tamara-gateway_sandbox_api_token').closest('tr').style.display = 'none';
        document.querySelector('#woocommerce_tamara-gateway_sandbox_notification_token').closest('tr').style.display = 'none';

    } else if ('sandbox_mode' === valueSelected) {
        document.querySelector('#woocommerce_tamara-gateway_live_api_url').closest('tr').style.display = 'none';
        document.querySelector('#woocommerce_tamara-gateway_live_api_token').closest('tr').style.display = 'none';
        document.querySelector('#woocommerce_tamara-gateway_live_notification_token').closest('tr').style.display = 'none';
        document.querySelector('#woocommerce_tamara-gateway_sandbox_api_url').closest('tr').style.display = 'table-row';
        document.querySelector('#woocommerce_tamara-gateway_sandbox_api_token').closest('tr').style.display = 'table-row';
        document.querySelector('#woocommerce_tamara-gateway_sandbox_notification_token').closest('tr').style.display = 'table-row';
    }

}

// Get the selected value on change
tamaraEnvToggle.addEventListener('change', getValueSelected);

// Get the selected value on save
window.addEventListener('load', getValueSelected);



