var successUrl;
var cancelUrl;
var failureUrl;
var tamaraCKOConfig = {
    init: init,
};
(function ($) {
    const userAgent = window.navigator.userAgent;
    const userAgentToLowerCase = userAgent.toLowerCase();

    function isIosMobileDevice() {
        return userAgentToLowerCase.indexOf('iphone') > -1 || userAgentToLowerCase.indexOf('ipad') > -1
            || userAgentToLowerCase.indexOf('macintosh') > -1 && 'ontouchend' in document;
    }

    $(document).ajaxSuccess(function (event, xhr, settings) {
        if (settings.url == wc_checkout_params.checkout_url && 'success' === xhr.responseJSON.result && 'tamara' === xhr.responseJSON.paymentMethod) {
            const md = new MobileDetect(userAgent);
            if (md.mobile() !== null || isIosMobileDevice()) {
                window.location.replace(xhr.responseJSON.tamaraIframeUrl)
            } else {
                try {
                    successUrl = xhr.responseJSON.tamaraSuccessUrl;
                    cancelUrl = xhr.responseJSON.tamaraCancelUrl;
                    failureUrl = xhr.responseJSON.tamaraFailureUrl;
                    TamaraCheckoutFrame.checkout(xhr.responseJSON.tamaraIframeUrl);
                    init();
                } catch (e) {
                }
            }
        }
    });
})(jQuery);

function success() {
    window.location.replace(successUrl);
}

function cancel() {
    window.location.replace(cancelUrl);
}

function failure() {
    window.location.replace(failureUrl);
}

function init() {
    TamaraCheckoutFrame.addEventHandlers(TamaraCheckoutFrame.Events.SUCCESS, success);
    TamaraCheckoutFrame.addEventHandlers(TamaraCheckoutFrame.Events.CANCELED, cancel);
    TamaraCheckoutFrame.addEventHandlers(TamaraCheckoutFrame.Events.FAILED, failure);
}