(function ($) {
    // Trigger cn WC checkout page
    $(document).on('click', 'input[name="tamara_checkout_field"]', {}, function () {
        togglePaymentTypes();
    });

    // Trigger on WC order-pay page
    $('input[name="tamara_checkout_field"]').click(function () {
        togglePaymentTypes();
    });

    function togglePaymentTypes() {
        let id = $('input[name="tamara_checkout_field"]:checked').attr('id')
        if (id === 'tamara_checkout_field_pay_by_instalments') {
            $('#tamara_checkout_field_instalments_text').slideDown(230)
            $('#tamara_checkout_field_pay_by_later_text').slideUp(230)
        } else {
            $('#tamara_checkout_field_instalments_text').slideUp(230)
            $('#tamara_checkout_field_pay_by_later_text').slideDown(230)
        }
    }
})(jQuery);