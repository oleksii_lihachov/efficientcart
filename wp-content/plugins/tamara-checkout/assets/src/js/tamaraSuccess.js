(function ($) {
    let currentSuccessUrl = new URL(window.location.href);
    $(document).ready(function () {
        function tamaraAuthorise() {
            $.post(
                tamaraCheckoutParams.ajaxUrl, {
                    action: 'tamara-authorise',
                    orderId: currentSuccessUrl.searchParams.get('orderId'),
                }, function (response) {
                    if (response.authoriseSuccessUrl !== undefined) {
                        // If the order is authorised successfully, redirect to Woocommerce Thank you page
                        window.location.replace(response.authoriseSuccessUrl)
                    }
                }
            )
        }

        tamaraAuthorise();
        // The ajax calls every 3s to check if the order is authorised
        setInterval(tamaraAuthorise, 3000);
    });

    // Redirect to cart page if the order can not be authorised after 30s
    function tamaraAuthoriseFailedRedirect() {
        window.location.replace(tamaraCheckoutParams.tamaraAuthoriseFailedUrl)
    }

    setTimeout(tamaraAuthoriseFailedRedirect, 30000)
})(jQuery);
