<?php

namespace Tamara\Wp\Plugin\Services;

use Tamara\Wp\Plugin\TamaraCheckout;
use Tamara\Wp\Plugin\Traits\ConfigTrait;
use Tamara\Wp\Plugin\Traits\ServiceTrait;
use Tamara\Wp\Plugin\Traits\WPAttributeTrait;

class WCTamaraGatewayPayByInstalments extends WCTamaraGateway
{
    use ConfigTrait;
    use ServiceTrait;
//    use WPAttributeTrait;

    public $textDomain = 'tamara';

    /**
     * Initialize attributes that are fixed
     */
    protected function initBaseAttributes()
    {
        $this->id = TamaraCheckout::TAMARA_GATEWAY_PAY_BY_INSTALMENTS_ID;
        $this->order_button_text = __('Proceed to Tamara Payment', $this->textDomain);
        $this->icon = $this->get_option('logo_image_url');
        $this->paymentType = static::PAYMENT_TYPE_PAY_BY_INSTALMENTS;
        if (is_admin()) {
            $this->title = __(static::TAMARA_GATEWAY_DEFAULT_TITLE, $this->textDomain);
        } else {
            $this->title = __($this->getPaymentTypeTitleMapping()[$this->id], $this->textDomain);
        }
    }

    /**
     * @param $cartTotal
     *
     * @return bool
     */
    protected function isCartTotalValid($cartTotal)
    {
        // Force pull country payment types from remote api
        $countryPaymentTypes = $this->getCountryPaymentTypes();
        $paymentTypes = $this->getPaymentTypes();

        return ($this->populateInstalmentsMinLimit() <= $cartTotal && $this->populateInstalmentsMaxLimit() >= $cartTotal);
    }
}