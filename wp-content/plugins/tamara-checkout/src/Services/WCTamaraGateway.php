<?php

namespace Tamara\Wp\Plugin\Services;

use DateTimeImmutable;
use Exception;
use Tamara\Client;
use Tamara\Configuration;
use Tamara\HttpClient\NyholmHttpAdapter;
use Tamara\Model\Checkout\PaymentType;
use Tamara\Model\Money;
use Tamara\Model\Order\Address;
use Tamara\Model\Order\Consumer;
use Tamara\Model\Order\Discount;
use Tamara\Model\Order\MerchantUrl;
use Tamara\Model\Order\Order;
use Tamara\Model\Order\OrderItem;
use Tamara\Model\Order\OrderItemCollection;
use Tamara\Model\Payment\Capture;
use Tamara\Model\ShippingInfo;
use Tamara\Request\Checkout\CreateCheckoutRequest;
use Tamara\Request\Order\CancelOrderRequest;
use Tamara\Request\Payment\CaptureRequest;
use Tamara\Request\Webhook\RegisterWebhookRequest;
use Tamara\Request\Webhook\RemoveWebhookRequest;
use Tamara\Response\Checkout\GetPaymentTypesResponse;
use Tamara\Response\Payment\CaptureResponse;
use Tamara\Wp\Plugin\Helpers\MoneyHelper;
use Tamara\Wp\Plugin\TamaraCheckout;
use Tamara\Wp\Plugin\Traits\ConfigTrait;
use Tamara\Wp\Plugin\Traits\ServiceTrait;
use Tamara\Wp\Plugin\Traits\WPAttributeTrait;
use WC_Admin_Settings;
use WC_Order;
use WC_Order_Refund;
use WC_Payment_Gateway;
use WC_Product;
use WP_REST_Response;

/**
 * Class WCTamaraGateway
 * @package Tamara\Wp\Plugin\Services
 * @method \Tamara\Wp\Plugin\TamaraCheckout getContainer()
 */
class WCTamaraGateway extends WC_Payment_Gateway
{
    use ConfigTrait;
    use ServiceTrait;
    use WPAttributeTrait;

    public const
        DEFAULT_COUNTRY_CODE = 'SA',
        IPN_SLUG = 'tamara-ipn',
        WEBHOOK_SLUG = 'tamara-webhook',
        PAYMENT_SUCCESS_SLUG = 'tamara-payment-success',
        PAYMENT_CANCEL_SLUG = 'tamara-payment-cancel',
        PAYMENT_FAIL_SLUG = 'tamara-payment-fail',
        PAYMENT_TYPE_PAY_BY_LATER = 'PAY_BY_LATER',
        PAYMENT_TYPE_PAY_BY_INSTALMENTS = 'PAY_BY_INSTALMENTS',
        REGISTERED_WEBHOOKS = [
        'order_expired',
        'order_declined',
    ],
        TAMARA_POPUP_WIDGET_POSITIONS = [
        'woocommerce_single_product_summary' => 'woocommerce_single_product_summary',
        'woocommerce_after_single_product_summary' => 'woocommerce_after_single_product_summary',
        'woocommerce_after_add_to_cart_form' => 'woocommerce_after_add_to_cart_form',
        'woocommerce_before_add_to_cart_form' => 'woocommerce_before_add_to_cart_form',
        'woocommerce_product_meta_end' => 'woocommerce_product_meta_end',
    ],
        ENVIRONMENT_LIVE_MODE = 'live_mode',
        ENVIRONMENT_SANDBOX_MODE = 'sandbox_mode',

        TAMARA_GATEWAY_DEFAULT_TITLE = 'Pay Later with Tamara',
        PAYMENT_TYPE_PAY_BY_LATER_DEFAULT_TITLE = 'Pay in 30 days without fees with Tamara',
        PAYMENT_TYPE_PAY_BY_LATER_DEFAULT_TITLE_AR = 'اطلب الآن وادفع خلال 30 یوم مع تمارا. بدون رسوم',
        PAYMENT_TYPE_PAY_BY_INSTALMENTS_DEFAULT_TITLE = 'Split into 3 payments, without fees with Tamara',
        PAYMENT_TYPE_PAY_BY_INSTALMENTS_DEFAULT_TITLE_AR = 'قسِّمها على 3 دفعات بدون رسوم مع تمارا';

    public $payByLaterEnabled;
    public $payByInstalmentsEnabled;
    public $environment;
    public $apiUrl;
    public $apiToken;
    public $notificationToken;

    /**
     * @var Client $tamaraClient
     */
    public $tamaraClient;
    public $tamaraStatus = [];
    public $webhookEnabled;
    public $beautifyMerchantUrlsEnabled;
    public $customLogMessageEnabled;
    public $popupWidgetPosition;

    protected $webhookId;
    protected $minLimit;
    protected $maxLimit;
    protected $instalmentsMinLimit;
    protected $instalmentsMaxLimit;
    protected $errorMap;
    protected $paymentType;
    protected $paymentTypes = [];

    /**
     * WCTamaraGateway constructor.
     */
    public function __construct()
    {
        $this->initBaseAttributes();
        $this->initSettingAttributes();
    }

    /**
     * Handle remote request after saving
     *
     * @param $settings
     */
    public function onSaveSettings($settings)
    {
        if ($this->validateRequiredFields()) {
            $this->initTamaraClient();
            $this->settings['country_payment_types'] = $this->getCountryPaymentTypes();
            $paymentTypes = $this->getPaymentTypes();
            $this->settings['webhook_id'] = $this->populateTamaraWebhook();
            $this->settings['min_limit'] = $this->populateMinLimit();
            $this->settings['max_limit'] = $this->populateMaxLimit();
            $this->settings['instalments_min_limit'] = $this->populateInstalmentsMinLimit();
            $this->settings['instalments_max_limit'] = $this->populateInstalmentsMaxLimit();
        } else {
            $this->settings['webhook_enabled'] = 'no';
            $this->settings['webhook_id'] = null;
            $this->settings['min_limit'] = null;
            $this->settings['max_limit'] = null;
            $this->settings['instalments_min_limit'] = null;
            $this->settings['instalments_max_limit'] = null;
            $this->settings['country_payment_types'] = [];
        }

        $this->processRewriteRules();

        $this->updateThisSettingsToOptions();

        $this->initSettingAttributes();
        $this->initFormFields();
    }

    public function init()
    {
        // Load the settings form.
        $this->initFormFields();

        // Process admin options
        add_action('woocommerce_update_options_payment_gateways_'.$this->id, [$this, 'processAdminOptions']);

        // Add Tamara Note on Order Received page
        add_filter('woocommerce_thankyou_order_received_text', [$this, 'tamaraOrderReceivedText'], 10, 2);

        // Add Tamara Icon on Checkout
        add_filter('woocommerce_gateway_icon', [$this, 'setTamaraIconForPaymentGateway'], 10, 2);

        // Tamara Capture payment
        add_action('woocommerce_order_status_changed', [$this, 'tamaraCapturePayment'], 10, 4);

        // Handle cancel order before capturing
        add_action('woocommerce_order_status_changed', [$this, 'tamaraCancelOrder'], 10, 4);

        // Invoke a filter to hide Tamara Gateway on checkout
        add_filter('woocommerce_available_payment_gateways', [$this, 'hideTamaraGatewayOnCheckout'], 9999, 1);

        // Invoke a filter to update description for Tamara Gateway on checkout
        add_filter('woocommerce_gateway_description', [$this, 'renderPaymentTypeDescription'], 20, 2);
    }

    public function getPayByLaterTitle()
    {
        return !empty($this->get_option('pay_by_later_title')) ? $this->get_option('pay_by_later_title') : static::PAYMENT_TYPE_PAY_BY_LATER_DEFAULT_TITLE;
    }

    public function getPayByLaterTitleAr()
    {
        return !empty($this->get_option('pay_by_later_title_ar')) ? $this->get_option('pay_by_later_title_ar') : static::PAYMENT_TYPE_PAY_BY_LATER_DEFAULT_TITLE_AR;
    }

    public function getPayByInstalmentsTitle()
    {
        return !empty($this->get_option('pay_by_instalments_title')) ? $this->get_option('pay_by_instalments_title') : static::PAYMENT_TYPE_PAY_BY_INSTALMENTS_DEFAULT_TITLE;
    }

    public function getPayByInstalmentsTitleAr()
    {
        return !empty($this->get_option('pay_by_instalments_title_ar')) ? $this->get_option('pay_by_instalments_title_ar') : static::PAYMENT_TYPE_PAY_BY_INSTALMENTS_DEFAULT_TITLE_AR;
    }

    public function getStoreBaseCountryCode()
    {
        return !empty(WC()->countries->get_base_country()) ? WC()->countries->get_base_country() : self::DEFAULT_COUNTRY_CODE;
    }

    public function getDefaultBillingCountryCode()
    {
        return !empty($this->settings['default_billing_country_code']) ? $this->settings['default_billing_country_code'] : $this->getStoreBaseCountryCode();
    }

    public function isBeautifyMerchantUrlsEnabled()
    {
        return ($this->beautifyMerchantUrlsEnabled === 'yes' && $this->enabled === 'yes');
    }

    public function isWebhookEnabled()
    {
        return ($this->webhookEnabled === 'yes');
    }

    public function isSandboxMode()
    {
        return (static::ENVIRONMENT_SANDBOX_MODE === $this->environment);
    }

    public function isLiveMode()
    {
        return (static::ENVIRONMENT_LIVE_MODE === $this->environment);
    }

    public function initSettings()
    {
        parent::init_settings();
    }

    public function processAdminOptions()
    {
        parent::process_admin_options();
    }

    /** @noinspection PhpFullyQualifiedNameUsageInspection */
    /**
     * Render description for Tamara payment types on checkout
     *
     * @param $description
     * @param $gatewayId
     *
     * @return string
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function renderPaymentTypeDescription($description, $gatewayId)
    {
        if ($this->id === $gatewayId) {
            $cartTotal = WC()->cart->total;
            $description .= TamaraCheckout::getInstance()->getServiceView()->render('views/woocommerce/checkout/'.$this->id.'-description',
                [
                    'textDomain' => $this->textDomain,
                    'cartTotal' => $cartTotal,
                    'defaultDescription' => $this->populateTamaraDefaultDescription(),
                ]);
        }

        return $description;
    }

    /**
     * Hide Tamara Payment Gateway on checkout if total value of order is under/over limit
     * and the shipping country is different than countries set in Tamara payment settings
     *
     * @param WC_Payment_Gateway $availableGateways
     *
     * @return WC_Payment_Gateway $availableGateways
     *
     * @throws Exception
     */
    public function hideTamaraGatewayOnCheckout($availableGateways)
    {
        if (is_checkout()) {
            $tamaraAllowedShippingCountryCodes = array_map('trim', explode(',', strtoupper($this->get_option('allowed_shipping_country_codes'))));
            $customerShippingCountry = WC()->customer->get_shipping_country();

            if (!$this->isCartTotalValid(TamaraCheckout::getInstance()->getTotalToCalculate(WC()->cart->total)) ||
                !in_array($customerShippingCountry, $tamaraAllowedShippingCountryCodes)) {
                unset($availableGateways[$this->id]);
            }
        }

        return $availableGateways;
    }

    /** @noinspection PhpFullyQualifiedNameUsageInspection */
    /**
     * Return the Tamara gateway's icon on checkout
     *
     * @param $iconHtml
     * @param $gatewayId
     *
     * @return string
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setTamaraIconForPaymentGateway($iconHtml, $gatewayId)
    {
        if ($this->id == $gatewayId && null == $this->icon) {
            $iconHtml = $this->getContainer()->getServiceView()->render('views/woocommerce/checkout/tamara-checkout-icon',
                [
                    'siteLocale' => substr(get_locale(), 0, 2) ?? 'en',
                ]);
        }

        return $iconHtml;
    }

    /** @noinspection PhpFullyQualifiedNameUsageInspection */
    /**
     * Add Tamara Note after successful payment
     *
     * @param string $str
     * @param int $orderId
     *
     * @return string
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function tamaraOrderReceivedText($str, $orderId)
    {
        $wcOrder = wc_get_order($orderId);
        $payment_method = $wcOrder->get_payment_method();

        if (!empty($payment_method) && $this->id === $payment_method) {

            return $str.$this->getContainer()->getServiceView()->render('views/woocommerce/checkout/tamara-order-received-button',
                    [
                        'textDomain' => $this->textDomain,
                    ]);
        }

        return $str;
    }

    /**
     * Create Tamara Checkout session, if failed, put errors to `wc_notices`
     *
     * @param $orderId
     *
     * @return array|bool|WP_REST_Response
     * @throws \WC_Data_Exception
     */
    public function tamaraCheckoutSession($orderId)
    {
        $wcOrder = wc_get_order($orderId);
        $initialPaymentMethod = $wcOrder->get_payment_method();

        if (!empty($wcOrder)) {
            $wcOrder->set_payment_method(TamaraCheckout::TAMARA_GATEWAY_ID);
            $wcOrder->set_payment_method_title($this->getPaymentTypeTitleMapping()[$initialPaymentMethod]);
            $wcOrder->save();
        }

        if (TamaraCheckout::isRestRequest()) {
            $restApiRequest = TamaraCheckout::getInstance()->getRestApiRequest();
            $paymentMethod = $restApiRequest->get_param('payment_method');
            if (TamaraCheckout::TAMARA_GATEWAY_ID === $paymentMethod) {
                $checkoutPaymentType = $this->getPaymentTypeMapping()[TamaraCheckout::TAMARA_GATEWAY_ID];
            } else {
                $checkoutPaymentType = $this->getPaymentTypeMapping()[TamaraCheckout::TAMARA_GATEWAY_PAY_BY_INSTALMENTS_ID];
            }
        } else {
            $checkoutPaymentType = $this->paymentType;
        }

        try {
            $checkoutResponse = $this->createTamaraCheckoutSession($wcOrder, $checkoutPaymentType);
            TamaraCheckout::getInstance()->logMessage(sprintf("Tamara Checkout Session Response Data: %s", print_r($checkoutResponse, true)));
        } catch (Exception $tamaraCheckoutException) {
            $errorMessage = __("Tamara Service unavailable! Please try again later.", $this->textDomain);
            wc_add_notice($errorMessage, 'error');

            // Log if service unavailable
            TamaraCheckout::getInstance()->logMessage("Tamara Checkout Session cannot be created."."\n");
            TamaraCheckout::getInstance()->logMessage(
                sprintf(
                    "Error message: '%s'.\nTrace: %s",
                    $tamaraCheckoutException->getMessage(),
                    $tamaraCheckoutException->getTraceAsString()
                )
            );
        }

        if (isset($checkoutResponse) && $checkoutResponse->isSuccess()) {
            $tamaraCheckoutUrl = $checkoutResponse->getCheckoutResponse()->getCheckoutUrl();
            $tamaraCheckoutSessionId = $checkoutResponse->getCheckoutResponse()->getCheckoutId();

            update_post_meta($orderId, 'tamara_checkout_session_id', $tamaraCheckoutSessionId);
            update_post_meta($orderId, 'tamara_checkout_url', $tamaraCheckoutUrl);
            update_post_meta($orderId, 'tamara_payment_type', $checkoutPaymentType);

            return [
                'result' => 'success',
                'redirect' => $tamaraCheckoutUrl,
                'tamaraCheckoutUrl' => $tamaraCheckoutUrl,
                'tamaraCheckoutSessionId' => $tamaraCheckoutSessionId,
            ];
        }

        if (isset($checkoutResponse) && !$checkoutResponse->isSuccess()) {
            $errorMap = $this->errorMap;

            $tamaraErrors = $checkoutResponse->getErrors();
            $errors = [];
            if (!empty($tamaraErrors) && is_array($tamaraErrors)) {
                foreach ($tamaraErrors as $tmpKey => $tamaraError) {
                    $errorCode = $tamaraError['error_code'] ?? null;
                    if ($errorCode && isset($errorMap[$errorCode])) {
                        $errors[] = $errorMap[$errorCode];
                    }
                }
            }
            if (empty($errors)) {
                $errorCode = $checkoutResponse->getMessage();
                if ($errorCode && isset($errorMap[$errorCode])) {
                    $errors[] = $errorMap[$errorCode];
                }

                if (!$checkoutResponse->isSuccess() && empty($errors)) {
                    $errors[] = $errorMap['tamara_disabled'];
                }
            }

            if (TamaraCheckout::isRestRequest()) {
                add_filter('rest_post_dispatch', function ($result, $rest_api_server, $request) use ($orderId, $errors) {
                    $httpStatusCode = 400;
                    /** @var \WP_HTTP_Response $result */
                    $result->set_status($httpStatusCode);
                    $result->set_data([
                        'code' => 'tamara_checkout_session_error',
                        'message' => '<p>'.implode('<br />', $errors).'</p>',
                        'data' => [
                            'order_id' => $orderId,
                            'status' => $httpStatusCode,
                        ],
                        'additional_errors' => $errors,
                    ]);

                    return $result;
                }, 100, 3);
            } else {
                foreach ($errors as $error) {
                    wc_add_notice($error, 'error');
                }
            }
        }

        // If this is the failed process, return false instead of ['result' => 'success']
        return false;
    }

    /**
     * Filling all needed data for a Tamara Order used for Pay By Later
     *
     * @param WC_Order $wcOrder
     * @param string $paymentType | null
     *
     * @return Order
     * @throws Exception
     */
    public function populateTamaraOrder($wcOrder, $paymentType = null)
    {
        if (empty($paymentType)) {
            throw new Exception('Error! No Payment Type specified');
        }

        $order = new Order();

        $order->setOrderReferenceId($wcOrder->get_id());
        $order->setLocale(get_locale());
        $order->setCurrency($wcOrder->get_currency());
        $order->setTotalAmount(new Money(MoneyHelper::formatNumber($wcOrder->get_total()), $order->getCurrency()));
        $order->setCountryCode(!empty($wcOrder->get_billing_country()) ? $wcOrder->get_billing_country() : $this->getDefaultBillingCountryCode());
        $order->setPaymentType($paymentType);
        $order->setPlatform(sprintf('WordPress %s, WooCommerce %s, Tamara Checkout %s', $GLOBALS['wp_version'], $GLOBALS['woocommerce']->version, TamaraCheckout::getInstance()->version));
        $order->setDescription(__('Use Tamara Gateway with WooCommerce', $this->textDomain));
        $order->setTaxAmount(new Money(MoneyHelper::formatNumber($wcOrder->get_total_tax()), $order->getCurrency()));
        $order->setShippingAmount(new Money(MoneyHelper::formatNumber($wcOrder->get_shipping_total()),
            $order->getCurrency()));
        $order->setDiscount(new Discount('Total Discount',
            new Money(MoneyHelper::formatNumber($wcOrder->get_discount_total()), $order->getCurrency())));

        $order->setMerchantUrl($this->populateTamaraMerchantUrl($wcOrder));
        $order->setBillingAddress($this->populateTamaraBillingAddress($wcOrder));
        $order->setShippingAddress($this->populateTamaraShippingAddress($wcOrder));
        $order->setConsumer($this->populateTamaraConsumer($wcOrder));

        $order->setItems($this->populateTamaraOrderItems($wcOrder));

        return $order;
    }

    /**
     * @inheritDoc
     */
    public function process_payment($orderId)
    {
        return $this->tamaraCheckoutSession($orderId);
    }

    /**
     * Set Tamara Items Detail
     *
     * @param WC_Order $wcOrder
     *
     * @return OrderItemCollection
     */
    public function populateTamaraOrderItems($wcOrder)
    {
        $wcOrderItems = $wcOrder->get_items();
        $orderItemCollection = new OrderItemCollection();

        foreach ($wcOrderItems as $itemId => $wcOrderItem) {
            /** @var WC_Product $wcOrderItemProduct */
            $wcOrderItemProduct = $wcOrderItem->get_product();

            $wcOrderItemName = strip_tags($wcOrderItem->get_name());
            $wcOrderItemQuantity = $wcOrderItem->get_quantity();
            $wcOrderItemSku = $wcOrderItemProduct->get_sku() ?: 'N/A';
            $wcOrderItemTotalTax = $wcOrderItem->get_total_tax();
            $wcOrderItemTotal = $wcOrderItem->get_total() + $wcOrderItemTotalTax;
            $wcOrderItemCategories = strip_tags(wc_get_product_category_list($wcOrderItemProduct->get_id())) ?: 'N/A';
            $wcOrderItemRegularPrice = $wcOrderItemProduct->get_regular_price();
            $wcOrderItemSalePrice = $wcOrderItemProduct->get_sale_price();
            $itemPrice = $wcOrderItemSalePrice ? $wcOrderItemSalePrice : $wcOrderItemRegularPrice;
            $wcOrderItemDiscountAmount = $itemPrice * $wcOrderItemQuantity - ($wcOrderItemTotal - $wcOrderItemTotalTax);

            $orderItem = new OrderItem();

            $orderItem->setName($wcOrderItemName);
            $orderItem->setQuantity($wcOrderItemQuantity);
            $orderItem->setUnitPrice(new Money(MoneyHelper::formatNumber($itemPrice), $wcOrder->get_currency()));
            $orderItem->setType($wcOrderItemCategories);
            $orderItem->setSku($wcOrderItemSku);
            $orderItem->setTotalAmount(new Money(MoneyHelper::formatNumber($wcOrderItemTotal),
                $wcOrder->get_currency()));
            $orderItem->setTaxAmount(new Money(MoneyHelper::formatNumber($wcOrderItemTotalTax),
                $wcOrder->get_currency()));
            $orderItem->setDiscountAmount(new Money(MoneyHelper::formatNumber($wcOrderItemDiscountAmount),
                $wcOrder->get_currency()));
            $orderItem->setReferenceId($itemId);
            $orderItem->setImageUrl(wp_get_attachment_url($wcOrderItemProduct->get_image_id()));

            $orderItemCollection->append($orderItem);
        }

        return $orderItemCollection;
    }

    /**
     * @param WC_Order_Refund $wcOrderRefund
     *
     * @return OrderItemCollection
     */
    public function populateTamaraRefundOrderItems($wcOrderRefund)
    {
        $wcOrderItems = $wcOrderRefund->get_items();
        $orderItemCollection = new OrderItemCollection();

        foreach ($wcOrderItems as $itemId => $wcOrderItem) {
            /** @var WC_Product $wcOrderItemProduct */
            $wcOrderItemProduct = $wcOrderItem->get_product();

            $wcOrderItemName = strip_tags($wcOrderItem->get_name());
            $wcOrderItemQuantity = abs($wcOrderItem->get_quantity());
            $wcOrderItemSku = $wcOrderItemProduct->get_sku() ?: 'N/A';
            $wcOrderItemTotalTax = abs($wcOrderItem->get_total_tax());
            $wcOrderItemTotal = abs($wcOrderItem->get_total()) + $wcOrderItemTotalTax;
            $wcOrderItemCategories = strip_tags(wc_get_product_category_list($wcOrderItemProduct->get_id())) ?: 'N/A';
            $wcOrderItemRegularPrice = $wcOrderItemProduct->get_regular_price();
            $wcOrderItemSalePrice = $wcOrderItemProduct->get_sale_price();
            $itemPrice = $wcOrderItemSalePrice ? $wcOrderItemSalePrice : $wcOrderItemRegularPrice;
            $wcOrderItemDiscountAmount = $itemPrice * $wcOrderItemQuantity - ($wcOrderItemTotal - $wcOrderItemTotalTax);

            $orderItem = new OrderItem();

            $orderItem->setName($wcOrderItemName);
            $orderItem->setQuantity($wcOrderItemQuantity);
            $orderItem->setUnitPrice(new Money(MoneyHelper::formatNumber($itemPrice), $wcOrderRefund->get_currency()));
            $orderItem->setType($wcOrderItemCategories);
            $orderItem->setSku($wcOrderItemSku);
            $orderItem->setTotalAmount(new Money(MoneyHelper::formatNumber($wcOrderItemTotal), $wcOrderRefund->get_currency()));
            $orderItem->setTaxAmount(new Money(MoneyHelper::formatNumber($wcOrderItemTotalTax), $wcOrderRefund->get_currency()));
            $orderItem->setDiscountAmount(new Money(MoneyHelper::formatNumber($wcOrderItemDiscountAmount), $wcOrderRefund->get_currency()));
            $orderItem->setReferenceId($itemId);
            $orderItem->setImageUrl(wp_get_attachment_url($wcOrderItemProduct->get_image_id()));

            $orderItemCollection->append($orderItem);
        }

        return $orderItemCollection;
    }

    /**
     * Fill up date for Tamara Merchant Url for Tamara to redirect on corresponding order status
     *
     * @param WC_Order $wcOrder
     *
     * @return MerchantUrl
     */
    public function populateTamaraMerchantUrl($wcOrder)
    {
        $merchantUrl = new MerchantUrl();

        $orderId = $wcOrder->get_id();

        $tamaraSuccessUrl = $this->getTamaraSuccessUrl([
            'orderId' => $orderId
        ]);
        $tamaraCancelUrl = $this->getTamaraCancelUrl([
            'orderId' => $orderId
        ]);
        $tamaraFailureUrl = $this->getTamaraFailureUrl([
            'orderId' => $orderId
        ]);

        $merchantUrl->setSuccessUrl($tamaraSuccessUrl);
        $merchantUrl->setFailureUrl($tamaraFailureUrl);
        $merchantUrl->setCancelUrl($tamaraCancelUrl);
        $merchantUrl->setNotificationUrl($this->getTamaraIpnUrl());

        return $merchantUrl;
    }

    /**
     * @param $wcAddress
     *
     * @return Address
     */
    public function populateTamaraAddress($wcAddress)
    {
        $firstName = !empty($wcAddress['first_name']) ? $wcAddress['first_name'] : 'N/A';
        $lastName = !empty($wcAddress['last_name']) ? $wcAddress['first_name'] : 'N/A';
        $address1 = !empty($wcAddress['address_1']) ? $wcAddress['first_name'] : 'N/A';
        $address2 = !empty($wcAddress['address_2']) ? $wcAddress['first_name'] : 'N/A';
        $city = !empty($wcAddress['city']) ? $wcAddress['first_name'] : 'N/A';
        $state = !empty($wcAddress['state']) ? $wcAddress['first_name'] : 'N/A';
        $phone = !empty($wcAddress['phone']) ? $wcAddress['phone'] : null;
        $country = !empty($wcAddress['country']) ? $wcAddress['country'] : $this->getDefaultBillingCountryCode();

        $tamaraAddress = new Address();
        $tamaraAddress->setFirstName((string)$firstName);
        $tamaraAddress->setLastName((string)$lastName);
        $tamaraAddress->setLine1((string)$address1);
        $tamaraAddress->setLine2((string)$address2);
        $tamaraAddress->setCity((string)$city);
        $tamaraAddress->setRegion((string)$state);
        $tamaraAddress->setPhoneNumber((string)$phone);
        $tamaraAddress->setCountryCode((string)$country);

        return $tamaraAddress;
    }

    /**
     * Set Tamara Order Billing Addresses
     *
     * @param WC_Order $wcOrder
     *
     * @return Address
     */
    public function populateTamaraBillingAddress($wcOrder)
    {
        $wcBillingAddress = $wcOrder->get_address('billing');
        return $this->populateTamaraAddress($wcBillingAddress);
    }

    /**
     * Set Order Shipping Addresses
     *
     * @param WC_Order $wcOrder
     *
     * @return Address
     */
    public function populateTamaraShippingAddress($wcOrder)
    {
        $wcShippingAddress = $wcOrder->get_address('shipping');
        $wcBillingAddress = $wcOrder->get_address('billing');

        $wcShippingAddress['first_name'] = !empty($wcShippingAddress['first_name']) ? $wcShippingAddress['first_name'] : (!empty($wcBillingAddress['first_name']) ? $wcBillingAddress['first_name'] : null);
        $wcShippingAddress['last_name'] = !empty($wcShippingAddress['last_name']) ? $wcShippingAddress['last_name'] : (!empty($wcBillingAddress['last_name']) ? $wcBillingAddress['last_name'] : null);
        $wcShippingAddress['address_1'] = !empty($wcShippingAddress['address_1']) ? $wcShippingAddress['address_1'] : (!empty($wcBillingAddress['address_1']) ? $wcBillingAddress['address_1'] : null);
        $wcShippingAddress['address_2'] = !empty($wcShippingAddress['address_2']) ? $wcShippingAddress['address_2'] : (!empty($wcBillingAddress['address_2']) ? $wcBillingAddress['address_2'] : null);
        $wcShippingAddress['city'] = !empty($wcShippingAddress['city']) ? $wcShippingAddress['city'] : (!empty($wcBillingAddress['city']) ? $wcBillingAddress['city'] : null);
        $wcShippingAddress['state'] = !empty($wcShippingAddress['state']) ? $wcShippingAddress['state'] : (!empty($wcBillingAddress['state']) ? $wcBillingAddress['state'] : null);
        $wcShippingAddress['country'] = !empty($wcShippingAddress['country']) ? $wcShippingAddress['country'] : (!empty($wcBillingAddress['country']) ? $wcBillingAddress['country'] : null);
        $wcShippingAddress['phone'] = !empty($wcShippingAddress['phone']) ? $wcShippingAddress['phone'] : (!empty($wcBillingAddress['phone']) ? $wcBillingAddress['phone'] : null);

        return $this->populateTamaraAddress($wcShippingAddress);
    }

    /**
     * Set Tamara Consumer
     *
     * @param WC_Order $wcOrder
     *
     * @return Consumer
     */
    public function populateTamaraConsumer($wcOrder)
    {
        $wcBillingAddress = $wcOrder->get_address('billing');

        $firstName = $wcBillingAddress['first_name'] ?? 'N/A';
        $lastName = $wcBillingAddress['last_name'] ?? 'N/A';
        $email = $wcBillingAddress['email'] ?? 'notavailable@email.com';
        $phone = $wcBillingAddress['phone'] ?? 'N/A';

        $consumer = new Consumer();
        $consumer->setFirstName($firstName);
        $consumer->setLastName($lastName);
        $consumer->setEmail($email);
        $consumer->setPhoneNumber($phone);

        return $consumer;
    }

    /** @noinspection PhpFullyQualifiedNameUsageInspection */
    /**
     * Create Tamara Checkout Session Request
     *
     * @param WC_Order $wcOrder
     *
     * @param $paymentType
     *
     * @return \Tamara\Response\Checkout\CreateCheckoutResponse
     * @throws Exception
     */
    public function createTamaraCheckoutSession($wcOrder, $paymentType)
    {
        $client = $this->tamaraClient;
        $checkoutRequest = new CreateCheckoutRequest($this->populateTamaraOrder($wcOrder, $paymentType));

        try {
            return $client->createCheckout($checkoutRequest);
        } catch (Exception $exception) {
            TamaraCheckout::getInstance()->logMessage(
                sprintf(
                    "Cannot create Tamara Checkout Session.\nError message: ' %s'.\nTrace: %s",
                    $exception->getMessage(),
                    $exception->getTraceAsString()
                )
            );
            throw new Exception('Cannot create Tamara Checkout Session');
        }
    }

    /**
     * Get Tamara Payment Types and its min/max amount
     *
     * @return mixed
     *
     */
    public function getPaymentTypes()
    {
        $countryCode = !is_admin() && !empty($this->getCurrencyToCountryMapping()[get_woocommerce_currency()]) ? $this->getCurrencyToCountryMapping()[get_woocommerce_currency()] : WC()->countries->get_base_country();
        $this->paymentTypes = $this->settings['country_payment_types'][$countryCode] ?? [];

        return $this->paymentTypes;
    }

    /**
     * Init admin form fields
     */
    public function initFormFields()
    {
        $this->form_fields = [
            'enabled' => [
                'title' => __('Enable/Disable', $this->textDomain),
                'label' => __('Enable Tamara Gateway', $this->textDomain),
                'type' => 'checkbox',
            ],
            'payment_types' => [
                'title' => __('Payment Types', $this->textDomain),
                'type' => 'title',
                'description' => __('Select the Payment Types that you wish to use with Tamara', $this->textDomain),
            ],
            'pay_by_later_enabled' => [
                'title' => __('Pay By Later', $this->textDomain),
                'label' => __('Enable Pay By Later', $this->textDomain),
                'default' => 'yes',
                'type' => 'checkbox',
                'description' => '',
            ],
            'pay_by_later_title' => [
                'title' => __('Pay By Later English Title', $this->textDomain),
                'description' => __('Enter the English title to display for Tamara Pay By Later method on checkout page.',
                    $this->textDomain),
                'default' => static::PAYMENT_TYPE_PAY_BY_LATER_DEFAULT_TITLE,
            ],
            'pay_by_later_title_ar' => [
                'title' => __('Pay By Later Arabic Title', $this->textDomain),
                'description' => __('Enter the Arabic title to display for Tamara Pay By Later method on checkout page.',
                    $this->textDomain),
                'default' => static::PAYMENT_TYPE_PAY_BY_LATER_DEFAULT_TITLE_AR,
            ],
            'pay_by_instalments_enabled' => [
                'title' => __('Pay By Instalments', $this->textDomain),
                'label' => __('Enable Pay By Instalments', $this->textDomain),
                'default' => 'yes',
                'type' => 'checkbox',
                'description' => '',
            ],
            'pay_by_instalments_title' => [
                'title' => __('Pay By Instalments English Title', $this->textDomain),
                'description' => __('Enter the English title to display for Tamara Pay By Instalments method on checkout page.',
                    $this->textDomain),
                'default' => static::PAYMENT_TYPE_PAY_BY_INSTALMENTS_DEFAULT_TITLE,
            ],
            'pay_by_instalments_title_ar' => [
                'title' => __('Pay By Instalments Arabic Title', $this->textDomain),
                'description' => __('Enter the Arabic title to display for Tamara Pay By Instalments method on checkout page.',
                    $this->textDomain),
                'default' => static::PAYMENT_TYPE_PAY_BY_INSTALMENTS_DEFAULT_TITLE_AR,
            ],
            'country_payment_types' => [
                'title' => __('Country Payment Types Limit Amounts', $this->textDomain),
                'type' => 'title',
                'description' => $this->renderTamaraCountryPaymentTypesHtml(),
            ],
            'logo_image_url' => [
                'title' => __('Logo Image URL', $this->textDomain),
                'description' => __('URL of image icon to appear on payment method selecting. The recommended size must be no larger than 230x60, image format.',
                    $this->textDomain),
            ],
            'tamara_payment_cancel' => [
                'title' => __('Order status for payment cancelled from Tamara', $this->textDomain),
                'type' => 'select',
                'default' => 'wc-tamara-p-canceled',
                'options' => wc_get_order_statuses(),
                'description' => __('Map status for order when the payment is cancelled from Tamara during checkout.', $this->textDomain),
            ],
            'tamara_payment_failure' => [
                'title' => __('Order status for payment failed from Tamara', $this->textDomain),
                'type' => 'select',
                'default' => 'wc-tamara-p-failed',
                'options' => wc_get_order_statuses(),
                'description' => __('Map status for order when the payment is failed from Tamara during checkout.', $this->textDomain),
            ],
            'tamara_authorise_done' => [
                'title' => __('Order status for Authorise success from Tamara', $this->textDomain),
                'type' => 'select',
                'default' => 'wc-tamara-a-done',
                'options' => wc_get_order_statuses(),
                'description' => __('Map status for order when the payment is authorised successfully from Tamara.', $this->textDomain),
            ],
            'tamara_authorise_failure' => [
                'title' => __('Order status for Authorise failed from Tamara', $this->textDomain),
                'type' => 'select',
                'default' => 'wc-tamara-a-failed',
                'options' => wc_get_order_statuses(),
                'description' => __('Map status for order when the payment is failed in authorising from Tamara.', $this->textDomain),
            ],
            'tamara_cancel_order' => [
                'title' => __('Order status for cancelling an order and cancel the payment on Tamara', $this->textDomain),
                'type' => 'select',
                'options' => wc_get_order_statuses()['wc-cancelled'],
                'description' => __('The default "Cancelled" status of WooCommerce is used to cancel the order as well as trigger the Cancel payment to Tamara (before it is captured).',
                    $this->textDomain),
            ],
            'tamara_payment_capture' => [
                'title' => __('Order status to proceed Tamara Capture Payment', $this->textDomain),
                'type' => 'select',
                'default' => 'wc-completed',
                'options' => wc_get_order_statuses(),
                'description' => __('Map status for order to proceed the Capture payment process and send API to Tamara.', $this->textDomain),
            ],
            'tamara_capture_failure' => [
                'title' => __('Order status for Capture failed from Tamara', $this->textDomain),
                'type' => 'select',
                'default' => 'wc-tamara-c-failed',
                'options' => wc_get_order_statuses(),
                'description' => __('Map status for order when the Capture process is failed.', $this->textDomain),
            ],
            'tamara_order_cancel' => [
                'title' => __('Order status for cancelling the order from Tamara through Webhook', $this->textDomain),
                'type' => 'select',
                'default' => 'wc-tamara-o-canceled',
                'options' => wc_get_order_statuses(),
                'description' => __('Map status for order when it is cancelled from Tamara (Order Expired, Order Declined...) through Webhook.',
                    $this->textDomain),
            ],
            'environment' => [
                'title' => __('Tamara Working Mode', $this->textDomain),
                'label' => __('Choose Tamara Working Mode', $this->textDomain),
                'type' => 'select',
                'default' => static::ENVIRONMENT_LIVE_MODE,
                'options' => [
                    static::ENVIRONMENT_LIVE_MODE => 'Live Mode',
                    static::ENVIRONMENT_SANDBOX_MODE => 'Sandbox Mode',
                ],
                'description' => __('This setting specifies whether you will process live transactions, or whether you will process simulated transactions using the Tamara Sandbox.',
                    $this->textDomain),
            ],
            'live_api_url' => [
                'title' => __('Live API URL', $this->textDomain),
                'type' => 'text',
                'description' => __('The Tamara Live API URL (https://api.tamara.co).', $this->textDomain),
                'default' => 'https://api.tamara.co',
            ],
            'live_api_token' => [
                'title' => __('Live Merchant Token', $this->textDomain),
                'type' => 'textarea',
                'description' => __('Get your API credentials from Tamara.', $this->textDomain),
            ],
            'live_notification_token' => [
                'title' => __('Live Notification Token', $this->textDomain),
                'type' => 'text',
                'description' => __('Get your API credentials from Tamara.', $this->textDomain),
                'default' => '',
            ],
            'sandbox_api_url' => [
                'title' => __('Sandbox API URL', $this->textDomain),
                'type' => 'text',
                'description' => __('The Tamara Sandbox API URL (https://api-sandbox.tamara.co).', $this->textDomain),
                'default' => 'https://api-sandbox.tamara.co',
            ],
            'sandbox_api_token' => [
                'title' => __('Sandbox Merchant Token', $this->textDomain),
                'type' => 'textarea',
                'description' => __('Get your API Token credentials for testing from Tamara.', $this->textDomain),
            ],
            'sandbox_notification_token' => [
                'title' => __('Sandbox Notification Token', $this->textDomain),
                'type' => 'text',
                'description' => __('Get your API Notification Token credentials for testing from Tamara.',
                    $this->textDomain),
            ],
            'crobjob_enabled' => [
                'title' => __('Enable Cron Job', $this->textDomain),
                'type' => 'checkbox',
                'description' => __('If you enable this setting, Tamara will use a cron-job to find all completed orders that has not been captured within 3 days and force them to be captured.',
                    $this->textDomain),
            ],
            'allowed_shipping_country_codes' => [
                'title' => __('Allowed Shipping Country Codes', $this->textDomain),
                'type' => 'text',
                'description' => sprintf(__('Add allowed shipping country codes for your site to use with Tamara (These codes, %s, are separated by commas e.g. AE, SA)',
                    $this->textDomain), '<a target="_blank" href="https://wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166-1 alpha-2 format</a>'),
                'default' => 'SA,AE',
            ],
            'default_billing_country_code' => [
                'title' => __('Default Billing Country Code', $this->textDomain),
                'type' => 'text',
                'description' => sprintf(__('Add a default billing country code (%s, e.g. AE, SA) to use with Tamara if none is set during the checkout progress.',
                    $this->textDomain), '<a target="_blank" href="https://wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166-1 alpha-2 format</a>'),
                'default' => 'SA',
            ],
            'custom_log_message_enabled' => [
                'title' => __('Enable Tamara Custom Log Message', $this->textDomain),
                'type' => 'checkbox',
                'description' => __('If you enable this setting, all the message logs will be written and saved to the Tamara custom log file and shown in the text area below.',
                    $this->textDomain),
            ],
            'custom_log_message' => [
                'title' => __('Tamara Custom Log Message', $this->textDomain),
                'type' => 'text',
                'description' =>
                    '<textarea id="tamara-custom-log" class="tamara-custom-log code" rows="15" readonly="readonly">'
                    .(file_exists(TamaraCheckout::getInstance()->logMessageFilePath()) ? file_get_contents(TamaraCheckout::getInstance()->logMessageFilePath()) : __('[No message logs found]', $this->textDomain)).
                    '</textarea>',
            ],
            'popup_widget_position' => [
                'title' => __('Popup Widget Position', $this->textDomain),
                'type' => 'select',
                'options' => static::TAMARA_POPUP_WIDGET_POSITIONS,
                'description' => __('Choose a position where you want to display the Tamara Payment Popup Widget. Or, you can use shortcode [tamara_show_popup] to show it.',
                    $this->textDomain),
                'default' => 'woocommerce_single_product_summary',
            ],
            'webhook_enabled' => [
                'title' => __('Enable Webhook', $this->textDomain),
                'type' => 'checkbox',
                'description' => __('If you enable this setting, Tamara will use the webhook to handle the Order Declined and Order Expired.',
                    $this->textDomain),
            ],
            'webhook_id' => [
                'title' => __('Webhook ID', $this->textDomain),
                'type' => 'title',
                'description' => $this->getWebhookId(),
            ],
            'beautify_merchant_urls' => [
                'title' => __('Enable Beautiful Merchant Urls', $this->textDomain),
                'type' => 'checkbox',
                'description' => __('If you enable this setting, the urls for handling webhook will be beautified. After enabling this, please go to "Dashboard => Settings => Permalinks" and click on "Save Changes" to take effect.',
                    $this->textDomain),
            ],
            'merchant_urls' => [
                'title' => __('Merchant URLs', $this->textDomain),
                'type' => 'title',
                'description' =>
                    '<p>'.__('- Tamara Success URL: ', $this->textDomain).($this->getTamaraSuccessUrl() ?: 'N/A').'</p>
                    <p>'.__('- Tamara Cancel URL: ', $this->textDomain).($this->getTamaraCancelUrl() ?: 'N/A').'</p>
                    <p>'.__('- Tamara Failure URL: ', $this->textDomain).($this->getTamaraFailureUrl() ?: 'N/A').'</p>
                    <p>'.__('- Tamara Notification URL: ', $this->textDomain).($this->getTamaraIpnUrl() ?: 'N/A').'</p>
                    <p>'.__('- Tamara Webhook URL: ', $this->textDomain).($this->getTamaraWebhookUrl() ?: 'N/A').'</p>',
            ],
            'debug_info' => [
                'title' => __('Debug Info', $this->textDomain),
                'type' => 'title',
                'description' =>
                    '<p>'.sprintf('PHP Version: %s', PHP_VERSION).'</p>'
                    .'<p>'.sprintf('PHP loaded extension: %s', implode( ', ', get_loaded_extensions())).'</p>'
            ],
        ];

        if (!TamaraCheckout::getInstance()->isCustomLogMessageEnabled()) {
            unset($this->form_fields['custom_log_message']);
            if (file_exists(TamaraCheckout::getInstance()->logMessageFilePath())) {
                wp_delete_file(TamaraCheckout::getInstance()->logMessageFilePath());
            }
        }
    }

    /**
     * Get Shipping Information
     */
    public function getShippingInfo()
    {
        $shippedAt = new DateTimeImmutable();
        $shippingCompany = 'N/A';
        $trackingNumber = 'N/A';
        $trackingUrl = 'N/A';

        return new ShippingInfo($shippedAt, $shippingCompany, $trackingNumber, $trackingUrl);
    }

    /**
     * Tamara Capture Payment
     *
     * @param int $wcOrderId
     * @param WC_Order $statusFrom
     * @param WC_Order $statusTo
     * @param WC_Order $wcOrder
     */
    public function tamaraCapturePayment($wcOrderId, $statusFrom, $statusTo, $wcOrder)
    {
        $payment_method = $wcOrder->get_payment_method();

        // Remove wc- prefix
        $tamaraCapturePaymentStatus = substr($this->tamaraStatus['payment_capture'],3);

        if ($tamaraCapturePaymentStatus === $statusTo && $this->id === $payment_method) {
            $tamaraOrderId = get_post_meta($wcOrderId, 'tamara_order_id', true);
            $captureId = get_post_meta($wcOrderId, 'capture_id', true);
            if (empty($captureId) && $tamaraOrderId) {
                $this->captureWcOrder($wcOrderId);
            } elseif ($captureId) {
                $orderNote = 'Re-capture for this order happened';
                $newOrderStatus = $this->tamaraStatus['payment_capture'] ?? $statusFrom;
                TamaraCheckout::getInstance()->updateOrderStatusAndAddOrderNote($wcOrder, $orderNote, $newOrderStatus, '');
            }
        }
    }

    /**
     * Tamara Capture Payment Method
     *
     * @param int $wcOrderId
     */
    public function captureWcOrder($wcOrderId)
    {
        $wcOrder = wc_get_order($wcOrderId);
        $wcOrderTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_total()), $wcOrder->get_currency());
        $wcShippingTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_shipping_total()), $wcOrder->get_currency());
        $wcTaxTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_total_tax()), $wcOrder->get_currency());
        $wcDiscountTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_discount_total()), $wcOrder->get_currency());
        $wcOrderShippingInfo = $this->getShippingInfo();
        $wcOrderItems = $this->populateTamaraOrderItems($wcOrder);
        $tamaraOrderId = get_post_meta($wcOrderId, 'tamara_order_id', true);

        try {
            $captureResponse = $this->tamaraClient->capture(
                new CaptureRequest(
                    new Capture(
                        $tamaraOrderId,
                        $wcOrderTotal,
                        $wcShippingTotal,
                        $wcTaxTotal,
                        $wcDiscountTotal,
                        $wcOrderItems,
                        $wcOrderShippingInfo
                    )
                )
            );
            TamaraCheckout::getInstance()->logMessage(sprintf("Tamara Capture Response Data: %s", print_r($captureResponse, true)));
        } catch (Exception $tamaraCaptureException) {
            TamaraCheckout::getInstance()->logMessage(
                sprintf(
                    "Tamara Service timeout or disconnected.\nError message: ' %s'.\nTrace: %s",
                    $tamaraCaptureException->getMessage(),
                    $tamaraCaptureException->getTraceAsString()
                )
            );
        }

        if (!empty($captureResponse) && $captureResponse instanceof CaptureResponse && $captureResponse->isSuccess()) {
            $captureId = $captureResponse->getCaptureId();
            update_post_meta($wcOrderId, 'capture_id', $captureId);
            $orderNote = sprintf(__('The Payment has been captured successfully - Capture ID: %s'), $captureId);
            $newOrderStatus = $this->tamaraStatus['payment_capture'];
            TamaraCheckout::getInstance()->updateOrderStatusAndAddOrderNote($wcOrder, $orderNote, $newOrderStatus, '');
        } else {
            $errorMessage = null;
            if (isset($tamaraCaptureException) && $tamaraCaptureException instanceof Exception) {
                $errorMessage = $tamaraCaptureException->getMessage();
                TamaraCheckout::getInstance()->logMessage($errorMessage);
            } elseif (isset($captureResponse)) {
                $errorMessage = $captureResponse->getMessage();
                TamaraCheckout::getInstance()->logMessage($errorMessage);
            }
            $orderNote = sprintf(__('The Payment can not be captured, error message: %s'), $errorMessage);
            $newOrderStatus = $this->tamaraStatus['capture_failed'];
            TamaraCheckout::getInstance()->updateOrderStatusAndAddOrderNote($wcOrder, $orderNote, $newOrderStatus, '');
        }
    }

    /**
     * Handle Tamara Cancel Order before it's captured
     *
     * @param int $wcOrderId
     * @param WC_Order $statusFrom
     * @param WC_Order $statusTo
     * @param WC_Order $wcOrder
     */
    public function tamaraCancelOrder($wcOrderId, $statusFrom, $statusTo, $wcOrder)
    {
        $payment_method = $wcOrder->get_payment_method();

        $wcOrderTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_total()), $wcOrder->get_currency());
        $wcShippingTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_shipping_total()),
            $wcOrder->get_currency());
        $wcTaxTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_total_tax()), $wcOrder->get_currency());
        $wcDiscountTotal = new Money(MoneyHelper::formatNumber($wcOrder->get_discount_total()),
            $wcOrder->get_currency());
        $wcOrderItems = $this->populateTamaraOrderItems($wcOrder);

        if ('cancelled' === $statusTo && $this->id === $payment_method) {
            $tamaraOrderId = get_post_meta($wcOrderId, 'tamara_order_id', true);
            $captureId = get_post_meta($wcOrderId, 'capture_id', true);
            if (empty($captureId) && $tamaraOrderId) {
                try {
                    $cancelResponse = $this->tamaraClient->cancelOrder(new CancelOrderRequest($tamaraOrderId,
                        $wcOrderTotal,
                        $wcOrderItems, $wcShippingTotal, $wcTaxTotal, $wcDiscountTotal));
                    TamaraCheckout::getInstance()->logMessage(sprintf("Tamara Cancel Response Data: %s", print_r($cancelResponse, true)));
                } catch (Exception $tamaraCancelException) {
                    TamaraCheckout::getInstance()->logMessage(sprintf("Tamara Service timeout or disconnected.\nError message: '%s'.\nTrace: %s",
                        $tamaraCancelException->getMessage(), $tamaraCancelException->getTraceAsString()));
                }

                if (isset($cancelResponse) && $cancelResponse->isSuccess()) {
                    $cancelId = $cancelResponse->getCancelId();
                    $wcOrder->add_order_note(__('The order has been cancelled successfully', $this->textDomain));
                    update_post_meta($wcOrderId, 'tamara_cancel_id', $cancelId);
                } else {
                    $errorMessage = null;
                    if (isset($tamaraCancelException) && $tamaraCancelException instanceof Exception) {
                        $errorMessage = $tamaraCancelException->getMessage();
                        TamaraCheckout::getInstance()->logMessage($errorMessage);
                    } elseif (isset($cancelResponse)) {
                        $errorMessage = $cancelResponse->getMessage();
                        TamaraCheckout::getInstance()->logMessage($errorMessage);
                    }
                    $orderNote = ('The order can not be cancelled - '.$errorMessage);
                    $newOrderStatus = $statusFrom;
                    TamaraCheckout::getInstance()->updateOrderStatusAndAddOrderNote($wcOrder, $orderNote, $newOrderStatus, '');
                }

            } elseif ($captureId) {
                $orderNote = ('The order can not be cancelled as it was captured. Please try "Refund" function instead.');
                $newOrderStatus = $statusFrom;
                TamaraCheckout::getInstance()->updateOrderStatusAndAddOrderNote($wcOrder, $orderNote, $newOrderStatus, '');
            }
        }
    }

    /**
     * Register Tamara Webhook
     *
     * @return string|null
     */
    public function registerTamaraWebhook()
    {
        try {
            $webhookUrl = $this->getTamaraWebhookUrl();
            $webhookRequest = new RegisterWebhookRequest(
                $webhookUrl, static::REGISTERED_WEBHOOKS
            );

            $response = $this->tamaraClient->registerWebhook($webhookRequest);
            if ($response->isSuccess()) {
                $webhookId = $response->getWebhookId();
                TamaraCheckout::getInstance()->logMessage(sprintf("Webhook Register Data: %s", print_r($response, true)));

                return $webhookId;
            }

            $firstErrorCode = $response->getErrors()[0]['error_code'] ?? '';
            if ('webhook_already_registered' === $firstErrorCode) {
                $registeredWebhookId = $response->getErrors()[0]['data']['webhook_id'] ?? null;
                TamaraCheckout::getInstance()->logMessage(sprintf("Webhook Already Registered"));

                return $registeredWebhookId;
            }
        } catch (Exception $exception) {
            TamaraCheckout::getInstance()->logMessage(
                sprintf(
                    "Webhook Register Failed.\nError message: ' %s'.\nTrace: %s",
                    $exception->getMessage(),
                    $exception->getTraceAsString()
                )
            );
        }

        return null;
    }

    /**
     * Delete Tamara Webhook
     *
     * @param $webhookId string
     *
     * @return bool
     */
    public function deleteTamaraWebhook($webhookId)
    {
        try {
            $request = new RemoveWebhookRequest($webhookId);
            $response = $this->tamaraClient->removeWebhook($request);

            if ($response->isSuccess()) {
                TamaraCheckout::getInstance()->logMessage(sprintf("Webhook Delete Data: %s", print_r($response, true)));

                return true;
            }
        } catch (Exception $exception) {
            TamaraCheckout::getInstance()->logMessage(
                sprintf(
                    "Webhook Delete Failed.\nError message: ' %s'.\nTrace: %s",
                    $exception->getMessage(),
                    $exception->getTraceAsString()
                )
            );
        }

        return false;
    }

    /**
     * Check if all the required field values have been input or not
     * and verify api params
     */
    public function validateRequiredFields()
    {
        $workingMode = $this->get_option('environment');
        $apiUrl = (static::ENVIRONMENT_LIVE_MODE === $workingMode) ? $this->get_option('live_api_url') : $this->get_option('sandbox_api_url');
        $apiToken = (static::ENVIRONMENT_LIVE_MODE === $workingMode) ? $this->get_option('live_api_token') : $this->get_option('sandbox_api_token');
        $notificationToken = (static::ENVIRONMENT_LIVE_MODE === $workingMode) ? $this->get_option('live_notification_token') : $this->get_option('sandbox_notification_token');

        if (!$apiUrl || !$apiToken || !$notificationToken || !$this->verifyApiParams($apiUrl, $apiToken)) {
            $this->settings['enabled'] = 'no';
            $this->raiseAdminError();

            return false;
        } elseif (!TamaraCheckout::getInstance()->isPayByLaterEnabled() && !TamaraCheckout::getInstance()->isPayByInstalmentsEnabled()) {
            $this->settings['enabled'] = 'no';
            $this->raiseAdminErrorOnNonSelectedPaymentTypes();
        }

        return true;
    }

    /**
     * Verify to have correct Tamara API url and token or not
     *
     * @param $apiUrl
     * @param $apiToken
     *
     * @return bool
     */
    public function verifyApiParams($apiUrl, $apiToken)
    {
        $tamaraClient = $this->buildTamaraClient($apiUrl, $apiToken);
        try {
            $countryCode = $this->getStoreBaseCountryCode();
            $paymentTypesResponse = $tamaraClient->getPaymentTypes($countryCode);
            if (!$paymentTypesResponse->isSuccess()) {
                TamaraCheckout::getInstance()->logMessage(sprintf("Tamara Verify API Params Failed: %s", print_r($paymentTypesResponse->getMessage(), true)));
            }
        } catch (Exception $exception) {
            $paymentTypesResponse = null;
            TamaraCheckout::getInstance()->logMessage(
                sprintf(
                    "Tamara Service timeout or disconnected.\nError message: ' %s'.\nTrace: %s",
                    $exception->getMessage(),
                    $exception->getTraceAsString()
                )
            );
        }

        if (!empty($paymentTypesResponse) && $paymentTypesResponse instanceof GetPaymentTypesResponse) {
            return 200 === $paymentTypesResponse->getStatusCode();
        } else {
            return false;
        }
    }

    /**
     * Raise Admin Error notice on failed API params
     */
    public function raiseAdminError()
    {
        WC_Admin_Settings::add_error(
            __('Error! Tamara Checkout params cannot be retrieved correctly, Tamara disabled. Please recheck your Tamara settings (Api url, Merchant token, Notification token...).',
                $this->textDomain)
        );
    }

    /**
     * Raise Admin Error notice on non selected payment types
     */
    public function raiseAdminErrorOnNonSelectedPaymentTypes()
    {
        WC_Admin_Settings::add_error(
            __('Error! Tamara disabled. Please choose at least one payment type to use with Tamara.',
                $this->textDomain)
        );
    }

    /**
     * Get Merchant Success Url Slug
     *
     * @param $params
     *
     * @return string
     */
    public function getTamaraSuccessUrl($params = [])
    {
        if ($this->isBeautifyMerchantUrlsEnabled()) {
            $successUrlFromTamara = add_query_arg(
                $params,
                home_url(static::PAYMENT_SUCCESS_SLUG)
            );
        } else {
            $successUrlFromTamara = add_query_arg(
                array_merge(
                    $params,
                    [
                        'pagename' => static::PAYMENT_SUCCESS_SLUG,
                    ]
                ),
                home_url()
            );
        }
        return $successUrlFromTamara;
    }

    /**
     * Get Merchant Cancel Url Slug
     *
     * @param $params
     *
     * @return string
     */
    public function getTamaraCancelUrl($params = [])
    {
        if ($this->isBeautifyMerchantUrlsEnabled()) {
            $cancelUrlFromTamara = add_query_arg(
                $params,
                home_url(static::PAYMENT_CANCEL_SLUG)
            );
        } else {
            $cancelUrlFromTamara = add_query_arg(
                array_merge(
                    $params,
                    [
                        'pagename' => static::PAYMENT_CANCEL_SLUG,
                    ]
                ),
                home_url()
            );
        }
        return $cancelUrlFromTamara;
    }

    /**
     * Get Merchant Failure Url Slug
     *
     * @param $params
     *
     * @return string
     */
    public function getTamaraFailureUrl($params = [])
    {
        if ($this->isBeautifyMerchantUrlsEnabled()) {
            $failUrlFromTamara = add_query_arg(
                $params,
                home_url(static::PAYMENT_FAIL_SLUG)
            );
        } else {
            $failUrlFromTamara = add_query_arg(
                array_merge(
                    $params,
                    [
                        'pagename' => static::PAYMENT_FAIL_SLUG,
                    ]
                ),
                home_url()
            );
        }
        return $failUrlFromTamara;
    }

    /**
     * Get Tamara Ipn Url to handle Notification
     */
    public function getTamaraIpnUrl()
    {
        if ($this->isBeautifyMerchantUrlsEnabled()) {
            return home_url(static::IPN_SLUG);
        } else {
            return add_query_arg(
                [
                    'pagename' => static::IPN_SLUG,
                ],
                home_url()
            );
        }
    }

    /**
     * Get Tamara Webhook Url Slug
     */
    public function getTamaraWebhookUrl()
    {
        if ($this->isBeautifyMerchantUrlsEnabled()) {
            return home_url(static::WEBHOOK_SLUG);
        } else {
            return add_query_arg(
                [
                    'pagename' => static::WEBHOOK_SLUG,
                ],
                home_url()
            );
        }
    }

    /**
     * We need to update settings to db options (table options)
     */
    public function updateThisSettingsToOptions()
    {
        update_option(
            $this->get_option_key(),
            apply_filters(
                'woocommerce_settings_api_sanitized_fields_'.$this->id,
                $this->settings
            ),
            'yes'
        );
    }

    /**
     * Initialize attributes that are fixed
     */
    protected function initBaseAttributes()
    {
        $this->textDomain = 'tamara';
        $this->id = TamaraCheckout::TAMARA_GATEWAY_ID;
        $this->icon = $this->get_option('logo_image_url');
        $this->has_fields = true;
        $this->order_button_text = __('Proceed to Tamara Payment', $this->textDomain);
        $this->method_title = __('Tamara Gateway', $this->textDomain);
        $this->method_description = __(static::TAMARA_GATEWAY_DEFAULT_TITLE, $this->textDomain);
        if (is_admin()) {
            $this->title = __(static::TAMARA_GATEWAY_DEFAULT_TITLE, $this->textDomain);
        } else {
            $this->title = __($this->getPaymentTypeTitleMapping()[$this->id], $this->textDomain);
        }
        $this->errorMap = $this->getErrorMap();
        $this->paymentType = static::PAYMENT_TYPE_PAY_BY_LATER;
    }

    /**
     * Initialize attributes that can be changed through admin settings
     */
    protected function initSettingAttributes()
    {
        // Ensure the environment is set before calling isLiveMode()
        $this->environment = $this->get_option('environment', static::ENVIRONMENT_LIVE_MODE);
        $this->initTamaraClient();

        $this->enabled = $this->get_option('enabled', 'no');
        $this->payByLaterEnabled = $this->get_option('pay_by_later_enabled', 'no');
        $this->payByInstalmentsEnabled = $this->get_option('pay_by_instalments_enabled', 'no');
        $this->customLogMessageEnabled = $this->get_option('custom_log_message_enabled', 'no');
        $this->popupWidgetPosition = $this->get_option('popup_widget_position' ,'woocommerce_single_product_summary');
        $this->webhookEnabled = $this->get_option('webhook_enabled', 'no');
        $this->beautifyMerchantUrlsEnabled = $this->get_option('beautify_merchant_urls', 'no');
        $this->minLimit = $this->get_option('min_limit');
        $this->maxLimit = $this->get_option('max_limit');
        $this->instalmentsMinLimit = $this->get_option('instalments_min_limit');
        $this->instalmentsMaxLimit = $this->get_option('instalments_max_limit');
        $this->webhookId = $this->get_option('webhook_id');

        $this->initTamaraStatus();
    }

    /**
     * Initialize SDK client instance for Tamara actions
     */
    protected function initTamaraClient()
    {
        if ($this->isLiveMode()) {
            $this->apiUrl = $this->get_option('live_api_url', null);
            $this->apiToken = $this->get_option('live_api_token', null);
            $this->notificationToken = $this->get_option('live_notification_token', null);
        } else {
            $this->apiUrl = $this->get_option('sandbox_api_url', null);
            $this->apiToken = $this->get_option('sandbox_api_token', null);
            $this->notificationToken = $this->get_option('sandbox_notification_token', null);
        }

        $this->tamaraClient = $this->buildTamaraClient($this->apiUrl, $this->apiToken);
    }

    /**
     * Initilize Tamara Custom Order Statues for order managements
     */
    protected function initTamaraStatus()
    {
        $this->tamaraStatus['payment_cancelled'] = $this->get_option('tamara_payment_cancel', 'wc-tamara-p-canceled');
        $this->tamaraStatus['payment_failed'] = $this->get_option('tamara_payment_failure', 'wc-tamara-p-failed');
        $this->tamaraStatus['payment_capture'] = $this->get_option('tamara_payment_capture', 'wc-completed');
        $this->tamaraStatus['capture_failed'] = $this->get_option('tamara_capture_failure', 'wc-tamara-c-failed');
        $this->tamaraStatus['authorise_done'] = $this->get_option('tamara_authorise_done', 'wc-tamara-a-done');
        $this->tamaraStatus['authorise_failed'] = $this->get_option('tamara_authorise_failure', 'wc-tamara-a-failed');
        $this->tamaraStatus['order_cancelled'] = $this->get_option('tamara_order_cancel', 'wc-tamara-o-canceled');
    }

    /**
     * Common error codes when calling create checkout session API
     */
    protected function getErrorMap()
    {
        return [
            'total_amount_invalid_limit_24hrs_gmv' => __('We are not able to process your order via Tamara currently, please try again later or proceed with a different payment method.', $this->textDomain),
            'tamara_disabled' => __('Tamara is currently unavailable, please try again later.', $this->textDomain),
            'consumer_invalid_phone_number' => __('Invalid Consumer Phone Number', $this->textDomain),
            'invalid_phone_number' => __('Invalid Phone Number', $this->textDomain),
            'total_amount_invalid_currency' => __('We do not support cross currencies. Please select the correct currency for your country.', $this->textDomain),
            'billing_address_invalid_phone_number' => __('Invalid Billing Address Phone Number', $this->textDomain),
            'shipping_address_invalid_phone_number' => __('Invalid Shipping Address Phone Number', $this->textDomain),
            'total_amount_invalid_limit' => __('The grand total of order is over/under limit of Tamara', $this->textDomain),
            'currency_unsupported' => __('We do not support cross currencies. Please select the correct currency for your country', $this->textDomain),
            'Your order information is invalid' => __('Your order information is invalid', $this->textDomain),
            'Invalid country code' => __('Invalid country code', $this->textDomain),
            'We do not support your delivery country' => __('We do not support your delivery country', $this->textDomain),
            'Your phone number is invalid. Please check again' => __('Your phone number is invalid. Please check again', $this->textDomain),
            'We do not support cross currencies. Please select the correct currency for your country' => __('We do not support cross currencies. Please select the correct currency for your country', $this->textDomain),
        ];
    }

    /**
     * Get webhook id and shown in settings page
     */
    protected function getWebhookId()
    {
        if ($this->get_option('webhook_id') && !empty($this->get_option('webhook_id'))) {
            return $this->get_option('webhook_id');
        } else {
            return 'N/A';
        }
    }

    /**
     * Handle rewrite rules on enable/disable beautiful merchant urls
     */
    protected function processRewriteRules()
    {
        $this->beautifyMerchantUrlsEnabled = $this->get_option('beautify_merchant_urls');
        if ($this->isBeautifyMerchantUrlsEnabled()) {
            // Add rewrite rules
            TamaraCheckout::getInstance()->addCustomRewriteRules();
            // Flush rewrite rules
            flush_rewrite_rules(false);
        }
    }

    /**
     * Populate Pay By Later type min limit
     */
    public function populateMinLimit()
    {
        $minLimit = !empty($this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_LATER]['min_limit']) ? $this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_LATER]['min_limit'] : null;
        $this->settings['min_limit'] = $minLimit;

        return $minLimit;
    }

    /**
     * Populate Pay By Later type max limit
     */
    public function populateMaxLimit()
    {
        $maxLimit = !empty($this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_LATER]['max_limit']) ? $this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_LATER]['max_limit'] : null;
        $this->settings['max_limit'] = $maxLimit;

        return $maxLimit;
    }

    /**
     * Populate Pay By Instalments type min limit
     */
    public function populateInstalmentsMinLimit()
    {
        $instalmentMinLimit = !empty($this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_INSTALMENTS]['min_limit']) ? $this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_INSTALMENTS]['min_limit'] : null;
        $this->settings['instalments_min_limit'] = $instalmentMinLimit;

        return $instalmentMinLimit;
    }

    /**
     * Populate Pay By Instalments type max limit
     */
    public function populateInstalmentsMaxLimit()
    {
        $instalmentMaxLimit = !empty($this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_INSTALMENTS]['max_limit']) ? $this->getPaymentTypes()[static::PAYMENT_TYPE_PAY_BY_INSTALMENTS]['max_limit'] : null;
        $this->settings['instalments_max_limit'] = $instalmentMaxLimit;

        return $instalmentMaxLimit;
    }

    /**
     * Handle Tamara Webhook to register/delete Webhook Id
     */
    protected function populateTamaraWebhook()
    {
        $webhookId = $this->webhookId ?? '';
        $this->webhookEnabled = $this->get_option('webhook_enabled');
        if ($this->isWebhookEnabled()) {
            if (empty($webhookId) || !is_string($webhookId)) {
                return $this->registerTamaraWebhook();
            } elseif ($webhookId) {
                return $webhookId;
            }
        } else {
            $this->deleteTamaraWebhook($webhookId);
            return null;
        }

        return null;
    }

    /**
     * @param $cartTotal
     *
     * @return bool
     */
    protected function isCartTotalValid($cartTotal)
    {
        // Force pull country payment types from remote api
        $countryPaymentTypes = $this->getCountryPaymentTypes();
        $paymentTypes = $this->getPaymentTypes();

        return ($this->populateMinLimit() <= $cartTotal && $this->populateMaxLimit() >= $cartTotal);
    }

    /**
     * @param string $apiUrl
     * @param string $apiToken
     *
     * @return Client
     */
    protected function buildTamaraClient($apiUrl, $apiToken)
    {
        $apiUrl = $this->removeTrailingSlashes($apiUrl);
        $requestTimeout = 0;
        $logger = null;
        $transport = new NyholmHttpAdapter($requestTimeout, $logger);

        return Client::create(Configuration::create($apiUrl, $apiToken, $requestTimeout, $logger, $transport));
    }

    /**
     * Render HTML to show in Country Payments Description
     *
     * @return string
     */
    protected function renderTamaraCountryPaymentTypesHtml()
    {
        $htmlString = '';
        $currenyByCountryCode = array_flip($this->getCurrencyToCountryMapping());
        $countryPaymentTypes = isset($this->settings['country_payment_types']) ? $this->settings['country_payment_types'] : [];
        if (!empty($countryPaymentTypes)) {
            foreach ($countryPaymentTypes as $countryCode => $countryPaymentType) {
                $htmlString .= '
                <div class="tamara_country_payment_types">
                    <h4>'.__(WC()->countries->countries[$countryCode], $this->textDomain).'</h4>
                    <div class="tamara_country_payment_types_items">
                        <h4>'.__('Pay By Later:', $this->textDomain).'</h4>
                            <p>'.__('- Min Limit: ', $this->textDomain)
                               .__($currenyByCountryCode[$countryCode], $this->textDomain).' '
                               .(MoneyHelper::formatNumberGeneral($countryPaymentType[self::PAYMENT_TYPE_PAY_BY_LATER]['min_limit'])).'</p>
                            <p>'.__('- Max Limit: ', $this->textDomain)
                               .__($currenyByCountryCode[$countryCode], $this->textDomain).' '
                               .(MoneyHelper::formatNumberGeneral($countryPaymentType[self::PAYMENT_TYPE_PAY_BY_LATER]['max_limit'])).'</p>
                            
                        <h4>'.__('Pay By Instalments:', $this->textDomain).'</h4>
                            <p>'.__('- Min Limit: ', $this->textDomain)
                               .__($currenyByCountryCode[$countryCode], $this->textDomain).' '
                               .(MoneyHelper::formatNumberGeneral($countryPaymentType[self::PAYMENT_TYPE_PAY_BY_INSTALMENTS]['min_limit'])).'</p>
                            <p>'.__('- Max Limit: ', $this->textDomain)
                               .__($currenyByCountryCode[$countryCode], $this->textDomain).' '
                               .(MoneyHelper::formatNumberGeneral($countryPaymentType[self::PAYMENT_TYPE_PAY_BY_INSTALMENTS]['max_limit'])).'</p>
                    </div>
                </div>';
            }

            return $htmlString;
        } else {
            return 'N/A';
        }
    }

    /**
     * Get country code based on its currency
     *
     * @return array
     */
    public function getCurrencyToCountryMapping()
    {
        return [
            'SAR' => 'SA',
            'AED' => 'AE',
            'KWD' => 'KW',
        ];
    }

    /**
     * Build Tamara Country Payment Types Cache Key with unique Api params
     *
     * @return string
     */
    public function buildCountryPaymentTypesCacheKey()
    {
        return 'tamara_country_payment_types_'.md5(json_encode([
                $this->apiUrl,
                $this->apiToken,
            ]));
    }

    /**
     * Get All Country Payment Types Limit Amounts
     *
     * @return array
     */
    public function getCountryPaymentTypes()
    {
        $countryPaymentTypesCacheKey = $this->buildCountryPaymentTypesCacheKey();
        $countryPaymentTypes = get_transient($countryPaymentTypesCacheKey);
        if (empty($countryPaymentTypes)) {
            $countryCodes = array_map('trim',
                explode(',', $this->get_option('allowed_shipping_country_codes')));
            $countryPaymentTypes = [];
            foreach ($countryCodes as $countryCode) {
                $countryCode = strtoupper($countryCode);
                try {
                    $response = $this->tamaraClient->getPaymentTypes($countryCode);
                    if ($response->isSuccess() && $response->getPaymentTypes()->count() > 0) {
                        $paymentTypes = [];
                        /** @var PaymentType $paymentType */
                        foreach ($response->getPaymentTypes() as $paymentType) {
                            $paymentTypes[$paymentType->getName()]['min_limit'] = $paymentType->getMinLimit()->getAmount();
                            $paymentTypes[$paymentType->getName()]['max_limit'] = $paymentType->getMaxLimit()->getAmount();
                        }
                        $countryPaymentTypes[$countryCode] = $paymentTypes;
                    }
                } catch (Exception $exception) {
                    TamaraCheckout::getInstance()->logMessage(
                        sprintf(
                            "Tamara Service timeout or disconnected.\nError message: ' %s'.\nTrace: %s",
                            $exception->getMessage(),
                            $exception->getTraceAsString()
                        )
                    );
                }
            }
            if (!empty($countryPaymentTypes)) {
                set_transient($countryPaymentTypesCacheKey, $countryPaymentTypes, 600);
            }
        }

        return $countryPaymentTypes;
    }

    /**
     * Return the name of Tamara settings option in the WP DB.
     *
     * @return string
     */
    public function get_option_key()
    {
        return TamaraCheckout::getInstance()->getWCTamaraGatewayOptionKey();
    }

    /**
     * Populate Tamara default description on checkout
     */
    public function populateTamaraDefaultDescription()
    {
        $description = __('*Exclusive for shoppers in KSA and UAE', $this->textDomain);
        if ($this->isSandboxMode()) {
            $description .= '<br/>'.sprintf(__('SANDBOX ENABLED. See the %s for more details.',
                    $this->textDomain),
                    '<a target="_blank" href="https://app-sandbox.tamara.co">Tamara Sandbox Testing Guide</a>');
        }

        return trim($description);
    }

    /**
     * Get Payment type based on its ID
     *
     * @return array
     */
    public function getPaymentTypeMapping()
    {
        return [
            TamaraCheckout::TAMARA_GATEWAY_ID => static::PAYMENT_TYPE_PAY_BY_LATER,
            TamaraCheckout::TAMARA_GATEWAY_PAY_BY_INSTALMENTS_ID => static::PAYMENT_TYPE_PAY_BY_INSTALMENTS,
        ];
    }

    /**
     * Get Payment type title based on its ID and site locale
     *
     * @return array
     */
    public function getPaymentTypeTitleMapping()
    {
        $siteLocale = substr(get_locale(), 0, 2) ?? 'en';
        if ('ar' === $siteLocale) {
            return [
                TamaraCheckout::TAMARA_GATEWAY_ID => $this->getPayByLaterTitleAr(),
                TamaraCheckout::TAMARA_GATEWAY_PAY_BY_INSTALMENTS_ID => $this->getPayByInstalmentsTitleAr(),
            ];
        } else {
            return [
                TamaraCheckout::TAMARA_GATEWAY_ID => $this->getPayByLaterTitle(),
                TamaraCheckout::TAMARA_GATEWAY_PAY_BY_INSTALMENTS_ID => $this->getPayByInstalmentsTitle(),
            ];
        }
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function removeTrailingSlashes($url)
    {
        return rtrim(trim($url), '/');
    }
}
